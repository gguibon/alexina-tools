#include "lexedinfo.h"

Info::Info(Info *next = NULL, TYPEPTR offset = 0) {
    this->adress = 0;
    this->next = next;
    this->offset = offset;
}
  
Info* Info::get_next() {
    return this->next;
}

void Info::set_next(Info *next) {
    this->next = next;
}

TYPEPTR Info::get_offset() {
    return this->offset;
}

void Info::set_offset(TYPEPTR offset) {
    this->offset = offset;
}

TYPEPTR Info::get_adress() {
    return this->adress;
}

void Info::set_adress(TYPEPTR Adress) {
    this->adress = Adress;
}
