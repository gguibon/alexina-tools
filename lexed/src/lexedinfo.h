//<head>//////////////////////////////////////////////////////////////
//
//	
//	Lexed version 3.1
//	Copyright (C) 1999 2000 2001 2002 2003.
//	Universit� Paris 7
//	INRIA Rocquencourt
//
//	Conception & programmation :
//	Lionel Cl�ment
//	e-mail : lionel.clement@linguist.jussieu.fr
//
//	vendredi 18 janvier 2002, 13h41
//
//
////////////////////////////////////////////////////////////////</head>////

#ifndef LEXEDINFO_H
#define LEXEDINFO_H

#include "lexedtypes.h"

class Info {
private:
  Info *next;
  TYPEPTR offset;
  TYPEPTR adress;

public:
  Info(Info *, TYPEPTR);
  Info* get_next();
  void set_next(Info *next);
  TYPEPTR get_offset();
  void set_offset(TYPEPTR offset);
  TYPEPTR get_adress();
  void set_adress(TYPEPTR adress);
};

#endif // LEMMEINFO_
