#!/usr/bin/env perl

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my ($constr,$subconstrs);
my $op;
my %code;
my %der2constrs;
my $branch_level = 0;
my $indent = "  ";

while (<>) {
  chomp;
  s/\#.*//;
  if (s/^\s*\%([^ \(\)]+)(?:\(\%(.*)\))?\s*=\s*//) {
    if ($constr ne "") {
      $code{$constr}.="\n$indent\$answer .= \"\$weight;\$pred;\$cat;\".&sort_subcatframe(\$sc).\";\".&sort_macros(\$macros);\n";
      for (1..$branch_level) {
	$code{$constr}.="\n".("  "x($branch_level-$_+1))."}";
      }
      $code{$constr}.=" # $branch_level";
    }
    $constr = $1;
    $subconstrs = $2;
    if ($subconstrs ne "") {
      if ($constr !~ /^\%/) {die "Only derivations can specify a list of constructions"}
      else {$der2constrs{$constr} = $subconstrs}
    }
    $branch_level = 0;
    die "\%default is a reserved construction, you can't redefine it" if ($constr eq "default");
    $code{$constr}.="\n".$indent."my \$answer = \"\";";
  }
  while ($_ !~ /^\s*$/ && s/^(.*?)(\s*\+\s*|$)//) {
    $op = $1;
    next if ($op =~ /^\s*$/);
    $code{$constr}.="\n$indent".op2code($op);
  }
}
if ($constr ne "") {
  $code{$constr}.="\n$indent\$answer .= \"\$weight;\$pred;\$cat;\".&sort_subcatframe(\$sc).\";\".&sort_macros(\$macros);\n";
  for (1..$branch_level) {
    $code{$constr}.="\n".("  "x($branch_level-$_+1))."}";
  }
  $code{$constr}.=" # $branch_level";
}

my $main = <<END;
#!/usr/bin/env perl

use open ':utf8';
use utf8;

binmode STDIN, ":utf8"; 
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my \$l;
my \%cache;

while (<>) {
  chomp;
  \$l++;
  if (\$l % 1000 == 0) {print STDERR "\\r     Line \$l";}
  if (/^([^\\t]*\\t[^\\t]*)\\t([^\\t]*)\\t([^\\t]*)\\t([^\\t]*);\\\%([^\\t;]*)\\t([^\\t]*)/) {
    my \$lemma = \$1; my \$msprop = \$2; my \$infos = \$3; my \$sc = \$4; my \$constrs = \$5; my \$inflclass = \$6;
    my \$newsc;
    for my \$constr (split(/,\\s*\%/,\$constrs)) {
      \$sc = \$newsc=\$sc;
      for (split(/\\s*\\+\\s*\%/,\$constr)) {
        \$newsc = \&transform(\$newsc,\$msprop,\$_);
        last if (\$newsc eq "");
      }
      if (\$newsc ne "") {
        &__output (\$lemma,\$msprop,\$infos,\$newsc,\"\%\$constr\",\$inflclass);
      }
    }
  } else {
    print STDERR "\nFormat error in .f file (probably resulting from an error in the .ilex or .mf file): \$_ (skipping line)\n";
  }
}
print STDERR "\\r     Done             \\n";

sub transform {
  my \$s = shift;
  my \$msprop = shift;
  my \$constr = shift;
END
print "$main  ";
for (keys %code) {
    print "if (\$constr eq \"$_\") {return \&".unaccent($_)." (\$s, \$msprop);}\n  els";
}
print "if (\$constr eq \"default\") {return \$s;}\n  else {return \"ERROR (\$constr)\";}\n";
print "}\n\n";

my $sort_functions = <<END;
sub sort_subcatframe {
    my \$sc = shift;
    my \$newsc = "";
    my (\$args,\$arg,\$asr);
    if (\$sc ne "") {
	\$sc =~ /^<(.*)>(.*)\$/ || die "Wrong subcat format in \$_ (\$sc)\\n";
	\$args = \$1;
	\$asr = \$2; # asemantic realizations
	\$newsc.="<";
	for \$arg (split(/\\s*,\\s*/,\$args)) {
	    \$arg =~ s/^([^:]+):(\\(?)(.*?)(\\)?)\$/\$3/;
	    \$newsc .= \$1.":".\$2.join("|", sort split(/\\s*\\|\\s*/,\$arg))."\$4,";
	}
	\$newsc =~ s/,\$//;
	\$newsc .= ">".(join(",", sort split(/\\s*,\\s*/,\$asr)));
    }
    return \$newsc;
}

sub sort_macros {
    my \$macros = shift;
    \$macros =~ s/^,+//;
    \$macros =~ s/,+\$//;
    \$macros = join(",", sort split(/\\s*,+\\s*/,\$macros));
    while (\$macros =~ s/(^|,)([^,]+)(?:,\\2)+,/\\1\\2,/) {}
    return \$macros;
}
END

print $sort_functions;


print <<END;
sub __output {
  my \$lemma = shift;
  my \$msprop = shift;
  my \$infos = shift;
  my \$sc = shift;
  my \$constrs = shift;
  my \$inflclass = shift;
  \$constrs =~ /^(^|.*,)\\s*(\\\%[^,]+)\\s*\$/;
  my \$lastconstr = \$2;
  if (\$constrs !~ /^\\\%/) {
    print "\$lemma\\t\$msprop\\t\$infos\\t\$sc\\t\%\$constrs\\n";
END
for (keys %der2constrs) {
  print "  } elsif (\$lastconstr eq \"\%$_\") {\n";
  print "    for my \$constr (\"".join("\",\"",split(/\s*,\s*\%/,$der2constrs{$_}))."\") {\n";
print <<END;
      \$newsc = \&transform(\$sc,\$msprop,\$constr);
      if (\$newsc ne "") {
        print "\$lemma\\t\$msprop\\t\$infos\\t\$newsc\\t\$constrs+\%\$constr\\t\$inflclass\\n";
      }
    }
END
}
print <<END;
  } else {
    print "\$lemma\\t\$msprop\\t\$infos\\t\$sc\\t\$constrs\\t\$inflclass\\n";
  }
}
END

for (keys %code) {
    print "sub ".unaccent($_)." \{\n  my \$s = shift;\n  my \$msprop = shift;\n";
    print "  \$s =~ /^(.*);(.*);(.*);(.*);(.*)\$/ || die \"Wrong input format: \$s\";\n";
    print "  my \$weight = \$1; my \$pred = \$2; my \$cat = \$3; my \$sc = \$4; my \$macros = \$5;";
    print $code{$_};
    print "\n  return \$answer;\n";
    print "}\n\n";
}

sub op2code {
  my $o = shift;
  my ($msonly,$mustwork,$op,$arg,$origo);
  my $c = $o;
  if ($o =~ s/^\%//) {
    if ($o =~ /,/) {
      $c = "\$olds = \$s;\n";
      $c .= $indent."for (\"".join("\", \"",split(/,\s*\%/,$o))."\") {\n";
      $branch_level++;
      $indent = "  ".("  "x$branch_level);
      $c .= $indent."\$s = \&{\$_} (\$olds, \$msprop);  # $o\n";
      $c .= $indent."if (\$s eq \"\") {next;}\n";
    } else {
      $c = "\$s = \&".unaccent($o)." (\$s, \$msprop);  # $o\n";
      $c .= $indent."if (\$s eq \"\") {return \"\";}\n";
    }
    $c .= $indent."\$s =~ /^(.*);(.*);(.*);(.*);(.*)\$/ || die \"Wrong input format: \$s\";\n";
    $c .= $indent."\$weight = \$1; \$pred = \$2; \$cat = \$3; \$sc = \$4; \$macros = \$5;";
    $o = "\%$o";
  } elsif ($o =~ /^(?:(.*):)?(\??)\{\s*(\S+?)\s+(.*?)\s*\}\s*$/) {
    $c = "";
    $origo = $o;
    $msonly = $1; $mustwork = ($2 eq "");
    $op = $3; $arg = $4;
    $mustwork &&=  (0 && $op !~ /^(Skip|Only|0)$/ && $arg !~ /^(<|\(\)|\)\()/ && !($op =~ /^[A-Z]/ && $arg !~ /^[<-]/));
    if ($msonly ne "") {
      $c = "if (\$msprop eq \"$msonly\") { ";
    }
    if ($op eq "Skip") {
      $c .= "return \"\" if (\$msprop eq \"$arg\" || \$macros =~ /(^|,)".quotemeta($arg)."(,|\$)/)";
    } elsif ($op eq "Macros") {
      $c .= "\$macros .= \",".quotemeta($arg)."\"";
    } elsif ($op eq "Lemma") {
      $c .= "\$pred = \"".quotemeta($arg)."\"";
    } elsif ($op eq "Cat") {
      $c .= "\$cat = \"".quotemeta($arg)."\"";
    } elsif ($op eq "Only") {
      $c .= "return \"\" unless (\$msprop eq \"$arg\")";
    } elsif ($op eq "0") {
      if ($arg eq "0") {
	$c .= "\$sc =~ s/>.*\$/>/";
      } elsif ($arg =~ s/^-//) {
	$c .= "\$sc =~ s/>(\\(?[^\\),>\\/]+\\|)?$arg/>\$1/;\n";
	$c .= $indent."\$sc =~ s/,,/,/";
      } else {
	$c .= "\$sc =~ s/>\$/>$arg/ ||\n";
	$c .= $indent."\$sc =~ s/\$/,$arg/";
      }
    } elsif ($op =~ /^[A-Z]/) {
      $op=$op.":";
      if ($arg eq "0") {
	$c .= "\$sc =~ s/$op\[^,>\]+//;\n";
	$c .= $indent."\$sc =~ s/([,<]),/\$1/;\n";
	$c .= $indent."\$sc =~ s/,>/>/;\n";
      } elsif ($arg eq "()") {
	$c .= $indent."\$sc =~ s/$op([^\\(][^,>]*)/$op\\(\$1\\)/";
      } elsif ($arg eq ")(") {
	$c .= "\$sc =~ s/$op\\(([^,>]+)\\)/$op\$1/";
      } elsif ($arg eq "-") {
	$c .= "\$sc =~ s/$op\\(?([^,>]+)\\)?//;";
	$c .= $indent."\$sc =~ s/([,<]),/\$1/;";
	$c .= $indent."\$sc =~ s/,>/>/";
      } elsif ($arg =~ s/^-//) {
	$c .= "\$sc =~ s/$op(\\(?[^\\),>\\/]+\\|)?$arg(?=[\\|\\),>])/$op$1/;";
	$c .= $indent."\$sc =~ s/\\|+/\\|/;";
	$c .= $indent."\$sc =~ s/:(\\()?\\|/:\$1/;";
	$c .= $indent."\$sc =~ s/\\|(\\))?([,>])/\$1\$2/;";
	$c .= $indent."\$sc =~ s/$op([,>])/\$1/;";
	$c .= $indent."\$sc =~ s/([,<]),/\$1/;";
	$c .= $indent."\$sc =~ s/,>/>/";
      } elsif ($arg =~s/^<//) {
	$arg=~s/^([^\[]+)(?:\[(.*)\])?$/$1/;
	my $ops=$2; my %ops=();
	for (split(/\s*,\s*/,$ops)) {
	  /^([^\s]+)\s*>\s*([^\s]*)/ || die "error with op $_\n";
	  $ops{$1}=$2;
	}
	$arg=$arg.":";
	if ($arg ne $op) {
	  $c .= $indent."\$sc =~ s/".$op."[^,>]*//;\n"
	}
	$c .= $indent."if (\$sc =~ /$arg([^,>]*)/) {\n";
	$c .= $indent."  my \$temp=\$1;\n";
	for (keys %ops) {
	  if ($ops{$_} ne "") {
	    $c .= $indent."  \$temp=~s/(^|\\||\\()$_((?:__[A-Z])?)(\\||\\)|\$)/\$1$ops{$_}\$2\$3/;\n";
	  } else {
	    $c .= $indent."  \$temp=~s/(^|\\||\\()$_(?:__[A-Z])?(\\||\\)|\$)/\$1$ops{$_}\$2/;\n";
	  }
	}
	$c .= $indent."  \$sc =~ s/$arg([^,>]*)/$op\$temp/;\n";
	$c .= $indent."}\n";
	$c .= $indent."\$sc =~ s/\\|+/\\|/g;\n";
	$c .= $indent."\$sc =~ s/:(\\()?\\|/:\\1/;\n";
	$c .= $indent."\$sc =~ s/\\|(\\))?([,>])/\\1\\2/;\n";
	$c .= $indent."\$sc =~ s/$op([,>])/\\1/;\n";
	$c .= $indent."\$sc =~ s/([,<]),/\\1/;\n";
	$c .= $indent."\$sc =~ s/,>/>/";
      } else {
	my $arg2 = $arg;
	$arg2=~s/^\\(([^\\(\\)]*)\\)$/$1/;
	$c .= $indent."if (\$sc =~ /$op/) {\n";
	$c .= $indent."  \$sc =~ s/$op(\\(?[^\\),>\\/]+)/$op\$1|$arg2/;\n";
	$c .= $indent."} else {\n";
	$c .= $indent."  \$sc =~ s/<>\$/<$op$arg>/ ||\n";
	$c .= $indent."        \$sc =~ s/>\$/,$op$arg>/;\n";
	$c .= $indent."}";
      }
    } else {
      $op =~ s/\@/\\\@/;
      $arg =~ s/\@/\\\@/;
      $op =~ s/(^|[^\\])\./$1\[^,\]/g;
      $op =~ s/(^|[^\\])\./$1\[^,\]/g;
      $c .= "\$macros =~ s/$op/$arg/g";
    }
    if ($mustwork) {
      $c .= " || die \"Rule \\\"".quotemeta($origo)."\\\" could not be applied on input \$s (\$msprop)\"";
    }
    $c .= ";";
    if ($msonly ne "") {
      $c .= " }";
    }
    $c .= "  # $o";
  }
  return $c;
}

sub unaccent {
    my $s = shift;
    $s =~ tr/àâäéèêëîïôöùûüÿçÀÂÄÉÈÊËÎÏÔÖÙÛÜŸÇ/aaaeeeeiioouuuycAAAEEEEIIOOUUUYC/;
    $s =~ tr/%/__/;
    $s =~ tr/-/_/;
    $s =~ s/œ/oe/g;
    return $s;
}
