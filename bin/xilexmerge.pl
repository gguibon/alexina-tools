#!/usr/bin/env perl

my $showalllemmas=0;
my $showpron=0;
my $noreal=0;

my $filenum;
while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-noreal$/i) {$noreal=1;}
    elsif (/^-sal$/i) {$showalllemmas=1;}
    elsif (/^-sp$/i) {$showpron=1;}
    elsif (/^([^-].*)$/i && $filenum == 0) {$file1=$1; $filenum = 1}
    elsif (/^([^-].*)$/i && $filenum == 1) {$file2=$1;}
}

my %data;
my %lemmas;
my %data2interpr;

if ($file1=~s/^(.*)[=:]//) {@data2interpr[1]=$1;} else {@data2interpr[1]="1";}
if ($file2=~s/^(.*)[=:]//) {@data2interpr[2]=$1;} else {@data2interpr[2]="2";}
@data2interpr[3]="common";

&loadfile($file1,1);
&loadfile($file2,2);

sub loadfile {
    my $file=shift;
    my $coeff=shift;
    print STDERR "Loading $file...";
    open(FILE,"<$file") || die "Could not open $file: $!\n";
    my %seen;
    my ($lemma,$infos);
    my $l=0;
    while (<FILE>) {
      $l++;
      if ($l % 1000 == 0) {print STDERR "\rLoading $file... $l";}
	if (/^([^=+ ].*?;)(<.*)$/) {
	    $lemma=$1;
	    $infos=$2;
	    $infos=~s/:a-/:à-/g;
	    $infos=~s/:aupr[eè]s[ _]?de/:auprès_de/g;
	    $infos=~s/Obja:/Objà:/g;
	    $infos=~s/\@(avoir|être),//;
	    while ($infos=~s/(:[^,>]+) /\1_/g) {}
	    $infos=~s/ //g;
	    $erroneous_entry=0;
	    for $f (qw/Suj Obj Objà Objde Att Loc Dloc Obl Obl2/) {
		$erroneous_entry = ($infos=~/<.*$f:.*$f:.*>/
#				    || $infos=~/<.*:(cl[nagdln]|y|en)[,>]/
				   );
		last if ($erroneous_entry);
	    }
	    next if ($erroneous_entry);
	    %real=();
	    for $f (qw/Suj Obj Objà Objde Att Loc Dloc Obl Obl2/) {
		if ($infos=~s/(<.*)$f:([^,>]+)(.*>)/\1\3/) {
#		    $real{$f2}=$r;
		    $real{$f}=$2;
		}
	    }
	    $infos=~s/<,+>/<>/;
	    for $f (qw/Suj Obj Objà Objde Att Loc Dloc Obl Obl2/) {
		if ($real{$f} ne "") {
#		    $infos=~s/>/,$f:$real{$f}>/;
		    $infos=~s/>/,$f>/;
		}
	    }
	    $infos=~s/<,/</;
	    if (!defined($seen{$lemma."\t".$infos})) {
		$data{$lemma}{$infos}+=$coeff;
		$lemmas{$lemma}{$coeff}=1;
		$seen{$lemma."\t".$infos}=1;
	    }
	}
    }
    print STDERR "\rLoading $file... done        \n";
}

for my $lemma (keys %data) {
  $base = $lemma;
  $base =~ s/^se // || $base =~ s/^s\'//;
  if ($showalllemmas || ($lemmas{$lemma}{1} && $lemmas{$lemma}{2}) 
      || ($showpron && ($lemmas{$base}{1} && $lemmas{$base}{2}))) {
    for my $infos (sort keys %{$data{$lemma}}) {
      print $data2interpr[$data{$lemma}{$infos}]."\t$lemma $infos\n";
    }
  }
}
