#!/usr/bin/env perl
# $Id: compute-tables.pl 2250 2015-03-12 13:53:36Z sagot $

use strict;

my $l;
my %in=();
my %probleme=();

my $affile;
my $cat;

while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^[^-]/) {$affile=$_;}
    elsif (/^-cat=(.*)$/) {$cat=$1;}
}
if ($cat eq "") {
    $cat=$affile;
    $cat=~s/\..*//;
}

my ($lemme,$forme,$tag,$origtag);

open(AF,"<$affile") || die "Fichier $affile introuvable\n";
open(MAN,">".$affile.".manual");

while (<AF>) {
    if (++$l % 1000 == 0) {print STDERR "af:$l\r"};
    chomp;
    s/\#.*//;
    if (/./) {
	/^([^\t]+)\t([^\t]+)\t([^\t]*)(?:\t.*)?$/ || die ("Erreur ligne $l\n");
	$forme=$1;
	$lemme=$2;
	$tag=$3;
	$origtag=$3;
	$tag = &normalize_tag($origtag);
	if (!defined($in{$lemme}{$tag}) || $in{$lemme}{$tag} eq $forme) {
	    $in{$lemme}{$tag}=$forme;
	} else {
	    $probleme{$lemme}=1;
	    print MAN "$forme\t$lemme\t$origtag\n";
	}
    }
}
close(AF);

$l=0;
print STDERR "                   \r";

for my $lemme (keys %probleme) {
    for my $t (sort keys %{$in{$lemme}}) {
	if (++$l % 1000 == 0) {print STDERR "mf:$l\r"};
	print MAN "$in{$lemme}{$t}\t$lemme\t$t\n";
    }
}

open(TBL,">".$affile.".tables");
open(LEM,">".$affile.".lemmes");

my $rad="";
my $term0="";
my $nf=0;
my %tables=();
my %out=();
my @terms=(); my @tags=();
my $done; my $firstchar;

$l=0;
print STDERR "                   \r";

for my $lemme (keys %in) {
    if (++$l % 1000 == 0) {print STDERR "l :$l\r"};
    @terms=();
    @tags=();
    $done=0;
    if (!defined($probleme{$lemme})) {
	$rad="";
	$nf=0;
	for my $t (sort keys %{$in{$lemme}}) {
	    push(@tags,$t);
	    push(@terms,$in{$lemme}{$t});
	}
	if ($#terms<0) {
	    die "Erreur incompréhensible\n";
	}
	while (!$done) {
	    for my $i (0..$#terms) {
		if ($terms[$i] eq "") {
		    $done=1;
		    last;
		}
	    }
	    if (!$done) {
		$terms[0]=~/^(.)/;
		$firstchar=$1;
		for my $i (0..$#terms) {
		    if ($terms[$i] !~ /^$firstchar/) {
			$done=1;
			last;
		    }
		}
	    }
	    if (!$done) {
		$rad.=$firstchar;
		for my $i (0..$#terms) {
		    $terms[$i] =~ s/^$firstchar// || die ("Erreur impossible");
		}
	    }
	}
	$lemme=~/$rad(.*)$/;
	$term0=$1;
	if ($term0=~/-/ || $term0 eq $lemme) {
	    for my $t (sort keys %{$in{$lemme}}) {
		print MAN "2:$in{$lemme}{$t}\t$lemme\t".&restore_tag($t)."\n";
	    }
	} else {
	    push(@{$tables{$term0."###".join("\t",@tags)."###".join("\t",@terms)}},$lemme);
	}
    } 
}

print STDERR "Nombre de tables extraites: ".(scalar(keys %tables))."\n";

my %tablenames=();
my @tagsarray=();
my @termsarray=();
my $tn="";
my $tnum=0;
my %tablenamestranslation=();
my $tn_suff="";
my $tn_pref="";
my $tags; my $terms;

for my $origtablename (keys %tables) {
    $origtablename=~/^([^\t\#]*)\#\#\#([^\#]*)\#\#\#([^\#]*)$/;
    $term0=$1;
    $tags=$2;
    $terms=$3;
    $tn="";
    $tnum=0;
    @tagsarray=split(/\t/,$tags);
    @termsarray=split(/\t/,$terms);
    if ($tags ne "") {
	for my $car (qw/s p a b c d e g h i j k l m o q r t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z n f 0 1 2 3 4 5 6 7 8 9/) {
	    my $ok=1;
	    for (@tagsarray) {
		if ($_ eq "" || $_!~/$car/) {
		    $ok=0;
		    last;
		}
	    }
	    if ($ok==1) {
		$tn=$tn.$car;
	    }
	}
    }
    $tn=&restore_tag($tn);
    if ($tn ne "") {$tn="/".$tn;}
    if ($term0 ne "") {
	$tn="-".$term0.$tn;
    }
    $tn_pref=$tables{$origtablename}[0];
    for (@{$tables{$origtablename}}) {
	my $temp=$tn_pref."\t".$_;
	$temp=~/(.*)\t.*\1$/;
	$tn_pref=$1;
    }
    $tn_pref=~/(.*)$term0$/;
    $tn_pref=$1;
    if ($tn_pref ne "") {
	$tn_pref="_".$tn_pref;
    }
    $tn=$cat.$tn_pref.$tn;
    $tn=~s/ //g;
    $tn_suff="";
    while (defined($tablenames{$tn.$tn_suff})) {
	$tnum++;
	$tn_suff="-".$tnum;
    }
    $tablenames{$tn.$tn_suff}=1;
    $tablenamestranslation{$origtablename}=$tn.$tn_suff;
    print TBL "<!--".$tables{$origtablename}[0]."\t".(scalar(@{$tables{$origtablename}}))."\t".($#tagsarray+1)."-->\n";
    print TBL "<table name=\"$tn$tn_suff\" source=\"$term0\" rads=\"..*\">\n";
    for my $i (0..$#tagsarray) {
	print TBL "    <form target=\"".$termsarray[$i]."\" tag=\"".&restore_tag($tagsarray[$i])."\">\n";
    }
    if ($#tagsarray==-1) {print TBL "\t\"\"\n";}
    print TBL "</table>\n";
}

for my $origtablename (keys %tables) {
    for (@{$tables{$origtablename}}) {
	print LEM $_."\t".$tablenamestranslation{$origtablename}."\n";
    }
}



sub normalize_tag {
    $_=shift;
    s/sg/s/; s/pl/p/;
    s/m1/1/; s/m2/2/; s/m3/3/;
    s/nom/N/; s/voc/V/; s/acc/A/; s/dat/D/; s/gen/G/; s/loc/L/; s/inst/I/;
    s/pos/P/;
    s/://g;
    return $_;
}
sub restore_tag {
    $_=shift;
    s/[fn]/n:/;
    s/s/sg:/; s/p/pl:/;
    s/1/m1:/; s/2/m2:/; s/3/m3:/;
    s/N/nom:/; s/V/voc:/; s/A/acc:/; s/D/dat:/; s/G/gen:/; s/L/loc:/; s/I/inst:/;
    s/P/pos:/;
    s/:+(\t|$)/\1/; s/:+/:/g;
    return $_;
}
