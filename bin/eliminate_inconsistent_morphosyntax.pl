#!/usr/bin/env perl
# $Id: eliminate_inconsistent_morphosyntax.pl 2250 2015-03-12 13:53:36Z sagot $

while (<>) {
  # pas d'impersonnel � une personne autre que 3s
  $imp=0;
  if (/\tv\t\[/ && /\@impers\b/) {
    if (s/(\@[A-Z]+)[12]*(3[^p\@]*\])/\1\2/) { 
      s/(\t[A-Z]+)[12]?(3[^p\@\t]*)$/\1\2/;
      print $_;
    }
  } else {
    print $_;
  }
}
