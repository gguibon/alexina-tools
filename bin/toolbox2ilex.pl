#/usr/bin/perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

while (<>) {
  chomp;
  s/\s+$//;
  if (/^\\lx ([^ ]+)$/) {
    print "$lx$ge\t$ps\n" unless ($lx eq "" || $ps eq "");
    $ps = "";
    $ge = "";
    $lx = $1;
  } elsif (/^\\ps (.+)$/) {
    print "$lx$ge\t$ps\n" unless ($lx eq "" || $ps eq "");
    $ps = $1;
  } elsif (/^\\ge (.+)$/) {
    $ge = "___".$1;
    $ge =~ s/ \([^)]*\)//g;
  }
}
