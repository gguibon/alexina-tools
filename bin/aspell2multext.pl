#!/usr/bin/env perl

use strict;
use utf8;
binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $infl;

while (<>) {
  chomp;
  /^([^\/]*)(?:\/(\S+))? (.+)$/ || die "Format error: $_";
  if ($2 eq "") {
    $infl = "inv#"
  } 
  else {
    $infl = "$2";
    $infl =~ s/^(.)//;
    if ($1 eq lc($1)) {
      $infl = "$1#$infl";
    } else {
      $infl = "M".lc($1)."#$infl";
    }
  }
  print "$3\t$1\t$infl\n";
}
