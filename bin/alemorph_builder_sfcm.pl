#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use strict;

my $lang;
my $line;
my ($str, $str2, $str3);
my ($i, $j);
my ($cur_pattern, $cur_table, $cur_zone, $cur_stemtable, $cur_pair_l1, $cur_pair_l2, $cur_block_id);
my %letterclass;
my %translitteration;
my %detranslitteration;
my %table;
my %pattern;
my %subpattern;
my %zone;
my %form;
my %stem;
my %stems;
my %pairs;
my (@sandhi_source, @sandhi_target, @sandhi_isfinal, @sandhi_isrev);
my (@pfm_block, %pfm, %subzone);
my %cat2patterns;
my %zone2tags;
my %zone_inheritance;
my %terms;
my %zone_var2incompatiblevars;
my %tmp_hash;
my $is_in_alt;

while (<>) {
  chomp;
  $line++;
  s/[ \t]+/ /g;
  s/<\!--.*?-->//g;
  s/^ +//;
  s/ +$//;
  next if /^$/;
  if (/\<\?xml.*? encoding="(.*?)".*?\?>/) {
    $str = $1;
    if ($str !~ /^utf-?8$/i) {
      error ("Encoding should be UTF-8");
    }
  } elsif (/<description lang="(.*?)"/) {
    $lang = $1;
  } elsif (/<letterclass /) {
    / name="(.*?)"/ || error ("Letter class has no name");
    $str = $1;
    / letters="(.*?)"/ || error ("Letter class $str undefined");
    $str2 = $1;
    $str2 =~ s/^ //;
    $str2 =~ s/ $//;
    $letterclass{$str} = $str2;
  } elsif (/<translitteration /) {
    / source="(.*?)"/ || error ("Letter class has no source");
    $str = $1;
    / target="(.*?)"/ || error ("Letter class has no target");
    error ("Two translitterations for source string '$str'") if defined($translitteration{$str});
    error ("Two translitterations for target string '$1'") if defined($detranslitteration{$1});
    $translitteration{$str} = $1;
    $detranslitteration{$1} = $str;
  } elsif (/<(?:sandhi|fusion) /) {
    / source="(.*?)"/ || error ("Sandhi has no source");
    $str = $1;
    / target="(.*?)"/ || error ("Sandhi has no target");
    $str2 = $1;
    push @sandhi_source, $str;
    push @sandhi_target, $str2;
    if (/ final=\"-\"/ || $_ !~ / final=\"\+\"/) {
      push @sandhi_isfinal, 0; # default: !isfinal
    } else {
      push @sandhi_isfinal, 1;
    }
    if (/ rev=\"\+\"/ || $_ !~ / rev=\"-\"/) {
      push @sandhi_isrev, 1; # default: isrev
    } else {
      push @sandhi_isrev, 0;
    }
  } elsif (/<table /) {
    $cur_block_id = -1;
    @pfm_block = ();
    / name="(.*?)"/ || error ("Table has no name");
    $cur_table = $1;
    error ("Table '$cur_table' defined twice") if defined($table{$cur_table});
    error ("Name conflict: '$cur_table' is both a table and a pattern") if defined($pattern{$cur_table});
    error ("Name conflict: '$cur_table' is both a table and a zone") if defined($zone{$cur_table});
    $cur_table =~ /^(.+?)(?:[-0-9]|$)/ || die "Bad table name: '$cur_table'";
    $zone{$cur_table}{table} = $cur_table;
    $cat2patterns{$1}{$cur_table} = 1;
    $table{$cur_table}{cat} = $1;
    $pattern{$cur_table}{cat} = $1;
    if (/ rads="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    $str = "__undef" if ($str eq ".*");
    $table{$cur_table}{rads} = $str;
    $pattern{$cur_table}{rads} = "__undef";
    $zone{$cur_table}{rads} = $str;
    if (/ except="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    $str = "__undef" if ($str eq ".*");
    $table{$cur_table}{except} = $str;
    $pattern{$cur_table}{except} = "__undef";
    $zone{$cur_table}{except} = $str;
    if (/ canonical_tag="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    # no canonical_tag directy associated with a table
    $pattern{$cur_table}{canonical_tag} = $str;
    $zone{$cur_table}{canonical_tag} = $str;
    if (/ default_stem_table="(.*?)"/) {$str = $1;} else {$str = "";}
    $table{$cur_table}{default_stem_table} = $str;
    $pattern{$cur_table}{default_stem_table} = $str;
    $cur_zone = $cur_table; # par défaut
    $subpattern{$cur_table}[0]{zone_name} = $cur_table;
  } elsif (/<\/table/) {
    error ("Unexpected end of table") if ($cur_table eq "");
    error ("End of table within <alt>") if $is_in_alt;
    $cur_table = "";
  } elsif (/<pattern /) {
    / name="(.*?)"/ || error ("Pattern has no name");
    $cur_pattern = $1;
    error ("Pattern '$cur_pattern' defined twice") if defined($table{$cur_pattern});
    error ("Name conflict: '$cur_pattern' is both a table and a pattern") if defined($table{$cur_pattern});
    $cur_pattern =~ /^(.+?)(?:[-0-9]|$)/ || die "Bad table name: '$cur_pattern'";
    $cat2patterns{$1}{$cur_pattern} = 1;
    $pattern{$cur_pattern}{cat} = $1;
    if (/ rads="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    $str = "__undef" if ($str eq ".*");
    $pattern{$cur_pattern}{rads} = $str;
    if (/ except="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    $str = "__undef" if ($str eq ".*");
    $pattern{$cur_pattern}{except} = $str;
    if (/ canonical_tag="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    $pattern{$cur_pattern}{canonical_tag} = $str;
    if (/ default_stem_table="(.*?)"/) {$str = $1;} else {$str = "";}
    $pattern{$cur_pattern}{default_stem_table} = $str;
  } elsif (/<\/pattern/) {
    error ("Unexpected end of pattern") if ($cur_pattern eq "");
    $cur_pattern = "";
  } elsif (/<subpattern /) {
    error ("Subpattern outside a pattern") if $cur_pattern eq "";
    error ("Subattern does not define zone_name") unless / zone_name="[^"]+"/;
    if (/ zone_name="(.*?)"/) {$str = $1;} else {$str = "";}
    $i = $#{$subpattern{$cur_pattern}} + 1;
    $subpattern{$cur_pattern}[$i]{zone_name} = $str;
    if (/ stem_name="(.*?)"/) {$str = $1;} else {$str = "";}
    $subpattern{$cur_pattern}[$i]{stem_name} = $str;
  } elsif (/<block /) {
    error ("PFM block outside a table") if $cur_table eq "";
    error ("PFM block inside a zone") if $cur_zone ne "";
#    / name="(.*?)"/ || error ("Block has no name");
#    $cur_block = $1;
    $cur_block_id++;
    @pfm_block = ();
  } elsif (/<\/block/) {
    my $j = -1;
    for my $i (sort {$pfm_block[$b]{specificity} <=> $pfm_block[$a]{specificity}} 0..$#pfm_block) {
      $j++;
      $pfm{$cur_table}[$cur_block_id][$j]{rule} = $pfm_block[$i]{rule};
      $pfm{$cur_table}[$cur_block_id][$j]{zone} = $pfm_block[$i]{zone};
      $pfm{$cur_table}[$cur_block_id][$j]{mfs} = $pfm_block[$i]{mfs};
      $pfm{$cur_table}[$cur_block_id][$j]{specificity} = $pfm_block[$i]{specificity};
    }
    $cur_block_id = -1;
  } elsif (/<pfm .*\/>/i) {
    error ("PFM rule outside a block") if $cur_block_id == -1;
    $i = $#pfm_block + 1;
    / rule="(.*?)"/ || error ("Zone has no name");
    $pfm_block[$i]{rule} = $1;
    if (/ zone="(.*?)"/) {$str = $1;} else {$str = $cur_table;}
    $pfm_block[$i]{zone} = $str;
    if (/ mfs="(.*?)"/) {$str = $1;} else {$str = "";}
    die "Incorrect mfs" unless $str =~ /^ *(?:\[ *[^\[\], ]+ +[^\[\], ]+ *\](?:, *\[ *[^\[\], ]+ +[^\[\], ]+ *\]))? *$/;
    $pfm_block[$i]{mfs} = $str;
    $pfm_block[$i]{specificity} = ($str =~ s/\[/\[/g);
  } elsif (/<zone /) {
    error ("Zone outside a table") if $cur_table eq "";
    / name="(.*?)"/ || error ("Zone has no name");
    error ("Zone name and table name can't be identical (here '$1')") if ($1 eq $cur_table);
    error ("Zone '$cur_zone' defined twice") if defined($zone{$1});
    $cur_zone = $1;
    if (/ canonical_tag="(.*?)"/) {$str = $1;} else {$str = "__undef";}
    $zone{$cur_zone}{table} = $cur_table;
    $zone{$cur_zone}{canonical_tag} = $str;
    $zone{$cur_zone}{rads} = $table{$cur_table}{rads};
    $zone{$cur_zone}{except} = $table{$cur_table}{except};
    $i = $#{$subpattern{$cur_table}} + 1;
    $subpattern{$cur_table}[$i]{zone_name} = $cur_zone;
  } elsif (/<\/zone/) {
    error ("Unexpected end of zone") if ($cur_zone eq "" || $cur_zone eq $cur_table);
    error ("End of zone within <alt>") if $is_in_alt;
    $cur_zone = $cur_table; # default
  } elsif (/<alt>/) {
    error ("Embedded <alt> elements") if ($is_in_alt);
    error ("<alt> element found outside a zone or table") if ($cur_zone eq "");
    $is_in_alt = 1;
  } elsif (/<\/alt>/) {
    error ("Unexpected end of <alt> element") unless ($is_in_alt);
    $is_in_alt = 0;
    for my $v1 (keys %tmp_hash) {
      for my $v2 (keys %tmp_hash) {
	$zone_var2incompatiblevars{$cur_zone}{$v1}{$v2} = 1 unless $v1 eq $v2;
      }
    }
    %tmp_hash = ();
  } elsif (/<like /) {
    / name="(.*?)"/ || error ("Inheritance link without the name of the inherited zone");
    $str2 = $1;
    if (/ except="(.*?)"/) {$str = $1;} else {$str = "";}
    $zone_inheritance{$cur_zone}{$str2}{except} = $str;
    if (/ only="(.*?)"/) {$str = $1;} else {$str = "";}
    $zone_inheritance{$cur_zone}{$str2}{only} = $str;
  } elsif (/<form .*\/>/) {
    error ("Form outside a table") if $cur_table eq "";
    if (/ tag="(.*?)"/) {$str = $1;} else {$str = "";}
    if (!defined($form{$cur_zone}{$str})) {
      $i = 0;
    } else {
      $i = $#{$form{$cur_zone}{$str}}+1;
    }
    push @{$zone2tags{$cur_zone}}, $str;
    if (/ var="(.*?)"/) {$str2 = $1; if ($is_in_alt) {$tmp_hash{$1} = 1}} else {$str2 = "";}
    $form{$cur_zone}{$str}[$i]{var} = $str2;
    if (/ synt="(.*?)"/) {$str2 = $1;} else {$str2 = "Default";}
    $form{$cur_zone}{$str}[$i]{mstag} = $str2;
    if (/ stem="(.*?)"/) {$str2 = $1;} else {$str2 = "";}
    $form{$cur_zone}{$str}[$i]{stem} = $str2;
    if (/ prefix="(.*?)"/) {$str2 = $1;} else {$str2 = "";}
    $form{$cur_zone}{$str}[$i]{prefix} = $str2;
    if (/ suffix="(.*?)"/) {$str2 = $1;} else {$str2 = "";}
    $form{$cur_zone}{$str}[$i]{suffix} = $str2;
    $terms{$str2}++;
    if (/ rads="(.*?)"/) {$str2 = $1;} else {$str2 = "__undef";}
    $str2 = "__undef" if ($str2 eq ".*");
    $form{$cur_zone}{$str}[$i]{rads} = $str2;
    if (/ except="(.*?)"/) {$str2 = $1;} else {$str2 = "__undef";}
    $str2 = "__undef" if ($str2 eq ".*");
    $form{$cur_zone}{$str}[$i]{except} = $str2;
    if (/ show="(.*?)"/) {$str2 = $1;} else {$str2 = "";}
    $form{$cur_zone}{$str}[$i]{show} = $str2;
  } elsif (/<subzone .*\/>/) {
    error ("Subzone outside a zone") if $cur_zone eq "";
    / mfs="(.*?)"/ || error ("Subzone has no morphosyntactic features (mfs)");
    $str = $1;
    if (/ name="(.*?)"/) {$str2 = $1;} else {$str2 = $str;}
    $subzone{$cur_zone}{$str} = $str2;
  } elsif (/<stemtable /) {
    / name="(.*?)"/ || error ("Stem table lacks a name");
    $cur_stemtable = $1;
  } elsif (/<\/stemtable/) {
    $cur_stemtable = "";
  } elsif (/<stem /) {
    / name="(.*?)"/ || error ("Stem lacks a name");
    $str = $1;
    if (/ source="(.*?)"/) {
      $stem{$cur_stemtable}{$str}{source} = $1;
    }
    if (/ operation="(.*?)"/) {$stem{$cur_stemtable}{$str}{operation} = $1;}
    else {$stem{$cur_stemtable}{$str}{operation} = "__undef";}
    push @{$stems{$cur_stemtable}}, $str;
  } elsif (/<pairs /) {
    / l1="(.*?)"/ || error ("Pair set lacks a l1");
    $cur_pair_l1 = $1;
    $cur_pair_l1 =~ s/ //g;
    / l2="(.*?)"/ || error ("Pair set lacks a l2");
    $cur_pair_l2 = $1;
    $cur_pair_l2 =~ s/ //g;
  } elsif (/<\/pairs/) {
    $cur_pair_l1 = "";
    $cur_pair_l2 = "";
  } elsif (/<pair /) {
    / l1="(.*?)"/ || error ("Pair lacks a l1");
    $str = $1;
    / l2="(.*?)"/ || error ("Pair lacks a l2");
    $str2 = $1;
    $pairs{$cur_pair_l1}{$cur_pair_l2}{$str}{$str2} = 1;
  }
}

translitterate_data ();

pfm2alexina ();

compute_zone_inheritance ();


open CHECK, ">morpho.$lang.check.pl";
binmode CHECK, ":utf8";
print CHECK <<END;
use utf8;
\$prefixes=qr/(?:(?:ne)?(?:u|na|za?|s|pre|pred|zod|pri|vy?|od?|po|do|zo)?)/;

our \%terms;
END
for (keys %terms) {
  print CHECK "\$terms{\"".translitterate($_)."\"}=1;\n";
}
for my $pair_l1 (keys %pairs) {
  for my $pair_l2 (keys %{$pairs{$pair_l1}}) {
    for my $l1 (keys %{$pairs{$pair_l1}{$pair_l2}}) {
      for my $l2 (keys %{$pairs{$pair_l1}{$pair_l2}{$l1}}) {
	print CHECK "\$".translitterate($pair_l1)."2".translitterate($pair_l2)."\{\"".translitterate($l1)."\"} = \"".translitterate($l2)."\";\n";
	print CHECK "\$".translitterate($pair_l2)."1".translitterate($pair_l1)."\{\"".translitterate($l2)."\"} = \"".translitterate($l2)."\";\n";
      }
    }
  }
}
print CHECK check_lemma_function();
print CHECK translitterate_function();
print CHECK detranslitterate_function();
print CHECK sandhi_function();
print CHECK sandhi_reverse_function();
print CHECK compute_possible_splits_function();
print CHECK try_to_lemmatize_function();
print CHECK lemmatize_function();
print CHECK compute_canonical_forms_function();
print CHECK sortvlist_function();
print CHECK remove_sep_function();
close CHECK;



open DIR, ">morpho.$lang.dir.pl";
binmode DIR, ":utf8";
print DIR <<END;
#!/usr/bin/env perl

use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

require("morpho.$lang.check.pl");

my \$log=0;
my \$showall=1;
my \$withvars=0;
my \$nosep=0;
my \$synt=0;
my \$rads=0;
my \$onetag=0;
my \$onerad=0;
my \$l=0;
my \$multiword=0; my \$mainword=0; my \$last_t;
my \%lastlemmaid;
my \@t; my \@w; my \@cat; my \@v; my \@msprop; my \@result; my \@tag; my \@table; my \@output;my \@num2mainnum; my \@st; my \@tr;
my (\$v, \$nf, \%forms);
while (my \$option=shift) {
  if (\$option eq "-no_sa") {\$showall=0}
  elsif (\$option eq "-vars") {\$withvars=1}
  elsif (\$option eq "-nosep") {\$nosep=1}
  elsif (\$option eq "-synt") {\$synt=1}
  elsif (\$option eq "-rads") {\$rads=1}
  elsif (\$option eq "-log") {\$log=1}
  elsif (\$option eq "-tag") {\$onetag=1}
  elsif (\$option eq "-onerad") {\$onerad=1}
}
my (\$w, \$r, \$v, \$cat, \$t, \$remainer, \$var, \$tag, \$class);
while (<>) {
  chomp;
  s/^#([^"].*|)\$//; next if (/^[ \\t]*\$/);
  /([^\\t]*)\\t([^\\t]*)((?:\\t.*)?)/ || die ("Bad input format for morpho.*.dir.pl : \\"\$_\\"\\n");

  \$w=\$1;
  \$class=\$2;
  \$remainer=\$3;
  if (\$onetag) {
    \$remainer=~s/^\\t([^\\t\\n]*)//;
    \$tag=\$1;
  } else {
    \$tag="";
  }
  \$mainword=0;

  \$var="___";
  if (\$w=~s/^([^\\t]*)(___.*)\$/\$1/) {\$var=&detranslitterate(\$2);}
  \$lastlemmaid{\$w.\$var}++;
  \@result=(); \@table=();
  \@tag=(); \@msprop=(); \@w=();
  if (\$class=~/[ \\+]/) {
    \@w=split(/ +/,\$w);
    \@v=(); \@cat=(); \@t=(); \@table=(); \@num2mainnum=();
    if (\$synt) {
      \@msprop=();
    }
    \$last_t="";
    for (split(/\\+/,\$class)) {
      if (\$last_t ne "NEXT_t") {
        if (\$_ ne "") {
          /^(([^-:\\t\\/\\d]+)[^:\\/]*)(:[^\\/]*)?(?:\\/([^\\/]*)(?:\\/([^\\/]*))?)?\$/;
          push(\@t,\$1);
          push(\@cat,\$2);
          push(\@v,\$3);
          push(\@st,\$4);
          push(\@tr,\$5);
        } else {
          \$last_t="NEXT_t";
        }
      } else {
        for my \$n (\$#t+1..\$#w-1) {
          push(\@t,"inv");
          push(\@v,"");
          push(\@cat,"inv");
          push(\@st,"");
          push(\@tr,"");
        }
        /^(([^-:\\t\\/\\d]+)[^:\\/]*)(:[^\\/]*)?(?:\\/([^\\/]*)(?:\\/([^\\/]*))?)?\$/;
        \$t[\$#w] = \$1;
        \$cat[\$#w] = \$2;
        \$v[\$#w] = \$3;
        \$st[\$#w] = \$4;
        \$tr[\$#w] = \$5;
      }
    }
    if (\$#w>\$#t) {
      for my \$n (\$#t+1..\$#w) {
        push(\@t,"inv");
        push(\@v,"");
        push(\@cat,"inv");
        push(\@st,"");
        push(\@tr,"");
      }
    }
    for my \$n (0..\$#w) {
      if (\$w[\$n]=~s/^{(.*)}/\$1/) {\$mainword=\$n}
    }
    for my \$n (0..\$#w) {
      if (\$t[\$n] ne "0") {
        inflect(\$w[\$n],\$n,\$cat[\$n],\$t[\$n],\$v[\$n],\$st[\$n],\$tr[\$n]);
      } elsif (\$mainword==\$n) {
        \$remainer=~s/^(\@[^ ]+)/\$1(\$w[\$n])/;
        \$remainer=~s/\\\$1/\$w[\$n]/g;
        if (\$synt) {
          \$msprop[\$mainword][0]="\\tDefault";
        }
      }
    }
  } else {
    \$w[0]=\$w;
    \$class =~ /^(([^-:\\t\\/\\d]+)[^:\\/]*)(:[^\\/]*)?(?:\\/([^\\/]*)(?:\\/([^\\/]*))?)?\$/;
    inflect(\$w,0,\$2,\$1,\$3,\$4,\$5);
  }
  if (\$withvars) {
    \$w.="\$var\\__".(\$lastlemmaid{\$w.\$var});
  }
  \$w=&detranslitterate(\$w);
  \$w=~s/ / /g;
  \$w=~s/{([^ ]+)}/\$1/;
  if (\$#w==0) {
    for my \$i (0..\$#{\$result[0]}) {
      print "\$w\$msprop[0][\$i]\\t\$result[0][\$i]\\t\$tag[0][\$i]\\t\$table[0][\$i]\$remainer\\n";
    }
  } else {
    \@output=();
    \$output[0]="";
    my \@temp=();
    my \@temp2=();
    for my \$i (0..\$#w) {
      if (\$t[\$i] ne "0") {
        \@temp=();
        for my \$j (0..\$#output) {
          \@temp2=();
          for my \$k (0..\$#{\$result[\$i]}) {
            push(\@temp,\$output[\$#output-\$j]." ".\$result[\$i][\$k]);
          }
          for my \$k (0..\$#{\$result[\$i]}) {
            if (\$i == \$mainword) {
              push(\@temp2,\$k);
            } else {
              push(\@temp2,\$num2mainnum[\$#output-\$j]);
            }
          }
          splice(\@num2mainnum,\$#output-\$j,1,\@temp2);
        }
        \@output=();
        for (\@temp) {push(\@output,\$_);}
      }
    }
    for my \$i (0..\$#output) {
      \$output[\$i]=~s/^ //;
      print "\$w\$msprop[\$mainword][\$num2mainnum[\$i]]\\t\$output[\$i]\\t\$tag[\$mainword][\$num2mainnum[\$i]]\\t\$table[\$mainword][\$num2mainnum[\$i]]\$remainer\\n";
    }
  }
}
print STDERR "  Inflecting lemmas...done      \\n";
END
print DIR inflect_function();
print DIR compute_stem_function();
print DIR push_result_function();
print DIR wrapfusion_function();
close DIR;




sub translitterate_data {
  for (keys %letterclass) {
    $str = $_;
    $str2 = $letterclass{$_};
    delete $letterclass{$str};
    $letterclass{translitterate($str)} = translitterate($str2);
  }
  for (keys %table) {
    $table{$_}{except} = apply_letter_classes (translitterate ($table{$_}{except})) unless $table{$_}{except} eq "__undef";
    $table{$_}{rads} = apply_letter_classes (translitterate ($table{$_}{rads})) unless $table{$_}{rads} eq "__undef";
  }
  for (keys %pattern) {
    $pattern{$_}{except} = apply_letter_classes (translitterate ($pattern{$_}{except})) unless $pattern{$_}{except} eq "__undef";
    $pattern{$_}{rads} = apply_letter_classes (translitterate ($pattern{$_}{rads})) unless $pattern{$_}{rads} eq "__undef";
  }
  for (keys %zone) {
    $zone{$_}{except} = apply_letter_classes (translitterate ($zone{$_}{except})) unless $zone{$_}{except} eq "__undef";
    $zone{$_}{rads} = apply_letter_classes (translitterate ($zone{$_}{rads})) unless $zone{$_}{rads} eq "__undef";
  }
  $form{$cur_zone}{$str}[$i]{except} = $str2;
  for my $zone (keys %form) {
    for my $tag (keys %{$form{$zone}}) {
      for my $i (0..$#{$form{$zone}{$tag}}) {
	$form{$zone}{$tag}[$i]{except} = apply_letter_classes (translitterate ($form{$zone}{$tag}[$i]{except})) unless $form{$zone}{$tag}[$i]{except} eq "__undef";
	$form{$zone}{$tag}[$i]{rads} = apply_letter_classes (translitterate ($form{$zone}{$tag}[$i]{rads})) unless $form{$zone}{$tag}[$i]{rads} eq "__undef";
	$form{$zone}{$tag}[$i]{prefix} = translitterate ($form{$zone}{$tag}[$i]{prefix});
	$form{$zone}{$tag}[$i]{suffix} = translitterate ($form{$zone}{$tag}[$i]{suffix});
      }
    }
  }
}

sub pfm2alexina {
  for my $zone (keys %zone) {
    my $table = $zone{$zone}{table};
    next unless defined ($pfm{$table});
    for my $mfs_set (keys %{$subzone{$zone}}) {
      my %mfs_set = ();
      # build %mfs_set
      for my $mfs (keys %mfs_set) {
	my $prefXsuff = "X";
	my $tmp;
	for my $block_id (0..$#{$pfm{$table}}) {
	  for my $rule_id (0..$#{$pfm{$table}[$block_id]}) {
	    next unless compatible_mfs ($pfm{$table}[$block_id]{mfs}, $mfs);
	    $tmp = $pfm{$table}[$block_id]{rule};
	    $tmp =~ s/X/$prefXsuff/;
	    $prefXsuff = $tmp;
	  }
	}
      }
    }
  }
}

sub compute_zone_inheritance {
  return if (scalar keys %zone_inheritance == 0);
  my $prev_inheriting_zone_number = -1;
  while (1) {
    error ("Loop in zone inheritance, involving zones (".(join (", ", sort keys %zone_inheritance)).")") if ($prev_inheriting_zone_number eq scalar keys %zone_inheritance);
    $prev_inheriting_zone_number = scalar keys %zone_inheritance;
  loop:
    for my $zone (keys %zone_inheritance) {
      for my $inherited_zone (keys %{$zone_inheritance{$zone}}) {
	next loop if defined ($zone_inheritance{$inherited_zone});
      }
      for my $inherited_zone (keys %{$zone_inheritance{$zone}}) {
	$zone{$zone}{canonical_tag} = $zone{$inherited_zone}{canonical_tag} unless defined($zone{$zone}{canonical_tag});
	for my $v1 (keys %{$zone_var2incompatiblevars{$inherited_zone}}) {
	  for my $v2 (keys %{$zone_var2incompatiblevars{$inherited_zone}{$v1}}) {
	    $zone_var2incompatiblevars{$zone}{$v1}{$v2} = 1;
	  }
	}
	my $tag;
	for $i (0..$#{$zone2tags{$inherited_zone}}) {
	  $tag = $zone2tags{$inherited_zone}[$i];
	  next unless check_tag ($tag, $zone_inheritance{$zone}{$inherited_zone}{only}, $zone_inheritance{$zone}{$inherited_zone}{except});
	  push @{$zone2tags{$zone}}, $tag;
	}
	for $tag (keys %{$form{$inherited_zone}}) {
	  next unless check_tag ($tag, $zone_inheritance{$zone}{$inherited_zone}{only}, $zone_inheritance{$zone}{$inherited_zone}{except});
	  next if defined($form{$zone}{$tag});
	  for $i (0..$#{$form{$inherited_zone}{$tag}}) {
	    for (keys %{$form{$inherited_zone}{$tag}[$i]}) {
	      $form{$zone}{$tag}[$i]{$_} = $form{$inherited_zone}{$tag}[$i]{$_};
	    }
	  }
	}
      }
      if (scalar keys %zone_inheritance == 1) {
	%zone_inheritance = ();
	return 1;
      } else {
	delete ($zone_inheritance{$zone});
      }
    }
  }
}

sub check_tag {
  my $tag = shift;
  my $only = shift;
  my $except = shift;
  my $ok_for_only = 0;
  if ($only ne "") {
    $only =~ s/^ //;
    $only =~ s/ $//;
    for my $t (split / /, $only) {
      if ($tag =~ /^$t/) {
	$ok_for_only = 1;
	last;
      }
    }
    return 0 if $ok_for_only == 0;
  }
  if ($except ne "") {
    $except =~ s/^ //;
    $except =~ s/ $//;
    for my $t (split / /, $except) {
      if ($tag =~ /^$t/) {
	return 0;
      }
    }
  }
  return 1;
}






sub translitterate_function {
  my $r = "";
  $r .= "sub translitterate {\n";
  $r .= "  my \$w=shift;\n";
  for (keys %translitteration) {
    $r .= "  \$w =~ s/$_/$translitteration{$_}/g;\n";
  }
  $r .= "  return \$w;\n";
  $r .= "}\n\n";
  return $r;
}

sub detranslitterate_function {
  my $r = "";
  $r .= "sub detranslitterate {\n";
  $r .= "  my \$w=shift;\n";
  for (keys %detranslitteration) {
    $r .= "  \$w =~ s/$_/$detranslitteration{$_}/g;\n";
  }
  $r .= "  return \$w;\n";
  $r .= "}\n\n";
  return $r;
}

sub sandhi_function {
  my $r = "";
  my $source;
  my $target;
  my $rank;
  $r .= <<END;
sub fusion {
  my \$s=shift;
  my \$gotfusion=1;
  my \$loopn=0;
  while (\$gotfusion==1 && \$s =~ / /) {
    \$gotfusion=0;
END
  for (0..$#sandhi_source) {
    next if ($sandhi_isfinal[$_] == 1);
    $source = sandhi_input_transform(translitterate($sandhi_source[$_]),1);
    $target = sandhi_output_transform(translitterate($sandhi_target[$_]),translitterate($sandhi_source[$_]));
    $rank = $_ + 1;
#    $r .= "    if (\$s =~ s/$source/$target/) {\$gotfusion = 1;} # rank = $rank\n";
    my $rule = "$sandhi_source[$_] -> $sandhi_target[$_]";
    $rule =~ s/([\$\"])/\\\1/g;
#    $r .= "    if (\$s =~ s/$source/$target/) {\$gotfusion = 1; print STDERR \"$rank\\t$rule\\n\"} # rank = $rank\n";
    $r .= "    if (\$s =~ s/$source/$target/) {\$gotfusion = 1;} # rank = $rank\n";
  }
  $r .= <<END;
  }
END
  for (0..$#sandhi_source) {
    next unless ($sandhi_isfinal[$_] == 1);
    $source = sandhi_input_transform(translitterate($sandhi_source[$_]),1);
    $target = sandhi_output_transform(translitterate($sandhi_target[$_]),translitterate($sandhi_source[$_]));
    $rank = $_ + 1;
#    $r .= "  if (\$s =~ s/$source/$target/) {\$gotfusion = 1;} # rank = $rank\n";
    my $rule = "$sandhi_source[$_] -> $sandhi_target[$_]";
    $rule =~ s/([\$\"])/\\\1/g;
#    $r .= "  if (\$s =~ s/$source/$target/) {\$gotfusion = 1; print STDERR \"$rank\\t$rule\\n\"} # rank = $rank\n";
    $r .= "  if (\$s =~ s/$source/$target/) {\$gotfusion = 1;} # rank = $rank\n";
  }
  $r .= <<END;
  return \$s;
}
END
  return $r;
}

sub sandhi_reverse_function {
  my $r = "";
  my $source;
  my $target;
  $r .= <<END;
sub rev_fusion {
  my \$orig=shift;
  my \$realorig=shift;
  \$_=\$orig;
  my \$level=shift;
  my \$result=shift;
  my \$pos=shift;
  my \$fusionok=shift;
  my \$firstrank=(shift || 0);
  if (\$_!~/ /) {die "Error: input of rev_fusion (\$_) must have a split mark"}
  if ((!defined(\$result->{\$_}) && (\$fusionok || (fusion(\$_."\\t") eq \$realorig."\\t"))) || \$result->{\$_}>\$pos) {
     / ([^ ]*)\$/;
     if (defined(\$terms{\$1})) {\$result->{\$_}=\$pos}
     \$fusionok=1;
  }
END
  my $rank;
  for (0..$#sandhi_source) {
    $rank = $#sandhi_source-$_;
    next unless ($sandhi_isfinal[$rank] == 1);
    next unless ($sandhi_isrev[$rank] == 1);
    $source = sandhi_input_transform(translitterate($sandhi_target[$rank]),0);
    $target = sandhi_output_transform(translitterate($sandhi_source[$rank]),translitterate($sandhi_target[$rank]));
    $r .= "  if (\$firstrank < ".($rank+1)." && s/$source/$target/) { # rank = ".($rank+1)."\n";
    $r .= <<END;
      \$locfusionok = (\$fusionok || (fusion(\$_."\\t") eq \$realorig."\\t"));
      if (\$locfusionok) {
        / ([^ ]*)\$/;
        if (defined(\$terms{\$1})) {\$result->{\$_}++}
      }
END
    $r .= "      if (\$level>1) {rev_fusion(\$_,\$realorig,\$level-1,\$result,\$pos,\$locfusionok,".($rank+1).")}\n";
    $r .= "      \$_=\$orig;\n";
    $r .= "  }\n";
  }
  for (0..$#sandhi_source) {
    $rank = $#sandhi_source-$_;
    next if ($sandhi_isfinal[$rank] == 1);
    next unless ($sandhi_isrev[$rank] == 1);
    $source = sandhi_input_transform(translitterate($sandhi_target[$rank]),0);
    $target = sandhi_output_transform(translitterate($sandhi_source[$rank]),translitterate($sandhi_target[$rank]));
    $r .= "  if (\$firstrank < ".($rank+1)." && s/$source/$target/) { # rank = ".($rank+1)."\n";
    $r .= <<END;
      \$locfusionok = (\$fusionok || (fusion(\$_."\\t") eq \$realorig."\\t"));
      if (\$locfusionok) {
        / ([^ ]*)\$/;
        if (defined(\$terms{\$1})) {\$result->{\$_}++}
      }
      if (\$level>1) {rev_fusion(\$_,\$realorig,\$level-1,\$result,\$pos,\$locfusionok,-1)}
      \$_=\$orig;
END
    $r .= "  }\n";
  }
  $r .= <<END;
  return 1;
}
END
  return $r;
}

sub compute_possible_splits_function {
  my $r = "";
  $r .= <<END;
sub compute_possible_splits {
  \$_ = shift;
  \$_ = translitterate(\$_);
  my \$orig = \$_;
  my \$possible_splits = shift;
  my \$length = length(\$orig);
  for \$pos (0..\$length) {
      substr (\$_,\$pos,0)=" ";
      rev_fusion (\$_,\$_,4,\$possible_splits,\$pos,0);
      \$_=\$orig;
  }
  my \$fus;
  for (keys \%\$possible_splits) {
    \$fus = remove_sep (fusion(\$_."\\t"));
    if (\$fus ne \$orig."\\t") {
       delete \$possible_splits->{\$_};
    }
  }
  return 1;
}
END
  return $r;
}

sub check_lemma_function {
  my $r = "";
  $r .= <<END;
sub check_lemma {
  my \$w=shift;
  chomp(\$w);
  \$w =~ s/ / /g;
  \$w=~/^([^\\t]*)\\t(([^-:\\t\\/\\d]+)[^:\\/]*)(:[^\\/]*)?(?:\\/([^\\/]*)(?:\\/([^\\/]*))?)?\$/ || die ("Bad input format for check_lemma : \\"\$w\\"");
  my \$r=\$1; my \$cat=\$3; my \$t=\$2; my \$v=\$4; my \$st=\$5; my \$tr=\$6;
  \$r=~s/___.*//;
  \$_=\$r;
  my \$locr;
  if (0) {
END
  for my $cat (keys %cat2patterns) {
    $r .= "  } elsif (\$cat eq \"$cat\") {\n";
    $r .= "    if (0) {\n";
    for my $pattern (keys %{$cat2patterns{$cat}}) {
      $r .= "    } elsif (\$t eq \"$pattern\") {\n";
      if ($pattern{$pattern}{rads} ne "__undef") {
	$r .= "      return 0 if \$r !~ /^".$pattern{$cur_pattern}{rads}."\$/;\n";
      }
      if ($pattern{$pattern}{except} ne "__undef") {
	$r .= "      return 0 if \$r =~ /^".$pattern{$cur_pattern}{except}."\$/;\n";
      }
      for my $subpattern_id (0..$#{$subpattern{$pattern}}) {
	my $zone = $subpattern{$pattern}[$subpattern_id]{zone_name};
	my $table = $zone{$zone}{table};
	my %conds = ();
	my $cond;
	$r .= "      # zone '$zone' (table '$table')\n";
	if ($table{$table}{rads} ne "__undef") {
	  $r .= "      return 0 if \$r !~ /^".$table{$table}{rads}."\$/;\n";
	}
	if ($table{$table}{except} ne "__undef") {
	  $r .= "      return 0 if \$r =~ /^".$table{$table}{except}."\$/;\n";
	}
	for my $tag (keys %{$form{$zone}}) {
	  my $has_nonconstrained_form = 0;
	  for $i (0..$#{$form{$zone}{$tag}}) {
	    if ($form{$zone}{$tag}[$i]{var} eq "" && $form{$zone}{$tag}[$i]{rads} eq "__undef" && $form{$zone}{$tag}[$i]{except} eq "__undef") {
	      $has_nonconstrained_form = 1;
	      last;
	    }
	  }
	  if ($has_nonconstrained_form == 0) {
	    my %subconds = ();
	    for $i (0..$#{$form{$zone}{$tag}}) {
	      if ($form{$zone}{$tag}[$i]{var} =~ /^\!(.*)$/) {
		$subconds{"$1\t$subpattern{$pattern}[$subpattern_id]{stem_name}\t$form{$zone}{$tag}[$i]{rads}\t$form{$zone}{$tag}[$i]{except}"} = 1;
	      } else {
		$subconds{join("|", keys %{$zone_var2incompatiblevars{$zone}{$form{$zone}{$tag}[$i]{var}}})."\t$subpattern{$pattern}[$subpattern_id]{stem_name}\t$form{$zone}{$tag}[$i]{rads}\t$form{$zone}{$tag}[$i]{except}"} = 1;
	      }
	    }
	    $cond = "";
	    for (sort keys %subconds) {
	      /^(.*?)\t(.*?)\t(.*?)\t(.*)$/;
	      if ($1 eq "") {
		$cond .= "	           || (1";
	      } else {
		$cond .= "	           || (\$v !~ /:($1)(:|\$)/";
	      }
	      $cond .= " && compute_stem(\$r,\"$2\",\$st,\"".$pattern{$pattern}{default_stem_table}."\") =~ /^$3\$/" unless ($3 eq "__undef");
	      $cond .= " && compute_stem(\$r,\"$2\",\$st,\"".$pattern{$pattern}{default_stem_table}."\") !~ /^$4\$/" unless ($4 eq "__undef");
	      $cond .= ")\n";
	    }
	    $cond =~ s/^\s*\|\| //;
	    $cond =~ s/\(1 && /(/g;
	    $cond =~ s/\n$//;
	    if (!defined($conds{$cond})) {
	      $r .= "      #   tag $tag\n";
	      $r .= "      return 0 unless $cond;\n";
	      $conds{$cond} = 1;
	    }
	  }
	}
      }
    }
    $r .= "    }\n";
  }
  $r .= <<END;
  }
  return 1;
}
END
  $r =~ s/if \(0\) {\n\s*} els//g;
  return $r;
}

sub sortvlist_function {
  my $r = "";
  $r .= <<END;
sub sortvlist {
    my \$vl=shift;
    \$vl=~s/^://;
    if (\$vl eq "") {return ""}
    \$vl=":".join(":",sort split(/:/,\$vl));
    while (\$vl=~s/(:[^:]+)\$1/\$1/) {}
    return \$vl;
}
END
  return $r;
}


sub remove_sep_function {
  my $r = "";
  $r .= <<END;
sub remove_sep {
  my \$r=shift;
  \$r =~ s/ //g;
  return \$r;
}
END
  return $r;
}


sub compute_canonical_forms_function {
  my $r = "";
  my ($canonical_tag, $canonical_tag_zone);
  $r .= <<END;
sub compute_canonical_forms {
  my \$r=shift;
  my \$cat=shift;
  my \$t=shift;
  my \$v=shift;
  my \@result;
  my \$pushedsomething;
  my \$newr;
  if (\$v =~ /^[^:]/) {\$v=":".\$v}
  my \$oldv=\$v;
  if (0) {
END
  for my $cat (keys %cat2patterns) {
    $r .= "  } elsif (\$cat eq \"$cat\") {\n";
    $r .= "    if (0) {\n";
    for my $pattern (keys %{$cat2patterns{$cat}}) {
      $canonical_tag = $pattern{$pattern}{canonical_tag};
      if ($canonical_tag eq "__undef") {
	$canonical_tag_zone = $subpattern{$pattern}[0]{zone_name};
	if ($zone{$canonical_tag_zone}{canonical_tag} ne "__undef") {
	  $canonical_tag = $zone{$canonical_tag_zone}{canonical_tag};
	} elsif ($#{$zone2tags{$canonical_tag_zone}} >= 0) {
	  $canonical_tag = $zone2tags{$canonical_tag_zone}[0];
	} else {
	  error ("Zone '$canonical_tag_zone' is empty ($pattern/$canonical_tag/$canonical_tag_zone)");
	}
      } else {
	for (0..$#{$subpattern{$pattern}}) {
	  if (defined($form{$subpattern{$pattern}[$_]{zone_name}}{$canonical_tag})) {
	    $canonical_tag_zone = $subpattern{$pattern}[$_]{zone_name};
	  }
	}
      }
      $r .= <<END;
    } elsif (\$t eq "$pattern") {
END
      for $i (0..$#{$form{$canonical_tag_zone}{$canonical_tag}}) {
	$str = $form{$canonical_tag_zone}{$canonical_tag}[$i]{var};
	$str2 = "";
	if ($str ne "") {
	  if ($str =~ /^\!(.*)$/) {
	    $r .= "      if (\$v !~ /:$1(:|\$)/) {\n";
	  } else {
	    $r .= "      if (\$v =~ /:$str(:|\$)/) {\n";
	  }
	  $str2 = "  ";
	}
	$r .= <<END;
$str2      \$newr = "$form{$canonical_tag_zone}{$canonical_tag}[$i]{prefix} ".\$r." $form{$canonical_tag_zone}{$canonical_tag}[$i]{suffix}\\t$canonical_tag";
$str2      \$newr = fusion(\$newr);
$str2      \$newr =~ s/\\t.*\$//;
$str2      push \@result, "\$newr\\t\$t\$v";
END
	if ($str ne "") {
	  $r .= "      }\n";
	}
      }
      $r .= <<END;
      return \@result;
END
    }
    $r .= "    }\n";
  }
  $r .= "  }\n";
  $r .= <<END;
  return \@result;
}
END
  $r =~ s/if \(0\) {\n\s*} els//g;
  return $r;
}

sub try_to_lemmatize_function {
  my $r = "";
  $r .= <<END;
sub try_to_lemmatize {
  my \$w=shift;
  my \$cat=shift;
  my \$table=shift;
  my \$v=shift;
  my \$st=shift;
  my \$tr=shift;
  my \$o=\$w;
  \@possible_results=();
  if (0) {
END
  my ($canonical_tag, $canonical_tag_zone);
  for my $cat (keys %cat2patterns) {
    $r .= "  } elsif (\$cat eq \"$cat\") {\n";
    $r .= "    if (0) {\n";
    for my $pattern (keys %{$cat2patterns{$cat}}) {
      $canonical_tag = $pattern{$pattern}{canonical_tag};
      if ($canonical_tag eq "__undef") {
	$canonical_tag_zone = $subpattern{$pattern}[0]{zone_name};
	if ($zone{$canonical_tag_zone}{canonical_tag} ne "__undef") {
	  $canonical_tag = $zone{$canonical_tag_zone}{canonical_tag};
	} elsif ($#{$zone2tags{$canonical_tag_zone}} >= 0) {
	  $canonical_tag = $zone2tags{$canonical_tag_zone}[0];
	} else {
	  error ("Zone '$canonical_tag_zone' is empty ($pattern/$canonical_tag/$canonical_tag_zone)");
	}
      } else {
	for (0..$#{$subpattern{$pattern}}) {
	  if (defined($form{$subpattern{$pattern}[$_]{zone_name}}{$canonical_tag})) {
	    $canonical_tag_zone = $subpattern{$pattern}[$_]{zone_name};
	  }
	}
      }
      $r .= <<END;
    } elsif (\$table eq "$pattern") { # pattern = $pattern ; canonical_tag = $canonical_tag ; canonical_tag_zone = $canonical_tag_zone
END
      for $i (0..$#{$form{$canonical_tag_zone}{$canonical_tag}}) {
	$str = $form{$canonical_tag_zone}{$canonical_tag}[$i]{var};
	$str2 = "";
	if ($str ne "") {
	  if ($str =~ /^\!(.*)$/) {
	    $r .= "      if (\$v !~ /:($1)(:|\$)/) {\n";
	  } else {
	    $r .= "      if (\$v !~ /:(".join("|", keys %{$zone_var2incompatiblevars{$canonical_tag_zone}{$str}}).")(:|\$)/) {\n";
	  }
	  $str2 = "  ";
	}
	$r .= "$str2      # form_rads = $form{$canonical_tag_zone}{$canonical_tag}[$i]{rads}\n";
	if ($form{$canonical_tag_zone}{$canonical_tag}[$i]{rads} ne "__undef") {
	  $r .= "$str2      if (\$w =~ s/^(".$form{$canonical_tag_zone}{$canonical_tag}[$i]{rads}.") ".quotemeta($form{$canonical_tag_zone}{$canonical_tag}[$i]{suffix})."\$/\$1\\t$pattern/) {\n";
	} else {
	  $r .= "$str2      if (\$w =~ s/ ".quotemeta(translitterate($form{$canonical_tag_zone}{$canonical_tag}[$i]{suffix}))."\$/\\t$pattern/) {\n";
	}
	my $table_except = ($table{$zone{$canonical_tag_zone}{table}}{except} ne "__undef");
	my $zone_except = ($zone{$canonical_tag_zone}{except} ne "__undef");
	my $form_except = ($form{$canonical_tag_zone}{$canonical_tag}[$i]{except} ne "__undef");
	$r .= "$str2        # table_except = $table_except (".$table{$zone{$canonical_tag_zone}{table}}{except}.")\n";
	$r .= "$str2        # zone_except = $zone_except (".$zone{$canonical_tag_zone}{except}.")\n";
	$r .= "$str2        # form_except = $form_except (".$form{$canonical_tag_zone}{$canonical_tag}[$i]{except}.")\n";
#	$r .= "$str2        push(\@possible_results,\"\$w\".sortvlist(\$v.\"\")) if check_lemma(\"\$w\".sortvlist(\$v.\"\").(\$st.\$tr eq \"\" ? \"\" : \"/\$st\").(\$tr eq \"\" ? \"\" : \"/\$tr\"))";
	$r .= "$str2        check_and_push(\\\@possible_results,\"\$w\".sortvlist(\$v.\"\").(\$st.\$tr eq \"\" ? \"\" : \"/\$st\").(\$tr eq \"\" ? \"\" : \"/\$tr\"))";
	$r .= " unless (" if ($table_except || $zone_except || $form_except);
	$r .= "\$w =~ /^".$table{$zone{$canonical_tag_zone}{table}}{except}."\\t/" if ($table_except);
	$r .= " && " if ($table_except && $zone_except);
	$r .= "\$w =~ /^".$zone{$canonical_tag_zone}{except}."\\t/" if ($zone_except);
	$r .= " && " if (($zone_except || $table_except) && $form_except);
	$r .= "\$w =~ /^".$form{$canonical_tag_zone}{$canonical_tag}[$i]{except}."\\t/" if ($form_except);
	$r .= ")" if ($table_except || $zone_except || $form_except);
	$r .= ";\n";
	$r .= "$str2        \$w = \$o;\n";
	$r .= "$str2      }\n";
	if ($str ne "") {
	  $r .= "      }\n";
	}
      }
    }
    $r .= "    }\n";
  }
  $r .= "  }\n";
  $r .= <<END;
  return \@possible_results;
}

sub check_and_push {
  my \$array_ref = shift;
  my \$str = shift;
  push(\@\$array_ref,\$str) if check_lemma(\$str);
}

END
  $r =~ s/if \(0\) {\n\s*} els//g;
  return $r;
}


sub lemmatize_function {
  my $r = "";
  $r .= <<END;
sub lemmatize {
  my \$w=shift;
  my \$cat=shift;
  my \$t=shift;
  my \$v=shift;
  my \$st=shift;
  my \$tr=shift;
  my \$rads=shift;
  my \$die_if_pb=shift;
  my \@possible_lemmas=();
  my \$length;
  if (!\$rads) {
    my \%possible_splits=();
    my \$origw=\$w;
    my \$lnumber=0;
    for \$pos (0..length(\$origw)) {
# this line takes more and more time on big files on Mac OS X 10.5 and higher; replaced by the 2 following lines
#        substr(\$w,length(\$origw)-\$pos,0)=\" \";
        \$length = length(\$origw)-\$pos;
        \$w =~ s/^(.{\$length})/\\1 /;
        \$w = translitterate(\$w);
        rev_fusion(\$w,\$w,2,\\\%possible_splits,\$pos,0);
        for (keys \%possible_splits) {
            \@_ = try_to_lemmatize(\$_,\$cat,\$t,\$v,\$st,\$tr);
            if (\$#_ >= 0) {push(\@possible_lemmas,\@_);}
        }
        \$lnumber=\$#possible_lemmas+1;
        if (\$lnumber >= 1) {last;} # \$w=~s/^([^\t]+):(?=.)/\$1/;
        else {\$w = \$origw;\%possible_splits=();}
    }
    if (\$lnumber==0) {
      if (\$die_if_pb) {return detranslitterate(\$origw)."\t<error[\$lnumber]>\\n";}
    }
  } else {
    push(\@possible_lemmas,\$w."\\t".\$t);
  }
  return \@possible_lemmas;
}
END
  return $r;
}


sub inflect_function {
  my $r = "";
  $r .= <<END;
sub inflect {
  my \$w = shift;
  my \$origw = \$w;
  \$w =~ s/ / /g;
  my \$ccnum = shift;
  my \$cat = shift;
  my \$t = shift;
  my \$v = shift;
  my \$st = shift;
  my \$tr = shift;
  \$l++;
  if (\$l % 100 == 0) {print STDERR "  Inflecting lemmas...\$l\\r"}
  if (\$log) {print STDERR "Inflecting (\$w,\$ccnum,\$cat,\$t,\$v)\\n"}
  my \$lemmas = ();
  for \$w (lemmatize(\$w,\$cat,\$t,\$v,\$st,\$tr,\$rads,1,\\\$lemmas)) {
    print STDERR "  Stem: \$w\\n" if (\$log);
    if (\$w =~ /<error/) {
        if (\$log) {print STDERR "No appropriate radical found\\n"}
        print detranslitterate(\$w);
        return;
    }
    if (\$log) {print STDERR "Using radical (\$w)\\n"}
    if (!check_lemma(\$w.(\$st.\$tr eq \"\" ? \"\" : \"/\$st\").(\$tr eq \"\" ? \"\" : \"/\$tr\"))) {
        if (\$log) {print STDERR "Incorrect radical\\n"}
        print detranslitterate(\$w)."\\t<error>\\n";
        return;
    }
    \$nf = 0;
    \$w =~ /^(.*)\\t([^:\\t\\/]+)([^\\/]*)/ || die ("Error in lemma format : \\"\$w\\"\\n");
    \$r = \$1; \$t = \$2; \$v = \$3; \$_ = \$r;
  
    if (0) {
END
  for my $cat (keys %cat2patterns) {
    $r .= "    } elsif (\$cat eq \"$cat\") {\n";
    $r .= "      if (0) {\n";
    for my $pattern (keys %{$cat2patterns{$cat}}) {
      $r .= "      } elsif (\$t eq \"$pattern\") {\n";
      for my $subpattern_id (0..$#{$subpattern{$pattern}}) {
	my $zone = $subpattern{$pattern}[$subpattern_id]{zone_name};
	$r .= "        # zone '$zone'\n";
	for my $tag (keys %{$form{$zone}}) {
	  $r .= "        if (\$tag eq \"\" || \$tag eq \"$tag\") {\n";
	  for $i (0..$#{$form{$zone}{$tag}}) {
	    $r .= "          if (";
	    if (defined($form{$zone}{$tag}[$i]{show})) {
	      $r .= "1";
	    } else {
	      $r .= "\$showall";
	    }
	    if ($form{$zone}{$tag}[$i]{var} ne "") {
	      if ($form{$zone}{$tag}[$i]{var} =~ /^\!(.*)$/) {
		$r .= " && \$v !~ /:($1)(:|\$)/";
	      } else {
		$r .= " && \$v !~ /:(".join("|", keys %{$zone_var2incompatiblevars{$zone}{$form{$zone}{$tag}[$i]{var}}}).")(:|\$)/";
	      }
	    }
	    $r .= " && \$r !~ /^".$form{$zone}{$tag}[$i]{except}."\$/" unless $form{$zone}{$tag}[$i]{except} eq "__undef";
	    $r .= " && \$r =~ /^".$form{$zone}{$tag}[$i]{rads}."\$/" unless $form{$zone}{$tag}[$i]{rads} eq "__undef";
	    $r .= ") {\n";
	    $r .= "            \$r = \"".$form{$zone}{$tag}[$i]{prefix}." \".compute_stem(\$r,\"".$subpattern{$pattern}[$subpattern_id]{stem_name}."\",\$st,\"".$pattern{$pattern}{default_stem_table}."\").\" ".$form{$zone}{$tag}[$i]{suffix}."\\t$tag\";\n";
	    if (defined($form{$zone}{$tag}[$i]{show}) && $form{$zone}{$tag}[$i]{show} ne "") {
	      $r .= "            if (\$showall) {\n";
	      $r .= "              \$r = wrapfusion(\$r);\n";
	      $r .= "            } else {\n";
	      $r .= "              \$r =~ s/\\t.*//;\n";
	      $r .= "              \$r = remove_sep(\$r);\n";
	      if ($form{$zone}{$tag}[$i]{show} = /^(.*)#(.*)$/) {
		$r .= "              \$forms{\"$1\".\$r.\"$2\"} = 1;\n";
	      } else {
		$r .= "            \$forms{\"".$form{$zone}{$tag}[$i]{show}."\"} = 1;\n";
	      }
	      $r .= "            }\n";
	    } else {
	      $r .= "            \$r = wrapfusion(\$r);\n";
	    }
	    $r .= "            push_result (\$ccnum,\"".$form{$zone}{$tag}[$i]{mstag}."\",\"xxx\",\$_,\$r,\"\$t\$v\");\n";
	    $r .= "            \$r = \$_;\n";
	    $r .= "            \$nf++;\n";
	    $r .= "          }\n";
	  }
	  $r .= "        }\n";
	}
      }
    }
    $r .= "      }\n";
  }
  $r .= <<END;
    }

    if (\$nf==0 && \$w=~/^([^\\t]*)\\t([^\\t]*)\$/) {
      if (\$showall) {
        push_result(\$ccnum,"?","?","?","?	?","?")
      } else {
        print ", ";
      }
    }
    my \$out;
    if (!\$showall) {
      \$out =detranslitterate(join(", ",sort {\$forms{\$a} <=> \$forms{\$b} || \$a cmp \$b} keys \%forms));
      \$out =~ s/, *(?=,)/ /g;
      while (\$out =~ s/^ *, *//g) {}
      \$out =~ s/  +/ /g;
      \$out =~ s/^ //g;
      \$out =~ s/ \$//g;
      print "\$out\\n";
      %forms=();
    }
    last if (\$onerad);
  }
}
END
  $r =~ s/if \(1 && /if (/g;
  $r =~ s/if \(0\) {\n\s*} els//g;
  return $r;
}


sub compute_stem_function {
  my $r = "";
  $r .= <<END;
sub compute_stem {
  my \$r = shift;
  my \$stem_name = shift;
  my \$stem_table = shift;
  my \$default_stem_table = shift;
  \$r =~ s/-/#=#=#/g;
  if (\$stem_table eq "") {\$stem_table = \$default_stem_table}
  elsif (\$stem_table =~ /^:/) {\$stem_table = \$default_stem_table.\$stem_table}
  my \$orig_stem_table = \$stem_table;
  my \@known_rads = ();
  if (\$stem_table =~ s/:(.*)\$//) {
    my \$tmp = \$1;
    \@known_rads = split /\\s*,\\s*/, \$tmp;
  }
  if (\$stem_name ne "") {
    if (0) {
END
  for my $stem_table (keys %stem) {
    $r .= "    } elsif (\$stem_table eq \"$stem_table\") {\n";
    $r .= "      if (0) {\n";
    for my $stem_id (0..$#{$stems{$stem_table}}) {
      my $stem_name = $stems{$stem_table}[$stem_id];
      $r .= "      } elsif (\$stem_name eq \"$stem_name\") {\n";
      $r .= "        if (\$known_rads\[$stem_id] ne \"\") {\n";
      $r .= "          \$r = \$known_rads\[$stem_id];\n";
      if ($stem{$stem_table}{$stem_name}{operation} eq "__undef") {
	$r .= "        } else {\n";
	$r .= "          die \"ERROR: Stem table \$stem_table does not provide a default \$stem_name construction rule. Any lexical entry using stem table \$stem_table must provide explicitely its \$stem_name stem\";\n";
      } else {
	$r .= "        } else {\n";
	if (defined($stem{$stem_table}{$stem_name}{source})) {
	  $r .= "          \$r = compute_stem(\$r, \"".$stem{$stem_table}{$stem_name}{source}."\", \$orig_stem_table);\n";
	  $r .= "          \$r =~ s/-/#=#=#/g;\n";
	}
	if ($stem{$stem_table}{$stem_name}{operation} ne "") {
	  $r .= "          \$r = \$r.\"".$stem{$stem_table}{$stem_name}{operation}."\";\n";
	  #	$r .= "          while (\$r =~ s/^(.*?)([^-+]+)-\\2(?:\\+([^-+\\|]+))?(\\|.*|\$)/\$1\$3/ || \$r =~ s/^(.*?)-[^-+]+(?:\\+[^-+\\|]+)?(\\||\$)/\$1/) {}\n";
	  #	$r .= "          \$r =~ s/^(.*)(?:\\+([^-+\\|]+))?.*\$/\$1\$2/;\n";
	  $r .= "          while (\$r =~ s/^(.*?)([^-+]+)-\\2(?:\\+([^-+\\|]+))?(\\|.*|\$)/\$1 \$3/ || \$r =~ s/^(.*?)-[^-+]+(?:\\+[^-+\\|]+)?(\\||\$)/\$1/) {}\n";
	  $r .= "          \$r =~ s/^(.*?)\\+([^-+\\|]+).*\$/\$1 \$2/;\n";
	  $r .= "          \$r = fusion (\$r);\n";
	  $r .= "          \$r =~ s/ //g;\n";
	}
      }
      $r .= "        }\n";
    }
    $r .= "      }\n";
  }
  $r .= <<END;
    }
  }
  \$r =~ s/#=#=#/-/g;
  return \$r;
}
END
  $r =~ s/if \(0\) {\n\s*} els//g;
  $r =~ s/(^|\n)\s*if \(0\) {\n\s*}\s*\n/\1/g;
  $r =~ s/(^|\n)\s*if \(0\) {\n\s*}\s*\n/\1/g;
  return $r;
}

sub push_result_function {
  my $r = "";
  $r .= <<END;
sub push_result {
    my \$ccnum=shift;
    my \$msprop=shift;
    my \$in=shift;
    my \$orig=shift;
    my \$result=shift;
    my \$table=shift;
    if (\$nosep) {
       \$result=&remove_sep(\$result);
    }
    if (\$synt) {
      push(\@{\$msprop[\$ccnum]},"\\t".\$msprop);
    }
    \$result = detranslitterate(\$result);
    \$result =~ /^(.*)\\t(.*)\$/;
    push(\@{\$result[\$ccnum]}, \$1);
    push(\@{\$tag[\$ccnum]}, \$2);
    push(\@{\$table[\$ccnum]}, \$table);
}
END
  return $r;
}

sub wrapfusion_function {
  my $r = "";
  $r .= <<END;
sub wrapfusion {
    my \$r = shift;
    if (\$log) {print STDERR "Before fusion (\$r)\n"}
    \$r = fusion(\$r);
    if (\$log) {print STDERR "After fusion (\$r)\n"}
    return \$r;
}
END
  return $r;
}














sub apply_letter_classes {
  my $s = shift;
  my ($c, $cdef);
  while ($s =~ /\\(.)/) {
    $c = $1;
    error ("Undefined letter class \\$c") unless defined($letterclass{$c});
    $cdef = $letterclass{$c};
    if ($cdef =~ /([^ ][^ ]+)/) {
      error ("Letter class \\$c contains non-single letters (e.g., $1)");
    }
    $cdef =~ s/ //g;
    $cdef = "[$cdef]";
    $c = quotemeta($c);
    $s =~ s/\\$c/$cdef/g;
  }
  while ($s =~ /\[:(.*?):\]/) {
    $c = $1;
    error ("Undefined letter class [:$c:]") unless defined($letterclass{$c});
    $cdef = $letterclass{$c};
    if ($cdef =~ /([^ ][^ ]+)/) {
      error ("Letter class \\$c contains non-single letters (e.g., $1)");
    }
    $cdef =~ s/ //g;
    $cdef = "[$cdef]";
    $c = quotemeta($c);
    $s =~ s/\[:$c:\]/$cdef/g;
  }
  return $s;
}

sub sandhi_input_transform {
  my $s = shift;
  my $bool = shift || 0;
  my $c;
  my $cdef;
  my $o;
  $s =~ s/\\\*/---anything---/g;
  while ($s =~ /\\(.)([\+\*\?]?)/) {
    $c = $1;
    $o = $2;
    error ("Undefined letter class \\$c") unless defined($letterclass{$c});
    $cdef = $letterclass{$c};
    if ($cdef =~ /([^ ][^ ]+)/) {
      error ("Letter class \\$c contains non-single letters (e.g., $1)");
    }
    $cdef =~ s/ //g;
    $cdef = "([$cdef]$o)";
    $c = quotemeta($c);
    $o = quotemeta($o);
    $s =~ s/\\$c$o/$cdef/g;
  }
  while ($s =~ /\[:(.*?):\]([\+\*\?]?)/) {
    $c = $1;
    $o = $2;
    error ("Undefined letter class [:$c:]") unless defined($letterclass{$c});
    $cdef = $letterclass{$c};
    if ($cdef =~ /([^ ][^ ]+)/) {
      error ("Letter class \\$c contains non-single letters (e.g., $1)");
    }
    $cdef =~ s/ //g;
    $cdef = "([$cdef]$o)";
    $c = quotemeta($c);
    $o = quotemeta($o);
    $s =~ s/\[:$c:\]$o/$cdef/g;
  }
  if ($bool) {
    $s =~ s/\$$/(?=\\t)/;
    $s =~ s/^\^/(?<=\\t)/;
  }
  $s =~ s/(?<=.)\^/\\\^/g;
  $s =~ s/_/ /g;
  $s =~ s/---anything---/([^\\t])/g;
  return $s;
}

sub sandhi_output_transform {
  my $s = shift; # the output
  my $i = shift; # the corresponding input, for dealing with pairs
  my $orig_s = $s;
  my $orig_i = $i;
  my $i_input_class;
  my $s_input_class;
  my $i_op;
  my $s_op;
  my $n = 1;
  while ($i =~ s/^(.*?)(?:\\(.)|\[:(.*?):\])([\+\*\?]?)//) {
    $i_input_class = $2.$3;
    $i_op = $4;
    $i_input_class =~ s/ //g;
    $s =~ /^(.*?)(?:\\(.)|\[:(.*?):\])([\+\*\?]?)/ || die "Invalid sandhi rule $i <-> $s (number of classes do not match)";
    $s_input_class = $2.$3;
    $s_op = $4;
    die "Invalid sandhi rule $i <-> $s (operators do not match)" if ($i_op ne $s_op);
    $i_op = quotemeta($i_op);
    $s_input_class =~ s/ //g;
    if ($i_input_class eq $s_input_class) {
      $s =~ s/^(.*?)(?:\\(.)|\[:.*?:\])$i_op/$1\$$n/;
    } else {
      die "Invalid sandhi rule $i <-> $s (regular operator on paired classes)" if ($s_op ne "");
      $s =~ s/^(.*?)(?:\\(.)|\[:.*?:\])/$1\$${i_input_class}2${s_input_class}\{\$$n\}/;
    }
    $n++
  }
  die "Invalid sandhi rule $i <-> $s" if ($s =~ /^(.*?)(?:\\(.)|\[:.*?:\])/);
  $s =~ s/\$$//;
  $s =~ s/^\^//;
  $s =~ s/_/ /g;
  return $s;
}

sub translitterate {
  my $w=shift;
  for (keys %translitteration) {
    $w =~ s/$_/$translitteration{$_}/g;
  }
  return $w;
}

sub detranslitterate {
  my $w=shift;
  for (keys %detranslitteration) {
    $w =~ s/$_/$detranslitteration{$_}/g;
  }
  return $w;
}




sub error {
  my $err_msg = shift;
  die "### ERROR line $line: $err_msg";
}
