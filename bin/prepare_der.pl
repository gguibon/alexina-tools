#!/usr/bin/env perl

$sep = " ";

while (<>) {
  $l++;
  chomp;
  s/^#.*$//;
  s/[ \t]*$//;
  next if (/^\s*$/);
  if (/^([^\t>][^\t]*|)\t([^\t]+)(?:\t([^\t]+).*)?$/) {
    $lemme=$1;
    $table=$2;
    $synt=$3;
    $synt =~ s/^(.*);.*$/\1/;
    $lemmevar="___";
    if ($lemme=~s/(___.*)$//) {
      $lemmevar=$1;
    }
    $lastlemmaid{$lemme.$lemmevar}++;
    $level=0;
    @functor=(); @revfunctor=();
    #		    $out=$out."\t$lemme$lemmevar\__".($lastlemmaid{$lemme.$lemmevar})."\t$out_tag";
  } elsif (/^>+\t([^\t]*)$/) {
    $functor=$1;
    $newlevel=0;
    while (s/^>//) {
      $newlevel++;
    }
    if ($newlevel>$level+1) {
      die ("Error line $l: level $newlevel derivation comes just after level $level derivation\n");
    }
    if ($newlevel<=$level) {
      for ($newlevel..$level) {
	delete($functor[$_-1]);
      }
    }
    $level=$newlevel;
    $functor[$level-1]=$functor;
    @revfunctor=reverse(@functor);
    print "$lemme$lemmevar\__".($lastlemmaid{$lemme.$lemmevar})."\t$table\t".join("@@",@revfunctor)."\t$synt;%%".join("+%%",@revfunctor)."\n";
  } else {
    die "failed\n###### Error line $l in ilex file: invalid line\n";
  }
}
