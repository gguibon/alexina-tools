#!/usr/bin/env perl

use strict;
use open ':utf8';
use utf8;

my $tables_file=shift;
#my $lemmes_file=shift;
#use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $sep=" "; # $sep=""
my $dfai=0;
my $output_dir="";
while (1) {
    $_=shift;
    if ($_!~/^\-/) {last;}
    elsif (/^--?od=(.+)$/ || /^--?output_dir=(.+)$/i) {$output_dir=$1;}
#    elsif (/^-s$/ || /^--?separator$/i) {$sep=" ";}
    elsif (/^-d$/ || /^--?derived_forms_are_inflexions$/i) {$dfai=1;}
    else {die "unknown option : $_"}
}
if ($output_dir=~/[^\/]$/) {$output_dir.="/"}

print STDERR "  Parsing morphological description...";

my $l=0;
my ($currtable, %tag_suffix, %fast);
my (%canonical_tag,%first_tag);
my %output=();
my @fusionarray;
my %derivations=();
my %rads=();
my %radsexcept=();
my %var=();
my %exclvars=();
my %locrads=();
my %locradsexcept=();
my %ftable=(); my %except;
my %letters=();
my %class=();
my ($in, $out);
my ($rads, $radsexcept);
my ($tag,$lasttag);
my $variant="";
my $cl;
my $show;
my $synt;
my $name;
my (%show, %synt, %req);
my %name;
my %show_pattern=();
my $liken=0;
my $t1; my $t2; my $t3; my $t4; my $t5; my $t6; my $m2; my $m4;
my $shownum;
my $fusionrank;
my %translitt;
my ($curr_l1, $curr_l2);
my %pairs; my $in_pairs;
my %formlike;

my $alt;
my %tempvars=();

open(TABLES_F,"<$tables_file") || die "########## Could not find/open $tables_file.\n";
binmode TABLES_F, ":utf8";
while (<TABLES_F>) {
    chomp;
    s/<!--.*?-->//g;
    s/^#.*//;
    next if (/^\s*$/);
    s/^\s+//; s/\s+$//;
    if (/^\s*<translitteration\s+source=\"(.+)\"\s+target=\"(.+)\"\/>$/) { # les translitterations pour que une lettre soit un caractere
      $translitt{$1}=$2;
    }
}
close(TABLES_F);

open(TABLES_F,"<$tables_file") || die "########## Could not find/open $tables_file.";
binmode TABLES_F, ":utf8";
while (<TABLES_F>) {
    $l++;
    chomp;
    s/<!--.*?-->//g;
    s/^#.*//;
    next if (/^\s*$/);
    s/^\s+//; s/\s+$//;
    if (/^<\?/ || /^<\/?description/) {
    } elsif (/^<table\s+name=\"(.+?)\"(?:\s+canonical_tag=(\".*?\"))?(?:\s+tag_suffix=\"(.*?)\")?(?:\s+rads=(\".*?\"))?(?:\s+except=(\".*?\"))?(?:\s+fast=\"(.*?)\")?>$/) { # début de nouvelle table
      if ($currtable ne "") {die "failed\n###### Error line $l: table $currtable not closed by </table>";}
      $currtable=my_quotemeta($1);
      $tag_suffix{$currtable}=my_quotemeta($3);
      $canonical_tag{$currtable}=unquote(my_quotemeta($2),"##NONE##");
      if ($canonical_tag{$currtable} ne "##NONE##") {$canonical_tag{$currtable}.=$tag_suffix{$currtable}}
      $rads{$currtable}=&normalize_classes(unquote(&dotranslitterate($4),".*"));
      $radsexcept{$currtable}=&normalize_classes(unquote(&dotranslitterate($5),"##NONE##"));
      $fast{$currtable}=$6;
      $first_tag{$currtable}="##NONE##";
      $shownum=2;
    } elsif (/^<\/table>$/) {
      if ($currtable eq "") {die "failed\n###### Error line $l: </table> doesn't match any <table>";}
      $currtable="";
    } elsif (/^<alt>$/) {
      die "failed\n###### Error line $l: embedded <alt> elements" if $alt;
      $alt=1;
      %tempvars=();
    } elsif (/^\s*<\/alt>$/) {
      die "failed\n###### Error line $l: </alt> doesn't match any <alt>" unless $alt;
      for $t1 (keys %tempvars) {
	for $t2 (keys %tempvars) {
	  if ($t1 ne $t2) {
	    $exclvars{$currtable}{$t1}{$t2}=1; # mutually excluding variants
	  }
	}
      }
      $alt=0;
    } elsif (/^<form\s+like=\"(.*?)\"\s+tag=\"(.*?)\"(?:\s+show=\"(.*?)\")?\/>$/) {
      # forme fléchie
      my $like=my_quotemeta($1).$tag_suffix{$currtable}; $tag=my_quotemeta($2).$tag_suffix{$currtable}; $show=$3;
      $formlike{$currtable}{$tag}{$like}=$show;
      $lasttag=$tag;
      die "failed\n###### Error line $l: no like element in an <alt>" if ($alt);
    } elsif (/^<form(?:\s+prefix=\"(.*?)\")?(?:\s+suffix=\"(.*?)\")?\s+tag=\"(.*?)\"(?:\s+rads=(\".*?\"))?(?:\s+except=(\".*?\"))?(?:\s+var=\"(.*?)\")?(?:\s+synt=\"(.*?)\")?(?:\s+show=\"(.*?)\")?(?:\s+req=\"(.*?)\")?\/>$/) {
      # forme fléchie
      $tag=my_quotemeta($3).$tag_suffix{$currtable}; $variant=my_quotemeta($6); $show=$8;
      if ($first_tag{$currtable} eq "##NONE##") {$first_tag{$currtable}=$tag;};
      die "failed\n###### Error line $l: any form in an <alt> must have a non-empty var name" if ($alt && $variant eq "");
      push(@{$output{$currtable}{$tag}{$variant}},&normalize_classes(my_quotemeta(dotranslitterate($1."###".$2))));
      push(@{$locrads{$currtable}{$tag}{$variant}},&normalize_classes(unquote(&dotranslitterate($4),".*")));
      push(@{$locradsexcept{$currtable}{$tag}{$variant}},&normalize_classes(unquote(&dotranslitterate($5),"##NONE##")));
      push(@{$synt{$currtable}{$tag}{$variant}},my_quotemeta($7));
      $req{$currtable}{$tag}{$variant}=($9 eq "+");
      if ($show ne "") {
	$shownum++;
	if ($tag ne $lasttag) {$shownum++}
	push(@{$show{$currtable}{$tag}{$variant}},$shownum);
      } else {
	push(@{$show{$currtable}{$tag}{$variant}},-1);
      }
      push(@{$show_pattern{$currtable}{$tag}{$variant}},$show);
      if ($variant ne "") {$tempvars{$variant}=1};
      $lasttag=$tag;
    } elsif (/^<like\s+name=\"(.*?)\"(?:\s+except=\"(.*?)\")?\/>$/) {
      # table parente
      $ftable{$currtable}=my_quotemeta($1);
      $t2=my_quotemeta($2);
      for (split(/ /,$t2)) {
	$except{$currtable}{$_}=1;
      }
      $liken++;
    } elsif (/^<derivation\s+(?:name=\"(.*?)\"\s+)?suffix=\"(.*?)\"\s+table=\"(.*?)\"(?:\s+rads=(\".*?\"))?(?:\s+except=(\".*?\"))?\/>$/) { # lemme derive
      $tag=my_quotemeta($3);
      push(@{$derivations{$currtable}{$tag}},&normalize_classes(my_quotemeta(dotranslitterate($2))));
      push(@{$locrads{$currtable}{$tag}{""}},&normalize_classes(unquote(&dotranslitterate($4)),".*"));
      push(@{$locradsexcept{$currtable}{$tag}{""}},&normalize_classes(unquote(&dotranslitterate($5)),"##NONE##"));
      push(@{$name{$currtable}{$tag}{""}},$1);
      push(@{$synt{$currtable}{$tag}{""}},"");
      $req{$currtable}{$tag}{""}=0;
      push(@{$show{$currtable}{$tag}{""}},0);
      push(@{$show_pattern{$currtable}{$tag}{""}},"");
    } elsif (/^<(?:fusion|sandhi)\s+source=\"([^_\"]*[^\\_\"])(\\?[^\"])\"\s+target=\"([^_\"]*[^\\_\"])(\\?[^_\"])\"(?:\s+final=\"([\+\-])\")?(?:\s+rev=\"([\+\-])\")?\/>$/
	     ||
	     /^<(?:fusion|sandhi)\s+source=\"((?:[^_\"]*[^\\_\"])?)_([^\"]*)\"\s+target=\"((?:[^_\"]*[^\\_\"])?)_([^\"]*)\"\/>$/||
	     /^<(?:fusion|sandhi)\s+source=\"((?:[^_\"]*[^\\_\"])?)_([^\"]*)\"\s+target=\"((?:[^_\"]*[^\\_\"])?)_([^\"]*)\"(?:\s+final=\"([\+\-])\")?(?:\s+rev=\"([\+\-])\")?\/>$/) {
      # pattern de fusion radical-terminaison
      $t1=dotranslitterate($1); $t2=&dotranslitterate($2); $t3=&dotranslitterate($3); $t4=&dotranslitterate($4); my $is_final=$5; my $is_rev=$6;
      $t1=~s/(?<=.)\^/\\\^/g; $t2=~s/\^/\\\^/g; $t3=~s/(?<=.)\^/\\\^/g; $t4=~s/\^/\\\^/g;
      if ($is_final eq "+") {$is_final=1} else {$is_final=0};
      if ($is_rev eq "-") {$is_rev=0} else {$is_rev=1};
      my $origt4=$t4;
      $t4=~s/\$//;
      $in=&normalize_classes($t1.$sep.$t2);
      $fusionarray[$fusionrank]{dir_in}=$in;
      $fusionarray[$fusionrank]{dir_out}=&normalize_classes($t3.$t4);
      $fusionarray[$fusionrank]{dir_sepout}=&normalize_classes($t3.$sep.$t4);
      if ($is_final) {$fusionarray[$fusionrank]{is_final}=1} else {$fusionarray[$fusionrank]{is_final}=0}
      $t2=~s/\$//;
      $in=&normalize_classes($t3.$sep.$origt4);
      $fusionarray[$fusionrank]{rev_in}=$in;
      $fusionarray[$fusionrank]{rev_out}=&normalize_classes($t1.$sep.$t2);
      if ($is_rev) {$fusionarray[$fusionrank]{is_rev}=1} else {$fusionarray[$fusionrank]{is_rev}=0}
      $fusionrank++;
    } elsif (/^<letterclass\s+name=\"(.+)\"\s+letters=\"(.+)\"\/>$/) { # les lettres de la langue
      $cl=&dotranslitterate($1);
      for (split(/ +/,&dotranslitterate($2))) {
	if (/./) {
	  if (!defined($letters{$_})) {
	    $letters{$_}=1;
	    push(@{$class{"##ANY##"}},$_);
	  }
	  push(@{$class{$cl}},$_);
	}
      }
    } elsif (/^\s*<pairs\s+l1=\"(.+)\"\s+l2=\"(.+)\"\s*>$/) { # operateurs sur les lettres
      $curr_l1=&dotranslitterate($1); $curr_l2=&dotranslitterate($2);
      $in_pairs=1;
    } elsif (/^\s*<pair\s+l1=\"(.+)\"\s+l2=\"(.+)\"\s*\/>$/ && $in_pairs) { # operateurs sur les lettres
      my $l1=$1; my $l2=$2;
      my $funcname=$curr_l1."2".$curr_l2;
      $funcname=~s/ /_/g;
      $pairs{$funcname}{dotranslitterate($l1)}=dotranslitterate($l2);
      my $funcname=$curr_l2."2".$curr_l1;
      $funcname=~s/ /_/g;
      $pairs{$funcname}{dotranslitterate($l2)}=dotranslitterate($l1);
    } elsif (/^\s*<\/pairs>$/) { # operateurs sur les lettres
      die "failed\n###### Error line $l: pairs $curr_l1 <-> $curr_l2 not closed by </pairs>" if (!$in_pairs);
    } elsif (/^\s*<translitteration\s+source=\"(.+)\"\s+target=\"(.+)\"\/>$/) {
      # déjà fait en première passe
    } else {
      die "failed\n###### Error line $l: unparsable line \"$_\"";
    }
}
close(TABLES_F);


# héritage selon les <like ... />
my $newtag;
while (1) {
    my $prevliken=$liken;
    my $suspect="";
    for $currtable (keys %canonical_tag) {
	if (defined($ftable{$currtable})) {
	    if (!defined($ftable{$ftable{$currtable}})) {
		if (!defined($output{$ftable{$currtable}})) {die("\n<like .../> sur une classe inconnue ($ftable{$currtable})\nClasses connues: ".join(", ",keys %output))}
	        # héritage des formes "like" dans $ftable{$currtable}
		for my $tag (keys %{$formlike{$ftable{$currtable}}}) {
		  for my $like (keys %{$formlike{$ftable{$currtable}}{$tag}}) {
		    $show=$formlike{$ftable{$currtable}}{$tag}{$like};
		    for $variant (keys %{$output{$ftable{$currtable}}{$like}}) {
		      @{$output{$ftable{$currtable}}{$tag}{$variant}}=@{$output{$ftable{$currtable}}{$like}{$variant}};
		      @{$locrads{$ftable{$currtable}}{$tag}{$variant}}=@{$locrads{$ftable{$currtable}}{$like}{$variant}};
		      @{$locradsexcept{$ftable{$currtable}}{$tag}{$variant}}=@{$locradsexcept{$ftable{$currtable}}{$like}{$variant}};
		      @{$synt{$ftable{$currtable}}{$tag}{$variant}}=@{$synt{$ftable{$currtable}}{$like}{$variant}};
		      $req{$ftable{$currtable}}{$tag}{$variant}=$req{$ftable{$currtable}}{$like}{$variant};
		      for (0..$#{$show{$ftable{$currtable}}{$tag}{$variant}}) {
			if ($show ne "") {
			  $show{$ftable{$currtable}}{$tag}{$variant}[$_]=$show{$ftable{$currtable}}{$like}{$variant}[$_];
			} else {
			  $show{$ftable{$currtable}}{$tag}{$variant}[$_]=-1;
			}
			$show_pattern{$ftable{$currtable}}{$tag}{$variant}[$_]=$show;
		      }
		    }
		  }
		}
		delete($formlike{$ftable{$currtable}});
		# héritage du canonical_tag
		$first_tag{$currtable} = $first_tag{$ftable{$currtable}};
		$first_tag{$currtable}=~s/$tag_suffix{$ftable{$currtable}}$/$tag_suffix{$currtable}/;
		# héritage du canonical_tag
		if ($canonical_tag{$currtable} eq "##NONE##") {
		  $canonical_tag{$currtable} = $canonical_tag{$ftable{$currtable}};
		  $canonical_tag{$currtable}=~s/$tag_suffix{$ftable{$currtable}}$/$tag_suffix{$currtable}/;
		}
		# héritage des exclusions mutuelles entre variantes
		for $t1 (keys %{$exclvars{$ftable{$currtable}}}) {
		    for $t2 (keys %{$exclvars{$ftable{$currtable}}{$t1}}) {
			$exclvars{$currtable}{$t1}{$t2}=1;
		    }
		}
		# heritage des formes
		for $tag (keys %{$output{$ftable{$currtable}}}) {
		    $newtag=$tag;
		    $newtag=~s/$tag_suffix{$ftable{$currtable}}$/$tag_suffix{$currtable}/;
		    if (!defined($output{$currtable}{$newtag}) && !defined($formlike{$currtable}{$newtag})
			&& !(defined($except{$currtable}{$tag}) 
			     || ($tag=~/^(.)/ && defined($except{$currtable}{$1}))
			     || ($tag=~/^([^:]+)/ && defined($except{$currtable}{$1})))) {
			for $variant (keys %{$output{$ftable{$currtable}}{$tag}}) {
			  $req{$currtable}{$newtag}{$variant}=$req{$ftable{$currtable}}{$tag}{$variant};
			    for my $varn (0..$#{$output{$ftable{$currtable}}{$tag}{$variant}}) {
				$output{$currtable}{$newtag}{$variant}[$varn]=$output{$ftable{$currtable}}{$tag}{$variant}[$varn];
				$locrads{$currtable}{$newtag}{$variant}[$varn]=$locrads{$ftable{$currtable}}{$tag}{$variant}[$varn];
				$locradsexcept{$currtable}{$newtag}{$variant}[$varn]=$locradsexcept{$ftable{$currtable}}{$tag}{$variant}[$varn];
				$show{$currtable}{$newtag}{$variant}[$varn]=$show{$ftable{$currtable}}{$tag}{$variant}[$varn];
				$show_pattern{$currtable}{$newtag}{$variant}[$varn]=$show_pattern{$ftable{$currtable}}{$tag}{$variant}[$varn];
				$synt{$currtable}{$newtag}{$variant}[$varn]=$synt{$ftable{$currtable}}{$tag}{$variant}[$varn];
			    }
			}
		    }
		}
		# heritage des derivations
		for $tag (keys %{$derivations{$ftable{$currtable}}}) {
		    if (!defined($derivations{$currtable}{$tag})) {
		      $req{$currtable}{$tag}{""}=$req{$ftable{$currtable}}{$tag}{""};
			for my $varn (0..$#{$derivations{$ftable{$currtable}}{$tag}}) {
			    $derivations{$currtable}{$tag}[$varn]=$derivations{$ftable{$currtable}}{$tag}[$varn];
			    $locrads{$currtable}{$tag}{""}[$varn]=$locrads{$ftable{$currtable}}{$tag}{""}[$varn];
			    $locradsexcept{$currtable}{$tag}{""}[$varn]=$locradsexcept{$ftable{$currtable}}{$tag}{""}[$varn];
			    $show{$currtable}{$tag}{""}[$varn]=$show{$ftable{$currtable}}{$tag}{""}[$varn];
			    $show_pattern{$currtable}{$tag}{""}[$varn]=$show_pattern{$ftable{$currtable}}{$tag}{""}[$varn];
			    $synt{$currtable}{$tag}{""}[$varn]=$synt{$ftable{$currtable}}{$tag}{""}[$varn];
			    $name{$currtable}{$tag}{""}[$varn]=$name{$ftable{$currtable}}{$tag}{""}[$varn];
			}
		    }
		}
		delete($ftable{$currtable});
		$liken--;
	    } else {
		$suspect=$ftable{$currtable};
	    }
	}
    }
    if ($liken>0 && $prevliken==$liken) {die("\nCircularite dans les <like .../> (suspect: $suspect - $prevliken - $liken)\n")}
    if ($liken==0){last}
}

# héritage des formes "like"
for my $currtable (keys %formlike) {
  for my $tag (keys %{$formlike{$currtable}}) {
    for my $like (keys %{$formlike{$currtable}{$tag}}) {
      $show=$formlike{$currtable}{$tag}{$like};
      for $variant (keys %{$output{$currtable}{$like}}) {
	@{$output{$currtable}{$tag}{$variant}}=@{$output{$currtable}{$like}{$variant}};
	@{$locrads{$currtable}{$tag}{$variant}}=@{$locrads{$currtable}{$like}{$variant}};
	@{$locradsexcept{$currtable}{$tag}{$variant}}=@{$locradsexcept{$currtable}{$like}{$variant}};
	@{$synt{$currtable}{$tag}{$variant}}=@{$synt{$currtable}{$like}{$variant}};
	$req{$currtable}{$tag}{$variant}=$req{$currtable}{$like}{$variant};
	for (0..$#{$show{$currtable}{$tag}{$variant}}) {
	  if ($show ne "") {
	    $show{$currtable}{$tag}{$variant}[$_]=$show{$currtable}{$like}{$variant}[$_];
	  } else {
	    $show{$currtable}{$tag}{$variant}[$_]=-1;
	  }
	  $show_pattern{$currtable}{$tag}{$variant}[$_]=$show;
	}
      }
    }
  }
}

for $currtable (keys %canonical_tag) {
  if ($canonical_tag{$currtable} eq "##NONE##") {$canonical_tag{$currtable}=$first_tag{$currtable};}
}

for $currtable (keys %show) {
  next if (!defined($show{$currtable}) || !defined($show{$currtable}{$canonical_tag{$currtable}}));
  for $variant (keys %{$show{$currtable}{$canonical_tag{$currtable}}}) {
    next if (!defined($show{$currtable}{$canonical_tag{$currtable}}{$variant}));
    for (0..$#{$show{$currtable}{$canonical_tag{$currtable}}{$variant}}) {
      $show{$currtable}{$canonical_tag{$currtable}}{$variant}[$_]=1;
    }
  }
}

my %re=();
for my $c (keys %class) {
    if ($c eq "##ANY##") {$re{$c}="[^\\t]"}
    else {$re{$c}="[".join("",@{$class{$c}})."]";}
}

my ($n, $t,$oldt,$fst,$rft,$oldfst,$op, $cl2);
for $fusionrank (0..$#fusionarray) {
    $n=0;
    $t=$fusionarray[$fusionrank]{dir_in};
    $fst=$fusionarray[$fusionrank]{dir_sepout};
    $oldt=$t;
    $oldfst=$fst;
    while ($t=~/\[:([^:]+):\]/) {
	$n++;
	$cl=$1;
	die "Error: undefined class $cl" if (!defined($re{$cl}));
	$t=~s/\[:$cl:\]([\*\+])?/($re{$cl}$1)/;
	$op=$1;
	if ($fst=~/\[:([^:]+):\]([\*\+])?/) {
	  $cl2=$1;
	  die "Inconsistent fusion pattern $oldt <-d-> $oldfst : [:$cl:]$op corresponds to [:$cl2:]$2" if ($2 ne $op);
	  $op=quotemeta($op);
	  if ($cl eq $cl2) {$fst=~s/\[:([^:]+):\]$op/\$$n/}
	  else {
	    my $funcname=$cl."2".$cl2;
	    $funcname=~s/ /_/g;
	    $fst=~s/\[:([^:]+):\]$op/\$${funcname}{\$$n}/;
	  }
	} else {
	  die "Inconsistent fusion pattern $oldt <-d-> $oldfst : $oldt has more variables than $oldfst (first pb: class $cl)";
	}
    }
    $fusionarray[$fusionrank]{dir_in}=$t;
    $fusionarray[$fusionrank]{dir_sepout}=$fst;
    $fst=~s/$sep//g;
    $fusionarray[$fusionrank]{dir_out}=$fst;

    $t=$fusionarray[$fusionrank]{rev_in};
    $rft=$fusionarray[$fusionrank]{rev_out};
    $oldt=$t;
    $n=0;
    while ($t=~/\[:([^:]+):\]/) {
      $n++;
      $cl=$1;
      die "Error: undefined class $cl" if (!defined($re{$cl}));
      $t=~s/\[:$cl:\]([\*\+])?/($re{$cl}$1)/;
      $op=$1;
      if ($rft=~/\[:([^:]+):\]([\*\+])?/) {
	$cl2=$1;
	$op=quotemeta($op);
	if ($cl eq $cl2) {$rft=~s/\[:([^:]+):\]$op/\$$n/}
	else {
	  my $funcname=$cl."2".$cl2;
	  $funcname=~s/ /_/g;
	  $rft=~s/\[:([^:]+):\]$op/\$${funcname}{\$$n}/;
	}
      } else {
	die "Bug (line 401)";
      }
    }
    $fusionarray[$fusionrank]{rev_in}=$t;
    $fusionarray[$fusionrank]{rev_out}=$rft;
}
print STDERR "done\n";

print STDERR"  Building inflection patterns...";

$l=0;
my $form="";
my $lemme="";
my $table="";
my $out="";
my $oldout="";
my $oform="";
my %origlemme=();

my $prefixes="(?:(?:ne)?(?:u|na|za?|s|pre|pred|zod|pri|vy?|od?|po|do|zo)?)";
my $all=".*";

my %rev_i;
my %rev_o;
my %chk_i;
my %dir_i;
my %dir_o;
my %der_i;
my %der_o;

my $v;

my $srads;
for my $table (keys %derivations) {
    for my $dtable (keys %{$derivations{$table}}) {
	for my $var (0..$#{$derivations{$table}{$dtable}}) {
	    $radsexcept="##NONE##";
	    if ($locradsexcept{$table}{$dtable}{""}[$var] ne "##NONE##") {
		$radsexcept=$locradsexcept{$table}{$dtable}{""}[$var];
	    } elsif ($radsexcept{$table} ne "##NONE##") {
		$radsexcept=$radsexcept{$table};
	    }
	    if ($locrads{$table}{$dtable}{""}[$var] ne "") {
		$rads=$locrads{$table}{$dtable}{""}[$var];
		$srads=$rads;
	    }
	    elsif (defined($rads{$table})) {
		$rads=$rads{$table};
		$srads=$rads;
#		while ($srads=~s/\\[^\*]([\+\*]?)/.$1/) {} # transforme \x en .
		while ($srads=~s/\[:[^:]+:\]([\+\*]?)/.$1/) {} # transforme [:x:] en .
		$srads=~s/^</.*/; # transforme le < initial en .*
		while ($srads=~s/(^|[^\(\|])\[[^\]]+\]([^\)\|]|$)/$1.$2/) {} # remplace [xyz] par .
#		print STDERR ">>> $srads\n";
	    }
	    else {$rads=$all; $srads=$rads}
	    while ($srads=~s/(\([^\[\]\|\(\)]*)\)\(([^\[\]\|\(\)]*\))/$1$2/g) {} # remplace )( par rien là où il faut
	    $srads=~s/^([^\(\)\^]+)(\(|$)/\($1\)$2/; # rajoute des parenthèses là où il faut
	    $srads=~s/\)([^\(\)]+)$/\)\($1\)/; # rajoute des parenthèses là où il faut
	    $srads=~s/\(\)//g; # supprime tous les ()
	    while ($srads=~s/(\([^\[\]\|\(\)]*)\)\(([^\[\]\|\(\)]*\))/$1$2/g) {} # remplace )( par rien là où il faut
	    while ($srads=~s/\.\*\.\*/\.\*/) {}
	    while ($srads=~s/\.\+\.\*/.+/ 
		   || $srads=~s/\.\*\.\+/.+/ 
		   || $srads=~s/\.\+\.\+/.+/ 
		   || $srads=~s/\.+\.[\+\*]/.+/ 
		   || $srads=~s/\.[\+\*]\.+/.+/) {}
#	    $srads=~s/^\(\.[\+\*]\)//;
	    if (0) {
	    for my $dtag (keys %{$output{$dtable}}) {
	      if ($dtag!~/D\[_/) { # A CONFIRMER
		for $variant (keys %{$output{$dtable}{$dtag}}) {
		  for my $dvar (0..$#{$output{$dtable}{$dtag}{$variant}}) {
		    $output{$dtable}{$dtag}{$variant}[$dvar]=~/^(.*)###(.*)$/;
		    push(@{$output{$table}{"D[#".$derivations{$table}{$dtable}[$var]."###".$dtable."]:".$dtag}{$variant}},$1.$derivations{$table}{$dtable}[$var].$2);
		    push(@{$locrads{$table}{"D[#".$derivations{$table}{$dtable}[$var]."###".$dtable."]:".$dtag}{$variant}},$srads);
		    push(@{$locradsexcept{$table}{"D[#".$derivations{$table}{$dtable}[$var]."###".$dtable."]:".$dtag}{$variant}},$radsexcept);
		  }
		}
	      }
	    }
	  }
	}
    }
}

my $n=0;
my ($pref,$suff);
for my $table (keys %output) {
  for my $tag (keys %{$output{$table}}) {
    for my $variant (keys %{$output{$table}{$tag}}) {
      for my $var (0..$#{$output{$table}{$tag}{$variant}}) {
	if ($locrads{$table}{$tag}{$variant}[$var] ne $all) {
	  $rads=$locrads{$table}{$tag}{$variant}[$var];
	  $srads=$rads;
	}
	elsif (defined($rads{$table})) {
	  $rads=$rads{$table};
	  $srads=$all;
	  #		    $srads=$rads;
	  #		    while ($srads=~s/\[:[^:]+:\]([\+\*]?)/.$1/) {} # transforme [:x:] en .
	  #		    $srads=~s/^</.*/; # transforme le < initial en .*
	  #		    while ($srads=~s/(^|[^\(\|])\[[^\]]+\]([^\)\|]|$)/$1.$2/) {} # remplace [xyz] par .
	}
	else {$rads=$all; $srads=$rads}
	while ($srads=~s/(\([^\[\]\|\(\)]*)\)\(([^\[\]\|\(\)]*\))/$1$2/g) {} # remplace )( par rien là où il faut
	$srads=~s/^([^\(\)\^]+)(\(|$)/\($1\)$2/; # rajoute des parenthèses là où il faut
	$srads=~s/\)([^\(\)]+)$/\)\($1\)/; # rajoute des parenthèses là où il faut
	$srads=~s/\(\)//g; # supprime tous les ()
	while ($srads=~s/(\([^\[\]\|\(\)]*)\)\(([^\[\]\|\(\)]*\))/$1$2/g) {} # remplace )( par rien là où il faut
	while ($srads=~s/\.\*\.\*/\.\*/) {}
	while ($srads=~s/\.\+\.\*/.+/ 
	       || $srads=~s/\.\*\.\+/.+/ 
	       || $srads=~s/\.\+\.\+/.+/ 
	       || $srads=~s/\.+\.[\+\*]/.+/ 
	       || $srads=~s/\.[\+\*]\.+/.+/) {}
	$srads=~s/\(\.(?:\|\.)+\)/\(.\)/g;
	$v="";#
	$output{$table}{$tag}{$variant}[$var]=~/^(.*?)###(.*)$/;
	$pref=$1; $suff=$2;
	$rev_i{$table}{$tag}{$variant}[$var]="$rads$sep$suff";
	$rev_i{$table}{$tag}{$variant}[$var]=~s/^<([\.\*\+]*)/$pref\(\$prefixes?$1\)/g || 
	  $rev_i{$table}{$tag}{$variant}[$var]=~s/^([\.\*\+]*)/$pref\($1\)/g;
	while($rev_i{$table}{$tag}{$variant}[$var]=~s/(^|[^\(\|])\[([^\]:]*)\]([\+\*]?)([^\)\|]|$)/$1(\{$2\}$3)$4/g) {}
	while($rev_i{$table}{$tag}{$variant}[$var]=~s/\[([^\]:]*)\]/\{$1\}/g) {}
	$rev_i{$table}{$tag}{$variant}[$var]=~s/\{/\[/g;
	$rev_i{$table}{$tag}{$variant}[$var]=~s/\}/\]/g;
	while($rev_i{$table}{$tag}{$variant}[$var]=~s/(^|[^\(\|])\[:([^:]+):\]([\+\*]?)([^\)\|]|$)/$1\($re{$2}$3\)$4/) {}
	while($rev_i{$table}{$tag}{$variant}[$var]=~s/\[:([^:]+):\]([\+\*]?)/$re{$1}$2/) {}
	
	$rev_o{$table}{$tag}{$variant}[$var]="$rads";
	$rev_o{$table}{$tag}{$variant}[$var]=~s/^\^//g;
	$rev_o{$table}{$tag}{$variant}[$var]=~s/^<?[\.\*]*/!/g;
	$rev_o{$table}{$tag}{$variant}[$var]=~s/\\.[\+\*]?/!/g;
	$rev_o{$table}{$tag}{$variant}[$var]=~s/\[[^\]]*\][\+\*]?/!/g;
	$rev_o{$table}{$tag}{$variant}[$var]=~s/\([^\)]*\)/!/g;
	$n=0;
	while(++$n && $rev_o{$table}{$tag}{$variant}[$var]=~s/!/\$$n/) {}
	
	$chk_i{$table}{$tag}{$variant}[$var]="$rads";
	$chk_i{$table}{$tag}{$variant}[$var]=~s/^<([\.\*\+]*)/\(\$prefixes?$1\)/g ||
	  $chk_i{$table}{$tag}{$variant}[$var]=~s/^([\.\*\+]*)/\($1\)/g;
	while($chk_i{$table}{$tag}{$variant}[$var]=~s/\[([^\]:]*)\]/(\{$1\})/g) {}
	$chk_i{$table}{$tag}{$variant}[$var]=~s/\{/\[/g;
	$chk_i{$table}{$tag}{$variant}[$var]=~s/\}/\]/g;
	while($chk_i{$table}{$tag}{$variant}[$var]=~s/\[:([^:]+):\]([\+\*]?)/$re{$1}$2/) {}
	
	$dir_i{$table}{$tag}{$variant}[$var]="$srads";
	$dir_i{$table}{$tag}{$variant}[$var]=~s/^\(</\(\$prefixes?/g;
	while($dir_i{$table}{$tag}{$variant}[$var]=~s/(^|[^\(\|])\[([^\]:]*)\]([\+\*]?)([^\)\|]|$)/$1(\{$2\}$3)$4/g) {}
	while($dir_i{$table}{$tag}{$variant}[$var]=~s/\[([^\]:]*)\]([^\)]|$)/\{$1\}$2/g) {}
	$dir_i{$table}{$tag}{$variant}[$var]=~s/\{/\[/g;
	$dir_i{$table}{$tag}{$variant}[$var]=~s/\}/\]/g;
	while($dir_i{$table}{$tag}{$variant}[$var]=~s/(^|[^\(\|])\[:([^:]+):\]([\+\*]?)([^\)\|]|$)/$1\($re{$2}$3\)$4/) {}
	while($dir_i{$table}{$tag}{$variant}[$var]=~s/\[:([^:]+):\]([\+\*]?)/$re{$1}$2/) {}
	
	$dir_o{$table}{$tag}{$variant}[$var]="$pref$sep$srads$sep$suff";
	#		if ($pref eq "") {
	#		  $dir_i{$table}{$tag}{$variant}[$var]=~s/^\(\.[\+\*]\)//;
	#		  $dir_o{$table}{$tag}{$variant}[$var]=~s/^\(\.[\+\*]\)//;
	#		}
	$dir_o{$table}{$tag}{$variant}[$var]=~s/^\^//g;
	$dir_o{$table}{$tag}{$variant}[$var]=~s/\\.[\+\*]?/!/g;
	$dir_o{$table}{$tag}{$variant}[$var]=~s/\[[^\]]*\][\+\*]?/!/g;
	$dir_o{$table}{$tag}{$variant}[$var]=~s/\([^\)]*\)/!/g;
	$n=0;
	while(++$n && $dir_o{$table}{$tag}{$variant}[$var]=~s/!/\$$n/) {}
      }
    }
  }
  for my $tag (keys %{$derivations{$table}}) {
    my $variant="";
    for my $var (0..$#{$derivations{$table}{$tag}}) {
      if ($locrads{$table}{$tag}{$variant}[$var] ne $all) {
	$rads=$locrads{$table}{$tag}{$variant}[$var];
	$srads=$rads;
      }
      elsif (defined($rads{$table})) {
	$rads=$rads{$table};
	$srads=$all;
	#		$srads=$rads;
	#		while ($srads=~s/\[:[^:]+:]([\+\*]?)/.$1/) {} # transforme \x en .
	#		$srads=~s/^</.*/; # transforme le < initial en .*
	#		while ($srads=~s/(^|[^\(\|])\[[^\]]+\]([^\)\|]|$)/$1.$2/) {} # remplace [xyz] par .
      }
      else {$rads=$all; $srads=$rads}
      while ($srads=~s/(\([^\[\]\|\(\)]*)\)\(([^\[\]\|\(\)]*\))/$1$2/g) {} # remplace )( par rien là où il faut
      $srads=~s/^([^\(\)\^]+)(\(|$)/\($1\)$2/; # rajoute des parenthèses là où il faut
      $srads=~s/\)([^\(\)]+)$/\)\($1\)/; # rajoute des parenthèses là où il faut
      $srads=~s/\(\)//g; # supprime tous les ()
      while ($srads=~s/(\([^\[\]\|\(\)]*)\)\(([^\[\]\|\(\)]*\))/$1$2/g) {} # remplace )( par rien là où il faut
      while ($srads=~s/\.\*\.\*/\.\*/) {}
      while ($srads=~s/\.\+\.\*/.+/ 
	     || $srads=~s/\.\*\.\+/.+/ 
	     || $srads=~s/\.\+\.\+/.+/ 
	     || $srads=~s/\.+\.[\+\*]/.+/ 
	     || $srads=~s/\.[\+\*]\.+/.+/) {}
      $srads=~s/^\(\.[\+\*]\)//;
      $v="";#
      
      $der_i{$table}{$tag}{$variant}[$var]="$srads";
      $der_i{$table}{$tag}{$variant}[$var]=~s/^\(</\(\$prefixes?/g;
      while ($der_i{$table}{$tag}{$variant}[$var]=~s/(^|[^\(\|])\[([^\]:]*)\]([\+\*]?)([^\)\|]|$)/$1(\{$2\}$3)$4/g) {}
      while($der_i{$table}{$tag}{$variant}[$var]=~s/\[([^\]:]*)\]/(\{$1\})/g) {}
      $der_i{$table}{$tag}{$variant}[$var]=~s/\{/\[/g;
      $der_i{$table}{$tag}{$variant}[$var]=~s/\}/\]/g;
      while($der_i{$table}{$tag}{$variant}[$var]=~s/(^|[^\(\|])\[:([^:]+):\]([\+\*]?)([^\)\|]|$)/$1\($re{$2}$3\)$4/) {}
      while($der_i{$table}{$tag}{$variant}[$var]=~s/\[:([^:]+):\]([\+\*]?)/\($re{$1}$2\)/) {}
      
      $der_o{$table}{$tag}{$variant}[$var]="$srads$sep$derivations{$table}{$tag}[$var]";
      $der_o{$table}{$tag}{$variant}[$var]=~s/^\^//g;
      $der_o{$table}{$tag}{$variant}[$var]=~s/\\.[\+\*]?/!/g;
      $der_o{$table}{$tag}{$variant}[$var]=~s/\[[^\]]*\]/!/g;
      $der_o{$table}{$tag}{$variant}[$var]=~s/\([^\)]*\)/!/g;
      $n=0;
      while(++$n && $der_o{$table}{$tag}{$variant}[$var]=~s/!/\$$n/) {}
    }
  }
}

for $table (keys %rads) {
    while($rads{$table}=~s/\[:([^:]+):\]([\+\*]?)/\($re{$1}$2\)/) {}
    $rads{$table}=~s/^</\$prefixes?/g;
}
for $table (keys %locrads) {
    for my $tag (keys %{$locrads{$table}}) {
	for my $variant (keys %{$locrads{$table}{$tag}}) {
	    for my $var (0..$#{$locrads{$table}{$tag}{$variant}}) {
		while($locrads{$table}{$tag}{$variant}[$var]=~s/\[:([^:]+):\]([\+\*]?)/\($re{$1}$2\)/) {}
		$locrads{$table}{$tag}{$variant}[$var]=~s/^</\$prefixes?/g;
	    }
	}
    }
}
for $table (keys %radsexcept) {
  while($radsexcept{$table}=~s/\[:([^:]+):\]([\+\*]?)/\($re{$1}$2\)/) {}
    $radsexcept{$table}=~s/^</\$prefixes?/g;
}
for $table (keys %locradsexcept) {
    for my $tag (keys %{$locradsexcept{$table}}) {
	for my $variant (keys %{$locradsexcept{$table}{$tag}}) {
	    for my $var (0..$#{$locradsexcept{$table}{$tag}{$variant}}) {
		while($locradsexcept{$table}{$tag}{$variant}[$var]=~s/\[:([^:]+):\]([\+\*]?)/\($re{$1}$2\)/) {}
		$locradsexcept{$table}{$tag}{$variant}[$var]=~s/^</\$prefixes?/g;
	    }
	}
    }
}
print STDERR "done\n";

my %tables_by_cat;
my %cat_of_table;
for my $table (keys %dir_i) {
    $table=~/^([^-]+).*$/ || die("Error in table name $table\n");
    $tables_by_cat{$1}{$table}=1;
    $cat_of_table{$table}=$1;
}

my %rev_i_by_terms;
for my $table (keys %rev_i) {
    for my $tag (keys %{$rev_i{$table}}) {
	if ($tag!~/^D\[/) {
	    for my $variant (keys %{$rev_i{$table}{$tag}}) {
		for my $var (0..$#{$rev_i{$table}{$tag}{$variant}}) {
		    $rev_i{$table}{$tag}{$variant}[$var]=~/.*$sep(.*)$/ || die("Problem: $rev_i{$table}{$tag}{$variant}[$var] should have symbol \"$sep\"\n");
		    $rev_i_by_terms{$1}{$table}{$tag}{$variant}[$var]=$rev_i{$table}{$tag}{$variant}[$var];
		}
	    }
	}
    }
}

$tables_file =~ s/^.*\///;

print STDERR "  Writing data and script files...";
open(DATA,">$output_dir$tables_file.data") || die "########## Fichier de tables $tables_file.data inouvrable.\n";
binmode DATA, ":utf8";
for my $table (keys %output) {
    for my $tag (keys %{$output{$table}}) {
	for my $variant (keys %{$output{$table}{$tag}}) {
	    print DATA "$table\t$tag\t$variant\n";
	}
    }
}
for my $t (keys %exclvars) {
  for $t1 (keys %{$exclvars{$t}}) {
    for $t2 (keys %{$exclvars{$t}{$t1}}) {
      print DATA "##EXCL##\t$t\t$t1\t$t2\n";
    }
  }
}
for $t (keys %req) {
  for $tag (keys %{$req{$t}}) {
    for $v (keys %{$req{$t}{$tag}}) {
      if ($req{$t}{$tag}{$v}) {
	print DATA "##REQ##\t$t\t$tag\t$v\n";
      }
    }
  }
}
close(DATA);

open(DIR,">$output_dir$tables_file.dir.pl") || die "########## Fichier de tables $tables_file.dir.pl inouvrable.\n";
binmode DIR, ":utf8";
print DIR "#!/usr/bin/env perl\n\n";
print DIR <<UTF8;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
UTF8

print DIR <<MAIN;
use strict;
my \$pwd = \_\_FILE\_\_;
\$pwd =~ s/^(.*[^\\/]\\/).*/\\1/;
require("\$pwd/$tables_file.check.pl");
my \$log=0;
my \$showall=1;
#my \$showder=0;
my \$withvars=0;
my \$nosep=0;
my \$synt=0;
my \$rads=0;
my \$onetag=0;
my \$onerad=0;
my \$l=0;
my \$multiword=0; my \$mainword=0; my \$last_t;
my \%lastlemmaid;
my \@t; my \@w; my \@cat; my \@v; my \@msprop; my \@result; my \@tag; my \@table; my \@output;my \@num2mainnum;
my (\$v, \$nf, \%forms);
while (my \$option=shift) {
  if (\$option eq \"-no_sa\") {\$showall=0}
#  elsif (\$option eq \"-der\") {\$showder=1}
  elsif (\$option eq \"-vars\") {\$withvars=1}
  elsif (\$option eq \"-nosep\") {\$nosep=1}
  elsif (\$option eq \"-synt\") {\$synt=1}
  elsif (\$option eq \"-rads\") {\$rads=1}
  elsif (\$option eq \"-log\") {\$log=1}
  elsif (\$option eq \"-tag\") {\$onetag=1}
  elsif (\$option eq \"-onerad\") {\$onerad=1}
}
my (\$w, \$r, \$v, \$cat, \$t, \$remainer, \$var, \$tag, \$class);
while (<>) {
  chomp;
  s/^#([^\"].*|)\$//; next if (/^[ \\t]*\$/);
  /([^\\t]*)\\t([^\\t]*)((?:\\t.*)?)/ || die (\"Bad input format for morpho.*.dir.pl : \\\"\$_\\\"\\n\");

  \$w=\$1;
  \$class=\$2;
  \$remainer=\$3;
  if (\$onetag) {
    \$remainer=~s/^\\t([^\\t\\n]*)//;
    \$tag=\$1;
  } else {
    \$tag="";
  }
  \$mainword=0;

  \$var=\"___\";
  if (\$w=~s/^([^\\t]*)(___.*)\$/\$1/) {\$var=&detranslitterate(\$2);}
  \$lastlemmaid{\$w.\$var}++;
  \@result=(); \@table=();
  \@tag=(); \@msprop=(); \@w=();
  if (\$class=~/[ \\+]/) {
    \@w=split(/ +/,\$w);
    \@v=(); \@cat=(); \@t=(); \@table=(); \@num2mainnum=();
    if (\$synt) {
      \@msprop=();
    }
    \$last_t=\"\";
    for (split(/\\+/,\$class)) {
      if (\$last_t ne \"NEXT_t\") {
        if (\$_ ne \"\") {
          /^(([^-:\\t]+)[^:]*)(:.*)?/;
          push(\@t,\$1);
          push(\@cat,\$2);
          push(\@v,\$3);
        } else {
          \$last_t=\"NEXT_t\";
        }
      } else {
        for my \$n (\$#t+1..\$#w-1) {
          push(\@t,\"inv\");
          push(\@v,\"\");
          push(\@cat,\"inv\");
        }
        /^(([^-:\\t]+)[^:]*)(:.*)?/;
        \$t[\$#w] = \$1;
        \$cat[\$#w] = \$2;
        \$v[\$#w] = \$3;
      }
    }
    if (\$#w>\$#t) {
      for my \$n (\$#t+1..\$#w) {
        push(\@t,\"inv\");
        push(\@v,\"\");
        push(\@cat,\"inv\");
      }
    }
#    if (\$#w != \$#t) {
#      print \"\$w\\t<error(mw \$#w != \$#t)>\\n\";
#      next;
#    }
    for my \$n (0..\$#w) {
      if (\$w[\$n]=~s/^\{(.*)\}/\$1/) {\$mainword=\$n}
    }
    for my \$n (0..\$#w) {
      if (\$t[\$n] ne \"0\") {
        inflect(\$w[\$n],\$n,\$cat[\$n],\$t[\$n],\$v[\$n]);
      } elsif (\$mainword==\$n) {
        \$remainer=~s/^(\@[^ ]+)/\$1\(\$w[\$n]\)/;
        \$remainer=~s/\\\$0/\$w[\$n]/g;
        my \$n_plus_1 = \$n + 1;
        \$remainer=~s/\\\$\$n_plus_1/\$w[\$n]/g;
        if (\$synt) {
          \$msprop[\$mainword][0]=\"\\tDefault\";
        }
      }
      if (\$n < 10) {
        my \$n_plus_1 = \$n + 1;
        \$remainer=~s/\\\$\$n_plus_1/\$w[\$n]/g;
      }
    }
  } else {
    \$w[0]=\$w;
    \$class =~ /^(([^-:\\t]+)[^:]*)(:.*)?/;
    inflect(\$w,0,\$2,\$1,\$3);
  }
  if (\$withvars) {
    \$w.=\"\$var\\__\".(\$lastlemmaid{\$w.\$var});
  }
  \$w=&detranslitterate(\$w);
  \$w=~s/$sep/ /g;
  \$w=~s/\{([^ ]+)\}/\$1/;
  if (\$#w==0) {
    for my \$i (0..\$#{\$result[0]}) {
      print \"\$w\$msprop[0][\$i]\\t\$result[0][\$i]\\t\$tag[0][\$i]\\t\$table[0][\$i]\$remainer\\n\";
    }
  } else {
    \@output=();
    \$output[0]=\"\";
    my \@temp=();
    my \@temp2=();
    for my \$i (0..\$#w) {
      if (\$t[\$i] ne \"0\") {
        \@temp=();
        for my \$j (0..\$#output) {
          \@temp2=();
          for my \$k (0..\$#{\$result[\$i]}) {
#            print STDERR "\\\$result[\$i][\$k]=\$result[\$i][\$k]\\n";
            push(\@temp,\$output[\$#output-\$j].\" \".\$result[\$i][\$k]);
          }
#          splice(\@output,\$#output-\$j,1,\@temp);
#          \@temp=();
          for my \$k (0..\$#{\$result[\$i]}) {
            if (\$i == \$mainword) {
              push(\@temp2,\$k);
            } else {
              push(\@temp2,\$num2mainnum[\$#output-\$j]);
            }
          }
          splice(\@num2mainnum,\$#output-\$j,1,\@temp2);
        }
        \@output=();
        for (\@temp) {push(\@output,\$_);}
      }
    }
    for my \$i (0..\$#output) {
      \$output[\$i]=~s/^ //;
      print \"\$w\$msprop[\$mainword][\$num2mainnum[\$i]]\\t\$output[\$i]\\t\$tag[\$mainword][\$num2mainnum[\$i]]\\t\$table[\$mainword][\$num2mainnum[\$i]]\$remainer\\n\";
    }
  }
}
print STDERR \"  Inflecting lemmas...done      \\n\";
MAIN

print DIR "sub inflect {\n";
print DIR "  my \$w=shift;\n";
print DIR "  my \$origw=\$w;\n";
print DIR "  \$w =~ s/$sep/ /g;\n";
print DIR "  my \$ccnum=shift;\n";
print DIR "  my \$cat=shift;\n";
print DIR "  my \$t=shift;\n";
print DIR "  my \$v=shift;\n";
print DIR "  \$l++;\n";
print DIR "  if (\$l % 100 == 0) {print STDERR \"  Inflecting lemmas...\$l\\r\"}\n";
print DIR "  if (\$log) {print STDERR \"Inflecting (\$w,\$ccnum,\$cat,\$t,\$v)\\n\"}\n";
print DIR "  my \$lemmas=();\n";
print DIR "  for \$w (&lemmatize(\$w,\$cat,\$t,\$v,\$rads,1,\\\$lemmas)) {\n";
print DIR "  if (\$w=~/<error/) {\n";
print DIR "      if (\$log) {print STDERR \"No appropriate radical found\\n\"}\n";
print DIR "      print &detranslitterate(\$w);\n";
print DIR "      return;\n";
print DIR "  }\n";
print DIR "  if (\$log) {print STDERR \"Using radical (\$w)\\n\"}\n";
print DIR "  if (!&check_lemma(\$w)) {\n";
print DIR "      if (\$log) {print STDERR \"Incorrect radical\\n\"}\n";
print DIR "      print &detranslitterate(\$w).\"\\t<error>\\n\";\n";
print DIR "      return;\n";
print DIR "  }\n";
print DIR "  \$nf=0;\n";
print DIR "  \$w=~/^(.*)\\t([^:\\t]+)(.*)\$/ || die (\"Error in lemma format : \\\"\$w\\\"\\n\");\n";
print DIR "  \$r=\$1; \$t=\$2; \$v=\$3; \$_=\$r;\n";
print DIR "\n";
print DIR "\n";
print DIR "  if (0) {\n";
for my $cat (keys %tables_by_cat) {
  print DIR "  } elsif (\$cat eq \"$cat\") {\n";
  print DIR "      if (0) {\n";
  for my $table (keys %{$tables_by_cat{$cat}}) {
    print DIR "      } elsif (\$t eq \"$table\") {\n";
    for my $tag (keys %{$dir_i{$table}}) {
      if ($tag!~/^D\[/) {
	print DIR "        if (\$tag eq \"\" || \$tag eq \"$tag\") {\n";
	for my $variant (keys %{$dir_i{$table}{$tag}}) {
	  my $variants=join("|",keys %{$dir_i{$table}{$tag}}); 
	  for my $var (0..$#{$dir_i{$table}{$tag}{$variant}}) {
	    print DIR "          if ( ";
	    if ($show{$table}{$tag}{$variant}[$var]<1) {print DIR "\$showall && "}
	    for my $locv (keys %{$exclvars{$table}{$variant}}) {
	      print DIR "\$v!~/:$locv(:|\$)/ &&";
	    }
	    if ($locradsexcept{$table}{$tag}{$variant}[$var] ne "##NONE##") {
	      print DIR "\$r!~/^".$locradsexcept{$table}{$tag}{$variant}[$var]."\$/ &&";
	    }
	    print DIR "\$r=~s/".$dir_i{$table}{$tag}{$variant}[$var]."\$/".$dir_o{$table}{$tag}{$variant}[$var]."\\t".$tag."/oi) {\n";
	    print DIR "            \$r=&wrapfusion(\$r);\n";
	    if ($show{$table}{$tag}{$variant}[$var]>0) {
	      print DIR "            if (\$showall) {\n    ";
	    }
	    if ($synt{$table}{$tag}{$variant}[$var] ne "") {
	      print DIR "            push_result(\$ccnum,\"".$synt{$table}{$tag}{$variant}[$var]."\",\"xxx\",\$_,\$r,\"\$t\$v\")";
	    } else {
	      print DIR "            push_result(\$ccnum,\"Default\",\"xxx\",\$_,\$r,\"\$t\$v\")";
	    }
	    print DIR ";\n";
	    if ($show{$table}{$tag}{$variant}[$var]>0) {
	      print DIR "            } else {\n";
	      print DIR "                \$r=~s/\\t.*//;\n";
	      print DIR "                \$r=&remove_sep(\$r);\n";
	      if ($show_pattern{$table}{$tag}{$variant}[$var]=~/^(.*)\#(.*)$/) {
		print DIR "                \$forms{\"$1\".\$r.\"$2\"}=".($show{$table}{$tag}{$variant}[$var]).";\n";
	      } else {
		print DIR "                \$forms{\"$show_pattern{$table}{$tag}{$variant}[$var]\"}=".($show{$table}{$tag}{$variant}[$var]).";\n";
	      }
	      print DIR "            }\n";
	    }
	    print DIR "            \$r=\$_;\n";
	    print DIR "            \$nf++;\n";
	    print DIR "          }\n";
	  }
	}
	print DIR "        }\n";
      }
    }
  }
  print DIR "    }\n";
}
print DIR "  }\n";
print DIR "  if (\$nf==0 && \$w=~/^([^\\t]*)\\t([^\\t]*)\$/) {\n";
print DIR "    if (\$showall) {\n";
print DIR "      push_result(\$ccnum,\"?\",\"?\",\"?\",\"?\t?\",\"?\")";
print DIR "    } else {\n";
print DIR "      print \"$2, \";\n";
print DIR "    }\n";
print DIR "  }\n";
print DIR "  my \$out;\n";
print DIR "  if (!\$showall) {\n";
print DIR "    \$out=&detranslitterate(join(\", \",sort {\$forms{\$a} <=> \$forms{\$b} || \$a cmp \$b} keys \%forms));\n";
print DIR "    \$out=~s/, *(?=,)/ /g;\n";
print DIR "    while (\$out=~s/^ *, *//g) {}\n";
print DIR "    \$out=~s/  +/ /g;\n";
print DIR "    \$out=~s/^ //g;\n";
print DIR "    \$out=~s/ \$//g;\n";
print DIR "    print \"\$out\\n\";\n";
print DIR "    \%forms=();\n";
print DIR "  }\n";
print DIR "  last if (\$onerad);\n";
print DIR "  }\n";
print DIR "}\n";
print DIR "\n";

print DIR <<PUSHRESULT;
sub push_result {
    my \$ccnum=shift;
    my \$msprop=shift;
    my \$in=shift;
    my \$orig=shift;
    my \$result=shift;
    my \$table=shift;
    if (\$nosep) {
       \$result=&remove_sep(\$result);
    }
    if (\$synt) {
      push(\@{\$msprop[\$ccnum]},\"\\t\".\$msprop);
    }
    \$result=&detranslitterate(\$result);
    \$result=~/^(.*)\\t(.*)\$/;
    push(\@{\$result[\$ccnum]},\$1);
    push(\@{\$tag[\$ccnum]},\$2);
    push(\@{\$table[\$ccnum]},\$table);
}
PUSHRESULT

print DIR <<WRAPFUSION;
sub wrapfusion {
    my \$r=shift;
    if (\$log) {print STDERR \"Before fusion (\$r)\\n\"}
    \$r=&fusion(\$r);
    if (\$log) {print STDERR \"After fusion (\$r)\\n\"}
    return \$r;
}
WRAPFUSION
close(DIR);

open(CHECK,">$output_dir$tables_file.check.pl") || die "########## Fichier de tables $tables_file.check.pl inouvrable.\n";
binmode CHECK, ":utf8";
print CHECK "use utf8;\n";
print CHECK "\$prefixes=qr/$prefixes/;\n";
print CHECK "\n";
for my $p (keys %pairs) {
  for (keys %{$pairs{$p}}) {
    print CHECK "\$".$p."{\"".$_."\"}=\"".$pairs{$p}{$_}."\";\n";
  }
}
print CHECK "\n";
print CHECK "our \%terms;\n";
for my $term (keys %rev_i_by_terms) {
  print CHECK "\$terms{\"$term\"}=1;\n";
}
print CHECK "\n";
print CHECK "sub check_lemma {\n";
print CHECK "  \$w=shift;\n";
print CHECK "  chomp(\$w);\n";
print CHECK "  \$w =~ s/$sep/ /g;\n";
print CHECK "  \$w=~/^([^\\t]*)\\t([^:\\t]+)(.*)\$/ || die (\"Bad input format for check_lemma : \\\"\$w\\\"\\n\");\n";
print CHECK "  \$r=\$1; \$t=\$2; \$v=\$3;\n";
print CHECK "  \$r=~s/___.*//;\n";
print CHECK "  \$_=\$r; \$rep=1;\n";
print CHECK "\n";
print CHECK "  if (0) {\n";
my %printed_tests=(); my $printedsomething;
my $newline;
my $complete_test;
for my $table (keys %chk_i) {
    %printed_tests=();
    $printedsomething=0;
    $complete_test="";
    if ($radsexcept{$table} ne "##NONE##") {
	$complete_test.="    if (\$r=~/^".$radsexcept{$table}."\$/) {return 0}";
	$printedsomething=1;
    }
    for my $tag (keys %{$chk_i{$table}}) {
	if ($tag!~/D\[#/) { # a confirmer
	    $newline="";
	    for my $variant (keys %{$chk_i{$table}{$tag}}) {
		for my $var (0..$#{$chk_i{$table}{$tag}{$variant}}) {
		    $newline.="    ";
		    $newline.="if (\$v=~/(^|:)$variant(:|\$)/) " if ($variant ne "");
		    $newline.="{if (";
		    if ($locradsexcept{$table}{$tag}{$variant}[$var] ne "##NONE##") {
			$newline.="\$r!~/^".$locradsexcept{$table}{$tag}{$variant}[$var]."\$/ && ";
		    }
		    if ($chk_i{$table}{$tag}{$variant}[$var] eq "(.*)") {
		      $newline.="1";
		    } else {
		      $newline.="\$r=~/^".$chk_i{$table}{$tag}{$variant}[$var]."\$/oi";
		    }
		    $newline.=") {\$rep=1;} elsif (\$rep==-1) {\$rep=0}}\n";
		}
	    }
	    if (!defined($printed_tests{$newline})) {
		if ($newline !~ /    \{if \(1\) \{\$rep=1;\} elsif \(\$rep==-1\) \{\$rep=0\}\}\n/) {
		  $complete_test .= "    \$rep=-1;\n";
		  $complete_test .= $newline;
		  $complete_test .= "    if (\$rep == 0) {return 0} # tag $tag, table $table\n";
		  $printed_tests{$newline}=1;
		  $printedsomething=1;
		}
	    }
	}
    }
    if ($printedsomething) {
      print CHECK "  } elsif (\$t eq \"$table\") {\n$complete_test";
    }
}
print CHECK "  }\n";
print CHECK "  return 1;\n";
print CHECK "}\n";
print CHECK "\n";
print CHECK "sub translitterate {\n";
print CHECK "    \$w=shift;\n";
for (sort {length($b) <=> length($a)} keys %translitt) {
    print CHECK "    \$w=~s/$_/$translitt{$_}/g;\n";
}
print CHECK "    return \$w;";
print CHECK "}\n";
print CHECK "\n";
print CHECK "sub detranslitterate {\n";
print CHECK "    \$w=shift;\n";
for (sort {length($b) <=> length($a)} keys %translitt) {
    print CHECK "    \$w=~s/$translitt{$_}/$_/g;\n";
}
print CHECK "    return \$w;\n";
print CHECK "}\n";

print CHECK "sub fusion {\n";
print CHECK "  my \$s=shift;\n";
print CHECK "  my \$gotfusion=1;\n";
print CHECK "  my \$loopn=0;\n";
print CHECK "  while (\$gotfusion==1) {\n";
print CHECK "    if (\$loopn++>20) {die((\" \"x50).\"\rInfinite loop detected on form \$s        \\n\");};\n";
print CHECK "    \$gotfusion=0;\n";

my $from;
my $from_endistab;
my $to;
for my $fr (0..$#fusionarray) {
  $from=$fusionarray[$fr]{dir_in};
  if ($from ne "" && $fusionarray[$fr]{is_final}<1) { #  && $fusionrank{$from}==$fr
    $from_endistab=$from;
    $from_endistab=~s/\$$/(?=\\t)/;
    $to=$fusionarray[$fr]{dir_sepout};
    $to=~s/^\^//;
    print CHECK "    if (\$s=~s/$from_endistab/$to/o) {\$gotfusion=1} # rank = $fr\n";
  }
}
print CHECK "  }\n";
for my $fr (0..$#fusionarray) {
  $from=$fusionarray[$fr]{dir_in};
  if ($from ne "" && $fusionarray[$fr]{is_final}==1) { #  && $fusionrank{$from}==$fr
    $from_endistab=$from;
    $from_endistab=~s/\$$/(?=\\t)/;
    $to=$fusionarray[$fr]{dir_sepout};
    $to=~s/^\^//;
    print CHECK "  \$s=~s/$from_endistab/$to/o; # rank = $fr\n";
  }
}
print CHECK "  return \$s;\n";
print CHECK "}\n";


print CHECK "sub rev_fusion {\n";
print CHECK "  my \$orig=shift;\n";
print CHECK "  my \$realorig=shift;\n";
print CHECK "  \$_=\$orig;\n";
print CHECK "  my \$level=shift;\n";
print CHECK "  my \$result=shift;\n";
print CHECK "  my \$pos=shift;\n";
print CHECK "  my \$fusionok=shift;\n";
print CHECK "  my \$firstrank=(shift || 100000);\n";
print CHECK "  if (\$_!~/$sep/) {die \"Error: input of rev_fusion (\$_) must have a split mark\"}\n";
print CHECK "    if ((!defined(\$result->{\$_}) && (\$fusionok || (&fusion(\$_.\"\\t\") eq \$realorig.\"\\t\"))) || \$result->{\$_}>\$pos) {\n";
print CHECK "       if (defined(\$terms{\$2})) {\$result->{\$_}=\$pos}\n";
print CHECK "       \$fusionok=1;\n";
print CHECK "    }\n";
my $rfi;
my $w;
for my $invfr (0..$#fusionarray) {
  my $fr = $#fusionarray-$invfr;
  $w=$fusionarray[$fr]{rev_in};
  if ($fusionarray[$fr]{is_rev}==1 && $fusionarray[$fr]{is_final}==1) {
    $rfi=$fusionarray[$fr]{rev_out};
    print CHECK "  if (\$firstrank>$fr && s/$w/$rfi/o) {\n";
    print CHECK "    \$locfusionok=(\$fusionok || (&fusion(\$_.\"\\t\") eq \$realorig.\"\\t\"));\n";
    print CHECK "    if (\$locfusionok) {\n";
    print CHECK "       if (defined(\$terms{\$2})) {\$result->{\$_}++}\n";
    print CHECK "    }\n";
    print CHECK "    if (\$level>1) {&rev_fusion(\$_,\$realorig,\$level-1,\$result,\$pos,\$locfusionok,$fr)}\n";
    print CHECK "    \$_=\$orig;\n";
    print CHECK "  }\n";
  }
}
for my $invfr (0..$#fusionarray) {
  my $fr = $#fusionarray-$invfr;
  $w=$fusionarray[$fr]{rev_in};
  if ($fusionarray[$fr]{is_rev}==1 && $fusionarray[$fr]{is_final}<1) {
    $rfi=$fusionarray[$fr]{rev_out};
    print CHECK "  if (s/$w/$rfi/o) {\n";
    print CHECK "    \$locfusionok=(\$fusionok || (&fusion(\$_.\"\\t\") eq \$realorig.\"\\t\"));\n";
    print CHECK "    if (\$locfusionok) {\n";
    print CHECK "       if (defined(\$terms{\$2})) {\$result->{\$_}++}\n";
    print CHECK "    }\n";
    print CHECK "    if (\$level>1) {&rev_fusion(\$_,\$realorig,\$level-1,\$result,\$pos,\$locfusionok,10000)}\n";
    print CHECK "    \$_=\$orig;\n";
    print CHECK "  }\n";
  }
}
print CHECK "  return 1;\n";
print CHECK "}\n";
my $rfr=0;
for my $invfr (0..$#fusionarray) {
  my $fr = $#fusionarray-$invfr;
  if ($fusionarray[$fr]{is_rev}==1) {
    $fusionarray[$fr]{rev_out}=~s/\$/\\\$/g;
    $fusionarray[$fr]{rev_out}=~s/^\^//;
    $fusionarray[$fr]{rev_out}=~s/^/\\"/;
    $fusionarray[$fr]{rev_out}=~s/$/\\"/;
    $fusionarray[$fr]{rev_out}=~s/\\\$(?=[^0-9])/\\".\\\$/g;
    $fusionarray[$fr]{rev_out}=~s/}/}.\\"/g;
    $fusionarray[$fr]{rev_out}=~s/\\\"\\\"\.//g;
    $fusionarray[$fr]{rev_out}=~s/\.\\\"\\\"//;
    print CHECK "push(\@rev_in";
    if ($fusionarray[$fr]{is_final}==1) {print CHECK "_final"}
    print CHECK ",qr/$fusionarray[$fr]{rev_in}/o);  # $rfr\n";
    print CHECK "push(\@rev_out";
    if ($fusionarray[$fr]{is_final}==1) {print CHECK "_final"}
    print CHECK ",\"$fusionarray[$fr]{rev_out}\");\n";
    $rfr++;
  }
}
print CHECK "sub new_rev_fusion {\n";
print CHECK "  my \$orig=shift;\n";
print CHECK "  my \$realorig=shift;\n";
print CHECK "  \$_=\$orig;\n";
print CHECK "  my \$level=shift;\n";
print CHECK "  my \$result=shift;\n";
print CHECK "  my \$pos=shift;\n";
print CHECK "  my \$fusionok=shift;\n";
print CHECK "  my \$firstrank=(shift || 0);\n";
print CHECK "  if (\$_!~/$sep/) {die \"Error: input of rev_fusion (\$_) must have a split mark\"}\n";
print CHECK "  if ((!defined(\$result->{\$_}) && (\$fusionok || (&fusion(\$_.\"\\t\") eq \$realorig.\"\\t\"))) || \$result->{\$_}>\$pos) {\n";
print CHECK "     /$sep([^$sep]*)\$/;\n";
print CHECK "     if (defined(\$terms{\$1})) {\$result->{\$_}=\$pos}\n";
print CHECK "     \$fusionok=1;\n";
print CHECK "  }\n";
print CHECK "  for my \$fr (0..\$#rev_in_final) {\n";
print CHECK "    if (\$firstrank<\$fr && s/\$rev_in_final[\$fr]/\$rev_out_final[\$fr]/ee) {\n";
print CHECK "      \$locfusionok=(\$fusionok || (&fusion(\$_.\"\\t\") eq \$realorig.\"\\t\"));\n";
print CHECK "      if (\$locfusionok) {\n";
print CHECK "        /$sep([^$sep]*)\$/;\n";
print CHECK "        if (defined(\$terms{\$1})) {\$result->{\$_}++}\n";
print CHECK "      }\n";
print CHECK "      if (\$level>1) {&rev_fusion(\$_,\$realorig,\$level-1,\$result,\$pos,\$locfusionok,\$fr)}\n";
print CHECK "      \$_=\$orig;\n";
print CHECK "    }\n";
print CHECK "  }\n";
print CHECK "  for my \$fr (0..\$#rev_in) {\n";
print CHECK "    if (\$firstrank>\$fr && s/\$rev_in[\$fr]/\$rev_out[\$fr]/ee) {\n";
print CHECK "      \$locfusionok=(\$fusionok || (&fusion(\$_.\"\\t\") eq \$realorig.\"\\t\"));\n";
print CHECK "      if (\$locfusionok) {\n";
print CHECK "        /$sep([^$sep]*)\$/;\n";
print CHECK "        if (defined(\$terms{\$1})) {\$result->{\$_}++}\n";
print CHECK "      }\n";
print CHECK "      if (\$level>1) {&rev_fusion(\$_,\$realorig,\$level-1,\$result,\$pos,\$locfusionok,10000)}\n";
print CHECK "      \$_=\$orig;\n";
print CHECK "    }\n";
print CHECK "  }\n";
print CHECK "  return 1;\n";
print CHECK "}\n";
print CHECK <<COMPUTEPOSSIBLESPLITS;
sub compute_possible_splits {
  \$_=shift;
  \$_=&translitterate(\$_);
  my \$orig=\$_;
  my \$possible_splits=shift;
  my \$length=length(\$orig);
  for \$pos (0..\$length) {
      substr(\$_,\$pos,0)=\"$sep\";
      &rev_fusion(\$_,\$_,4,\$possible_splits,\$pos,0);
      \$_=\$orig;
  }
  my \$fus;
  for (keys %\$possible_splits) {
    \$fus=&remove_sep(&fusion(\$_.\"\\t\"));
    if (\$fus ne \$orig.\"\\t\") {
       delete(\$possible_splits->{\$_});
    }
  }
  return 1;
}
COMPUTEPOSSIBLESPLITS

my $variant2;
print CHECK "sub try_to_lemmatize {\n";
print CHECK "  my \$w=shift;\n";
print CHECK "  my \$cat=shift;\n";
print CHECK "  my \$table=shift;\n";
print CHECK "  my \$v=shift;\n";
print CHECK "  my \$o=\$w;\n";
print CHECK "  \@possible_results=();\n";
print CHECK "  if (0) {\n";
for my $cat (keys %tables_by_cat) {
  print CHECK "  } elsif (\$cat eq \"$cat\") {\n";
  print CHECK "    if (0) {\n";
  for my $table (keys %{$tables_by_cat{$cat}}) {
    print CHECK "    } elsif (\$table eq \"$table\") {\n";
    my $tag = $canonical_tag{$table};
    for my $variant (keys %{$rev_i{$table}{$tag}}) {
      for my $var (0..$#{$rev_i{$table}{$tag}{$variant}}) {
	print CHECK "      if (";
	for my $locv (keys %{$exclvars{$table}{$variant}}) {
	  print CHECK "\$v!~/:$locv(:|\$)/ &&";
	}
	print CHECK "\$w=~s/^".$rev_i{$table}{$tag}{$variant}[$var]."\$/".$rev_o{$table}{$tag}{$variant}[$var]."\\t".$table."/o) {\n";
	print CHECK "        if (1";
#	if ($locrads{$table}{$tag}{$variant}[$var] ne $all) {
#	  print CHECK " && \$w=~/^".$locrads{$table}{$tag}{$variant}[$var]."\\t/";
#	}
#	if ($rads{$table} ne ".*") {
#	  print CHECK " && \$w=~/^".$rads{$table}."\\t/";
#	}
	if ($locradsexcept{$table}{$tag}{$variant}[$var] ne "##NONE##") {
	  print CHECK " && \$w!~/^".$locradsexcept{$table}{$tag}{$variant}[$var]."\\t/";
	}
	if ($radsexcept{$table} ne "##NONE##") {
	  print CHECK " && \$w!~/^".$radsexcept{$table}."\\t/";
	}
	print CHECK ") {\n";
	if ($variant ne "") {$variant2=":".$variant} else {$variant2=""}
	print CHECK "          push(\@possible_results,\"\$w\".sortvlist(\$v.\"$variant2\"));\n";
	print CHECK "        }\n";
	print CHECK "        \$w=\$o;\n";
	print CHECK "      }\n";
      }
    }
  }
  print CHECK "    }\n";
}
print CHECK "  }\n";
print CHECK "  return \@possible_results;\n";
print CHECK "}\n";

print CHECK <<LEMMATIZE;
sub lemmatize {
  my \$w=shift;
  my \$cat=shift;
  my \$t=shift;
  my \$v=shift;
  my \$rads=shift;
  my \$die_if_pb=shift;
  my \@possible_lemmas=();
  my \$length;
  if (!\$rads) {
    my \%possible_splits=();
    my \$origw=\$w;
    my \$lnumber=0;
    for \$pos (0..length(\$origw)) {
#        substr(\$w,length(\$origw)-\$pos,0)=\"$sep\";
        \$length = length(\$origw)-\$pos; \$w =~ s/^(.{\$length})/\\1$sep/;
        \$w=&translitterate(\$w);
        &rev_fusion(\$w,\$w,2,\\%possible_splits,\$pos,0);
        for (keys \%possible_splits) {
            \@_=&try_to_lemmatize(\$_,\$cat,\$t,\$v);
            if (\$#_ >= 0) {push(\@possible_lemmas,\@_);}
        }
        \$lnumber=\$#possible_lemmas+1;
#        if (\$lnumber>1) {return &detranslitterate(\$origw).\"\t<error[\$lnumber:\".join(\",\",\@possible_lemmas).\"]>\\n\";}
        if (\$lnumber >= 1) {last;} # \$w=~s/^([^\t]+):(?=.)/\$1/;
        else {\$w=\$origw;\%possible_splits=();}
    }
    if (\$lnumber==0) {
      if (\$die_if_pb) {return &detranslitterate(\$origw).\"\t<error[\$lnumber]>\\n\";}
    }
  } else {
    push(\@possible_lemmas,\$w.\"\\t\".\$t);
  }
  return \@possible_lemmas;
}
LEMMATIZE
print CHECK "sub compute_canonical_forms {\n";
print CHECK "  my \$r=shift;\n";
print CHECK "  my \$cat=shift;\n";
print CHECK "  my \$t=shift;\n";
print CHECK "  my \$v=shift;\n";
print CHECK "  my \@result;\n";
print CHECK "  my \$pushedsomething;\n";
print CHECK "  my \$oldr=\$r;\n";
print CHECK "  if (\$v =~ /^[^:]/) {\$v=\":\".\$v}\n";
print CHECK "  my \$oldv=\$v;\n";
print CHECK "  if (0) {\n";
for my $cat (keys %tables_by_cat) {
  print CHECK "  } elsif (\$cat eq \"$cat\") {\n";
  print CHECK "      if (0) {\n";
  for my $table (keys %{$tables_by_cat{$cat}}) {
    print CHECK "      } elsif (\$t eq \"$table\") {\n";
    my $tag=$canonical_tag{$table};
    for my $variant (keys %{$dir_i{$table}{$tag}}) {
      my $variants=join("|",keys %{$dir_i{$table}{$tag}}); 
      for my $var (0..$#{$dir_i{$table}{$tag}{$variant}}) {
	print CHECK "        if ( ";
	if ($show{$table}{$tag}{$variant}[$var]<1) {print CHECK "1 && "}
	for my $locv (keys %{$exclvars{$table}{$variant}}) {
	  print CHECK "\$v!~/:$locv(:|\$)/ &&";
	}
	if ($locradsexcept{$table}{$tag}{$variant}[$var] ne "##NONE##") {
	  print CHECK "\$r!~/^".$locradsexcept{$table}{$tag}{$variant}[$var]."\$/ && ";
	}
	print CHECK "\$r=~s/".$dir_i{$table}{$tag}{$variant}[$var]."\$/".$dir_o{$table}{$tag}{$variant}[$var]."\\t".$tag."/o) {\n";
	print CHECK "          \$r=&fusion(\$r);\n";
	print CHECK "          \$r=~s/\\t.*\$//;\n";
	if ($variant ne "") {
	  print CHECK "          if (\$v !~ /:$variant(:|\$)/) {\$v.=\":$variant\"}\n";
	}
	print CHECK "          push(\@result,\"\$r\\t\$t\$v\");\n";
	print CHECK "          \$pushedsomething=1;\n";
	print CHECK "          \$r=\$oldr; \$v=\$oldv\n";
	print CHECK "        }\n";
      }
    }
    print CHECK "        if (\$pushedsomething) {return \\\@result}\n";
  }
  print CHECK "    }\n";
}
print CHECK "  }\n";
print CHECK "  return \@result;\n";
print CHECK "}\n";
print CHECK <<SORTVLIST;
sub sortvlist {
    my \$vl=shift;
    \$vl=~s/^://;
    if (\$vl eq "") {return ""}
    \$vl=":".join(":",sort split(/\:/,\$vl));
    while (\$vl=~s/(:[^:]+)\\1/\\1/) {}
    return \$vl;
}
SORTVLIST
print CHECK <<REMOVESEP;
sub remove_sep {
  my \$r=shift;
#  if (\$r=~/_.*_.*_/ && \$r!~/ /) {
#    \$r=~s/^([^_]*)_/\\1/;
#    \$r=~s/_([^_]*)\$/\\1/;
#  } else {
#    while (\$r=~s/^(.*[^_])_([^_]|\\t|\$)/\$1\$2/ || \$r=~s/__\\t/_\\t/ || \$r=~s/^_//) {}
#  }
  \$r =~ s/$sep//g;
  return \$r;
}
REMOVESEP
print CHECK "1;\n";
close(CHECK);

open(PDER,">$output_dir$tables_file.pder.pl") || die "########## Fichier de tables $tables_file.pder.pl inouvrable.\n";
binmode PDER, ":utf8";
print PDER "#!/usr/bin/env perl\n";
print PDER <<UTF8;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
UTF8
print PDER "my \$pwd = \_\_FILE\_\_;\n";
print PDER "\$pwd =~ s/^(.*[^\\/]\\/).*/\\1/;\n";
print PDER "require(\"\$pwd/$tables_file.check.pl\");\n";
print PDER "\$prefixes=qr/$prefixes/;\n";
print PDER "while (\$option=shift) {\n";
print PDER "  if (\$option eq \"-nosep\") {\$nosep=1}\n";
print PDER "  elsif (\$option eq \"-log\") {\$log=1}\n";
print PDER "}\n";
print PDER "while (<>) {\n";
print PDER "  chomp;\n";
print PDER "  s/^#.*//;\n";
print PDER "  next if (/^\\s*\$/);\n";
print PDER "  next if (/^[^\\t]*\\t0\\t/);\n";
print PDER "  /^(.*?)(\\t([^-:\\t\\d]+)([^:\\t]*)[^\\t]*)/ || die (\"Bad input format : \\\"\$_\\\"\\n\");\n";
print PDER "  \$w=\$1;\n  \$class=\$2;\n";
print PDER "  \$cat=\$3;\n";
print PDER "  \$origw=\$w;\n";
print PDER "  \$t=\$3.\$4;\n";

print PDER "  if (\$log) {print STDERR \"Computing derivations for (\$w,,\$cat,\$t,\$v)\\n\"}\n";
print PDER "  for \$w (&lemmatize(\$w,\$cat,\$t,\"\",0,1)) {\n";
print PDER "  if (\$w=~/<error/) {\n";
print PDER "      if (\$log) {print STDERR \"No appropriate radical found\\n\"}\n";
print PDER "      next;\n";
print PDER "  }\n";
print PDER "  if (\$log) {print STDERR \"Using radical (\$w)\\n\"}\n";
print PDER "  if (!&check_lemma(\$w)) {\n";
print PDER "      if (\$log) {print STDERR \"Incorrect radical\\n\"}\n";
print PDER "      next;\n";
print PDER "  }\n";

print PDER "  \$_=\$w;\n";
print PDER "  if (!&check_lemma(\$_)) {\n";
print PDER "      next;\n";
print PDER "  }\n";
print PDER "  \$_=\$w;\n";
print PDER "  \$l++;\n";
print PDER "  \$nf=0;\n";
print PDER "  if (\$l % 100 == 0) {print STDERR \"\$l\\r\"}\n";
print PDER "  /^(.*)\\t([^:\\t]+)(.*)\$/ || die (\"Bad input format : \\\"\$_\\\"\\n\");\n";
print PDER "  \$r=\$1; \$t=\$2; \$v=\$3; \$_=\$r;\n";
print PDER "\n";
print PDER "\n";
print PDER "  if (0) {\n";
for my $cat (keys %tables_by_cat) {
    print PDER "  } elsif (\$cat eq \"$cat\") {\n";
    print PDER "      if (0) {\n";
    for my $table (keys %{$tables_by_cat{$cat}}) {
	print PDER "      } elsif (\$t eq \"$table\") {\n";
	for my $tag (keys %{$derivations{$table}}) {
	    for my $var (0..$#{$derivations{$table}{$tag}}) {
		print PDER "        if (1 ";
		if ($locradsexcept{$table}{$tag}{""}[$var] ne "##NONE##") {
		    print PDER "&& \$r!~/^".$locradsexcept{$table}{$tag}{""}[$var]."\$/";
		}
		print PDER "&& \$r=~s/".$der_i{$table}{$tag}{""}[$var]."\$/".$der_o{$table}{$tag}{""}[$var]."\\t".$tag."/o) {\n";
		print PDER "          \$r=&fusion(\$r);\n";
		print PDER "            if (\$nosep) {\n";
                print PDER "                \$r=&remove_sep(\$r);\n";
		print PDER "            }\n";
		print PDER "          print \$origw.\"\$class\\t\".&detranslitterate(\$r).\"\\n\";\n";
		print PDER "          \$r=\$_;\n";
		print PDER "        }\n";
	    }
	}
    }
    print PDER "    }\n";
}
    print PDER "  }\n";
print PDER "}\n";
print PDER "}\n";
close(PDER);

open(DER,">$output_dir$tables_file.der.pl") || die "########## Fichier de tables $tables_file.der.pl inouvrable.\n";
binmode DER, ":utf8";
print DER "#!/usr/bin/env perl\n";
print DER <<UTF8;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
UTF8
print DER "my \$pwd = \_\_FILE\_\_;\n";
print DER "\$pwd =~ s/^(.*[^\\/]\\/).*/\\1/;\n";
print DER "require(\"\$pwd/$tables_file.check.pl\");\n";
print DER "\$prefixes=qr/$prefixes/;\n";
print DER "while (\$option=shift) {\n";
print DER "  if (\$option eq \"-vars\") {\$withvars=1}\n";
print DER "  elsif (\$option eq \"-nosep\") {\$nosep=1}\n";
print DER "}\n";
print DER "while (<>) {\n";
print DER "  chomp;\n";
print DER "  /([^\\t]*)(\\t([^-:\\t]+)([^:\\t]*)[^\\t]*)(?:\\t([^\\t]*)((?:\\t.*)?))?/ || die (\"Bad input format : \\\"\$_\\\"\\n\");\n";
print DER "  \$w=\$1;\n  \$class=\$2;\n";
print DER "  \$origw=\$w.\$class;\n";
print DER "  \$cat=\$3;\n";
print DER "  \$t=\$3.\$4;\n";
print DER "  \$fullname=\$5;\n";
print DER "  \$remainer=\$6;\n";
print DER "  \$w=~s/___.*//;\n";
print DER "  if (\$fullname eq \"\") { \$fullname = \"ALL\" }\n";
print DER "  while (\$fullname ne \"\") {\n";
print DER "    \$fullname=~s/^(?:(.*)\\@\\@)?([^\\@]+)/\$1/;\n";
print DER "    \$name=\$2;\n";
print DER "    for \$w2 (&lemmatize(\$w,\$cat,\$t,\"\",0,1)) {\n";
print DER "      next if (\$w2=~/<error/ || \$w2!~/\\t/);\n";
print DER "      \$_=\$w2;\n";
print DER "      if (!&check_lemma(\$_)) {\n";
print DER "          next;\n";
print DER "      }\n";
print DER "      \$_=\$w2;\n";
print DER "      \$l++;\n";
print DER "      \$nf=0;\n";
print DER "      if (\$l % 100 == 0) {print STDERR \"\$l\\r\"}\n";
print DER "      /^([^\\t]*?)((?:___[^\\t]*)?)\\t([^:\\t]+)(.*)\$/ || die (\"Bad input format : \\\"\$_\\\"\\n\");\n";
print DER "      \$r=\$1; \$var=\$2; \$t=\$3; \$v=\$4; \$_=\$r;\n";
print DER "      \$oldr=\$r;\n";
print DER "  \n";
print DER "      if (0) {\n";
for my $cat (keys %tables_by_cat) {
    print DER "      } elsif (\$cat eq \"$cat\") {\n";
    print DER "          if (0) {\n";
    for my $table (keys %{$tables_by_cat{$cat}}) {
	print DER "          } elsif (\$t eq \"$table\") {\n";
	for my $tag (keys %{$derivations{$table}}) {
	    for my $var (0..$#{$derivations{$table}{$tag}}) {
#	      if ($name{$table}{$tag}{""}[$var] ne "") {
		print DER "            if ((\$name eq \"ALL\" || \$name eq \"".$name{$table}{$tag}{""}[$var]."\") ";
		if ($locradsexcept{$table}{$tag}{""}[$var] ne "##NONE##") {
		  print DER "&& \$r!~/^".$locradsexcept{$table}{$tag}{""}[$var]."\$/";
		}
		print DER '&& $r=~s/'.$der_i{$table}{$tag}{""}[$var].'$/'.$der_o{$table}{$tag}{""}[$var]."\\t".$tag."/o) {\n";
		print DER "              \$r=&fusion(\$r);\n";
		print DER "              if (\$nosep) {\n";
#		print DER "                  \$r=~s/_//;\n";
		print DER "                  \$r=~s/$sep//;\n";
		print DER "              }\n";
		print DER "              \$r=&detranslitterate(\$r);\n";
		print DER "              \$r=~/([^\\t]*)(\\t([^-:\\t]+)([^:\\t]*)[^\\t]*)/;\n";
		print DER "              \$w=\$1;  \$class=\$2;\n";
		print DER "              \$cat=\$3;\n";
		print DER "              \$t=\$3.\$4;\n";
		print DER "              if (\$name eq \"ALL\") { print \$origw.\"\\t\".\$r.\$remainer.\"\\n\"; \$r=\$oldr} else {last}\n";
		print DER "            }\n";
#	      }
	    }
	}
    }
    print DER "        }\n";
}
print DER "      }\n";
print DER "    }\n";
print DER "    if (\$name ne \"ALL\" && \$fullname eq \"\") {print \$origw.\"\\t\".\$r.\$remainer.\"\\n\";}\n";
print DER "  }\n";
print DER "}\n";
close(DER);


open(REV,">$output_dir$tables_file.rev.pl") || die "########## Fichier de tables $tables_file.rev.pl inouvrable.\n";
binmode REV, ":utf8";
print REV "#!/usr/bin/env perl\n";
print REV <<UTF8;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
UTF8
print REV <<INTRO;
while (\$option=shift) {
  if (\$option eq \"-all\") {\$all=1}
  if (\$option eq \"-log\") {\$log=1}
#  if (\$option eq \"-fast\") {\$fast=1}
}
my \$pwd = \_\_FILE\_\_;
\$pwd =~ s/^(.*[^\\/]\\/).*/\\1/;
require("\$pwd/$tables_file.check.pl");
\$prefixes=qr/$prefixes/;
my (\$origlemma, \$lemma, \$tag, \$cat);
while (<>) {
  chomp;
  /^([^\\t]+)(?:\\t([^\\t]*)\\t([^\\t]*)\\t([^\\t]*))?\$/ || die \"Error in input: \$_\";
  \$lemma=\$2; \$tag=\$3; \$cat=\$4; \$_=\$1;
  \$origlemma=\$lemma;
  \$lemma=&translitterate(\$lemma);
  \$l++;
  if (\$l % 10 == 0) {print STDERR \"\$l\\r\"}
  \$orig=\$_;
  \$printed_something=0;
  \%possible_splits=();
  &compute_possible_splits(\$_,\\%possible_splits);
  for (keys \%possible_splits) {
    if (\$log) {print STDERR "Trying (1) \$_\\n"}
    &try_to_uninflect_1(\$_);
  }
  if ((!\$printed_something || \$all)
#      && !\$fast
    ) {
#    for (keys \%possible_splits) {
#      if (\$log) {print STDERR "Trying (2) \$_\\n"}
#      &try_to_uninflect_2(\$_);
#    }
  }
  if (!\$printed_something) {
    print "#".&detranslitterate(\$_)."\\t\$origlemma\\t\$tag\\t\$cat\\tUNKNOWN\\n";
  }
}
INTRO
for my $step (1..1) { # (1..2) ... mais ça fait parfois des fichiers perl trop gros qui segfaultent
  print REV "sub try_to_uninflect_$step {\n";
  print REV "  my \$w=shift;\n";
  print REV "  my \$o=\$w;\n";
  print REV "  \$w=~/$sep([^$sep]*)\$/;\n";
  for my $term (keys %rev_i_by_terms) {
    print REV "  if (\$w=~/$sep$term\$/) {\n";
    for my $table (keys %{$rev_i_by_terms{$term}}) {
      next if ($step == 1 && $fast{$table} eq "-");
      next if ($step == 2 && $fast{$table} ne "-");
      print REV "    if (\$cat eq \"\" || \$cat eq \"$cat_of_table{$table}\") {\n";
      for my $tag (keys %{$rev_i_by_terms{$term}{$table}}) {
	print REV "      if (\$tag eq \"\" || \$tag eq \"$tag\") {\n";
	if ($tag!~/^D\[/) {
	  for my $variant (keys %{$rev_i_by_terms{$term}{$table}{$tag}}) {
	    for my $var (0..$#{$rev_i_by_terms{$term}{$table}{$tag}{$variant}}) {
	      if ($rev_i{$table}{$tag}{$variant}[$var] eq $rev_i_by_terms{$term}{$table}{$tag}{$variant}[$var]) {
		# sinon c'est un cas fantome du au fait qu'on garde le meme numero de var dans rev_i_by_terms alors que le classement par terms fait que tous les var ne sont pas remplis
		print REV "        if (\$w=~s/^".$rev_i_by_terms{$term}{$table}{$tag}{$variant}[$var]."\$/".$rev_o{$table}{$tag}{$variant}[$var]."/oi) {\n";
		print REV "          if (1";
# 		if ($locrads{$table}{$tag}{$variant}[$var] ne $all) {
# 		  print REV " && \$w=~/^".$locrads{$table}{$tag}{$variant}[$var]."\$/";
# 		}
# 		if ($rads{$table} ne ".*") {
# 		  print REV " && \$w=~/^".$rads{$table}."\$/";
# 		}
		if ($locradsexcept{$table}{$tag}{$variant}[$var] ne "##NONE##") {
		  print REV " && \$w!~/^".$locradsexcept{$table}{$tag}{$variant}[$var]."\$/";
		}
		if ($radsexcept{$table} ne "##NONE##") {
		  print REV " && \$w!~/^".$radsexcept{$table}."\$/";
		}
		print REV ") {\n";
		print REV "            for (\@{\&compute_canonical_forms(\$w,\"$cat_of_table{$table}\",\"$table\",\"$variant\")}) {\n";
#		print REV "              s/^([^\\t]*)#/\$1/;\n";
		print REV "              while (s/^([^\\t]*)$sep/\$1/) {}\n";
		print REV "              if (\$_ ne \"\" && (\$lemma eq \"\" || /^\$lemma\\t/)) {\n";
		print REV "                \$_=&detranslitterate(\$_);\n";
		print REV "                s/\\t/\\t$tag\\t/;\n";
		print REV "                print &detranslitterate(\$orig).\"\\t\".&detranslitterate(\$_).\"\\n\";\n";
		print REV "                \$printed_something=1;\n";
		print REV "              }\n";
		print REV "            }\n";
		print REV "          }\n";
		print REV "          \$w=\$o;\n";
		print REV "        }\n";
	      }
	    }
	  }
	}
	print REV "      }\n";
      }
      print REV "    }\n";
    }
    print REV "  }\n";
  }
  print REV "}\n";
}
close(REV);
print STDERR "done\n";

sub dotranslitterate {
    my $w=shift;
    for (sort {length($b) <=> length($a)} keys %translitt) {
	$w=~s/^$_/$translitt{$_}/;
	$w=~s/(?<=[^\\])$_/$translitt{$_}/g;
    }
    return $w;
}

sub normalize_classes {
  my $p = shift;
  $p=~s/\\\*/[:##ANY##:]/g;
  $p=~s/\\([^\^])/[:$1:]/g;
  return $p;
}

sub unquote {
  my $s=shift;
  my $default=shift;
  if ($s eq "") {return $default}
  elsif ($s=~s/^\"(.*)\"$/$1/) {
#    while ($s=~s/\(([^?\)][^\)]*)\)/(?:\1)/g) {};
    return $s;
  } else {die "Error: tried to unquote string \"$s\""}
}

sub my_quotemeta {
  my $s = shift;
  $s =~ s/([\/\[\]\(\)\?\*\+\\])/\\\1/g;
  return $s;
}
