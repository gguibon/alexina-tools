#!/usr/bin/env perl
# $Id: syntax.pl 2250 2015-03-12 13:53:36Z sagot $

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $state=0; my ($fulltemplate,$template,@templatevars);my $functorslist=""; my %templates=(); my %ftemplates=();
my $altnum=0;
my %scat=(); my %haspred=(); my %macrosL=(); my %macrosG=();
my $morpho=""; my $form=""; my $lemma="";
my %cat=(); my %w=();
my $obj=""; my $suite=""; my $fin="";
my @prop; my @msprop; my @base; my @target; my @must_work; my %duplications; my %propstr2num;
#my @funct; my @fmsprop; my @fbase; my @ftarget; my @fmust_work; my %dduplications; my %functstr2num;
 my $linenumber=0;
my %syntfunc;


my $output_dir="";
my $patternsfile="";
my $fpatternsfile="";
my $propfile="";
while (1) {
    $_=shift;
    if ($_!~/^\-/) {last;}
    elsif (/^--?od=(.+)$/ || /^--?output_dir=(.+)$/i) {$output_dir=$1;}
    elsif (/^--?p=(.+)$/ || /^--?patterns=(.+)$/i) {$patternsfile=$1;}
    elsif (/^--?pr=(.+)$/ || /^--?props=(.+)$/i) {$propfile=$1;}
    elsif (/^--?fp=(.+)$/ || /^--?fpatterns=(.+)$/i) {$fpatternsfile=$1;}
    else {die "unknown option : $_"}
}
if ($output_dir=~/[^\/]$/) {$output_dir.="/"}

my $temp; my $temp2; my $temp3;
open (PROP,"<$propfile") || die ("########## Fichier de propriétés introuvable ($propfile)\n");
binmode PROP, ":utf8";
for (<PROP>) {
    $linenumber++;
    chomp;
    s/^\#.*$//;
    s/([^\"])\#.*$/\1/;
    if (/./) {
	if (/^([^\t]+)\t([^\t]*)(?:\t([^\t]*))?$/ && $state>0) {
	    $state=2;
	    $temp=$1;
	    $temp2=$2;
	    $temp3=$3;
	    push(@target,$temp3);
	    if ($temp2 eq "") {$temp2="<>";}
	    if ($temp=~s/^\?//) {push(@must_work,0)} else {push(@must_work,1)}
	    if ($temp=~/^[A-Z]/) {push(@msprop,$temp)}
	    push(@prop,$temp);
	    push(@base,quotemeta($temp2));
	    push(@{$propstr2num{$temp}},$#prop);
	} elsif (/^FUNCTIONS[ \t]*$/ && $state==0) {
	    $state=1;
	} elsif (/^\t([^ \t]+)[ \t]*$/ && $state==1) {
	    $syntfunc{$1}=$linenumber;
	} else {
	    die ("########## Erreur dans le fichier de props. Ligne concernée n° $linenumber:\n$_\n");
	}
    }
}
close PROP;
$state=0;

my %inheritsfrom=();
my %finheritsfrom=();
my @answerarray=();

my $extendedpatternsfile=$patternsfile;
$extendedpatternsfile=~s/\.p$/.ep/;
$extendedpatternsfile=~s/^.*\///;
open(LOG,">$output_dir$extendedpatternsfile") || die ("########## Fichier des classes étendues $output_dir$extendedpatternsfile inouvrable\n");

print STDERR "  Parsing syntactic patterns...";

&load_patterns_file($patternsfile,\%inheritsfrom,\%templates,1);

if ($fpatternsfile ne "") {
  &load_patterns_file($fpatternsfile,\%finheritsfrom,\%ftemplates,0);
}

debugprint("\n# Examples:\n");
my $currtempl;
for $currtempl (keys %templates) {
    debugprint("#\n");
    for $altnum (keys %{$inheritsfrom{$currtempl}}) {
	if ($altnum=~/^[0-9]+$/) {
	    $duplications{$currtempl}{$altnum}{"Default"}{"Default"}=1;
	    $macrosL{$currtempl}{"Default"}{$altnum}="";
	    $macrosG{$currtempl}{"Default"}{$altnum}="";
	    $cat{$currtempl}{"Default"}{$altnum}="cat_unknown";
	    $scat{$currtempl}{"Default"}{$altnum}="<>";
	    $haspred{$currtempl}{"Default"}{$altnum}="Lemma"; # 1 si pred=lemme, 0 si pas de pred, autre chose si le pred est autre chose
	    $w{$currtempl}{"Default"}{$altnum}="";
	    if (defined($inheritsfrom{$currtempl}{"weight"})) {
		$w{$currtempl}{"Default"}{$altnum}=$inheritsfrom{$currtempl}{"weight"};
	    } elsif (defined($inheritsfrom{$currtempl}{$altnum}{"weight"})) {
		$w{$currtempl}{"Default"}{$altnum}=$inheritsfrom{$currtempl}{$altnum}{"weight"};
	    }
	    for my $i (0..$#prop) {
	      for my $msprop (keys %{$scat{$currtempl}}) {
		if (defined($prop[$i]) && $prop[$i] ne "" && (defined($inheritsfrom{$currtempl}{$prop[$i]})
							      || defined($inheritsfrom{$currtempl}{$altnum}{$prop[$i]})
							      || $prop[$i] eq $msprop)) {
		    if (defined($scat{$currtempl}{$msprop}{$altnum})) {
		      &apply_prop($i,$currtempl,$msprop,$altnum,0);
		    }
		  }
		}
	    }
	    for my $msprop (keys %{$scat{$currtempl}}) {
	      if (defined($scat{$currtempl}{$msprop}{$altnum})) {
		$scat{$currtempl}{$msprop}{$altnum}=~s/<>//;
	      }
	      $macrosL{$currtempl}{$msprop}{$altnum}=~s/(^|,),+/$1/g; $macrosL{$currtempl}{$msprop}{$altnum}=~s/,+$//g;
	      $macrosG{$currtempl}{$msprop}{$altnum}=~s/(^|,),+/$1/g; $macrosG{$currtempl}{$msprop}{$altnum}=~s/,+$//g;
	      debugprint("# \@$currtempl\t$msprop\t".($w{$currtempl}{"Default"}{$altnum} || 100)."\t".($haspred{$currtempl}{"Default"}{$altnum})."\t".($cat{$currtempl}{"Default"}{$altnum})."\t".($scat{$currtempl}{"Default"}{$altnum})."\t".($macrosL{$currtempl}{"Default"}{$altnum})."\t".($macrosG{$currtempl}{"Default"}{$altnum})."\n");
	    }
	}
    }
}
print STDERR "done\n"; $state=10;




print STDERR "  Generating .lex file...";
$linenumber=0;
my $msprop; my $functor; my $pred; my $tmp; my $origmorpho;
while (<>) {
    chomp;
    $linenumber++;
    if (/^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t(((?:\@\@[^\@]+)*)\@([^ \t\@\;]*))(?:\t.*)?$/ && $state==10) {
	$form=$1;
	$lemma=$2;
	if ($linenumber % 1000 == 0) {
	    print STDERR "\r  Generating .lex file...Line $linenumber".(" " x 20);
	}
	$msprop=$3;
	if ($msprop eq "") {$msprop="Default";}
	$morpho=$4;
	$fulltemplate=$5;
	$functorslist=$6;
	$template=$7;
	$fulltemplate=~s/^\@\@//;
	if ($template=~/^([^\(\)]*)(?:\((.*)\))?$/) {
	  $template=$1;
	  @templatevars = ("",split(/\s*,\s*/,$2));
	} else {
	  print STDERR ("#" x 30)."\n##########  Erreur: Ligne $linenumber, lemme $lemma: le pattern syntaxique \@$fulltemplate est invalide\n";
	  next;
	}
	if (!defined($scat{$template})) {
	    die (("#" x 30)."\n##########  Erreur: Ligne $linenumber, lemme $lemma: le pattern syntaxique \@$template est non-défini\n");
	}
	if ($functorslist ne "") {
	    if (!defined($scat{$fulltemplate})) {
		$msprop="Default";
		for my $alt (keys %{$scat{$template}{$msprop}}) {
		    $scat{$fulltemplate}{$msprop}{$alt}=$scat{$template}{$msprop}{$alt};
		    $cat{$fulltemplate}{$msprop}{$alt}=$cat{$template}{$msprop}{$alt};
		    $w{$fulltemplate}{$msprop}{$alt}=$w{$template}{$msprop}{$alt};
		    $macrosG{$fulltemplate}{$msprop}{$alt}=$macrosG{$template}{$msprop}{$alt};
		    $macrosL{$fulltemplate}{$msprop}{$alt}=$macrosL{$template}{$msprop}{$alt};
		    $haspred{$fulltemplate}{$msprop}{$alt}=$haspred{$template}{$msprop}{$alt};
		    for (keys %{$duplications{$template}{$alt}{$msprop}}) {
			$duplications{$fulltemplate}{$alt}{$msprop}{$_}=1;
		    }
		}
		while ($functorslist=~s/^(.*)\@\@([^\@]+)$/$1/) {
		    $functor=$2;
		    for my $i (0..$#prop) {
		      for my $msprop (keys %{$scat{$fulltemplate}}) {
			for my $alt (keys %{$scat{$fulltemplate}{$msprop}}) {
			  if (defined($prop[$i]) && $prop[$i] ne "" && (defined($finheritsfrom{$functor}{$prop[$i]})
									|| defined($finheritsfrom{$functor}{$alt}{$prop[$i]})
									|| $prop[$i] eq $msprop)) {
			    if (defined($scat{$fulltemplate}{$msprop}{$alt})) {
			      &apply_prop($i,$fulltemplate,$msprop,$alt,0);
			    }
			  }
			}
		      }
		    }
		}
	    }
	    $template=$fulltemplate;
	}
	$origmorpho=$morpho;
	if ($morpho ne "" && $morpho!~/^\@/) {$morpho="\@".$morpho;}
	$morpho=~s/-//; # provisoire - pour passer d'un systeme de tags a l'autre
	for my $altnum (keys %{$duplications{$template}}) {
	  &close_duplications($template,$altnum,$msprop);
	  for my $subpart (keys %{$duplications{$template}{$altnum}{$msprop}}) {
	    @answerarray=();
	    print "$form\t$w{$template}{$subpart}{$altnum}\t";
	    print "$cat{$template}{$subpart}{$altnum}\t[";
	    if ($haspred{$template}{$subpart}{$altnum} ne "0") {
	      if ($haspred{$template}{$subpart}{$altnum}=~/Lemma/) {
		$pred=$haspred{$template}{$subpart}{$altnum};
		$pred=~s/Lemma/$lemma/;
		push(@answerarray, "pred=\'$pred$scat{$template}{$subpart}{$altnum}\'");
	      } else {
		push(@answerarray, "pred=\'$haspred{$template}{$subpart}{$altnum}$scat{$template}{$subpart}{$altnum}\'");
	      }
	    }
	    if ($macrosL{$template}{$subpart}{$altnum} ne "") {
	      $tmp=$macrosL{$template}{$subpart}{$altnum};
	      $tmp=~s/\$([0-9])/$templatevars[$1]/g;
	      push(@answerarray, $tmp);
	    }
	    if ($macrosG{$template}{$subpart}{$altnum} ne "") {
	      $tmp=$macrosG{$template}{$subpart}{$altnum};
	      $tmp=~s/\$([0-9])/$templatevars[$1]/g;
	      push(@answerarray, $tmp);
	    }
	    if ($morpho ne "") {
	      push(@answerarray, $morpho);
	    }
	    print join(",",@answerarray);
	    print "]\t$lemma\t$subpart\t$origmorpho\n";
	  }
	}
    } elsif (/^$/) {
    } else {
	die (("#" x 30)."\n##########  Erreur: Syntax error in input file, line $linenumber (parser state $state)\nLine:$_\n");
    }
}

print STDERR "\r  Generating .lex file...done                                     \n";

close(LOG);




################## FUNCTIONS #################


sub debugprint {
    my $l=shift;
    print LOG $l;
}

sub apply_prop {
  my $prop=shift;
  my $templ=shift; # template name
  my $msprop=shift; # morphosyntactic property (Default, Infinitive, Imperative, PastParticiple)
  my $alt=shift; # alternative number inside templates that have disjunctions
  my $entry=shift; # entry number for the same alternative and the same morphological form (for ex.: past particple as active or passive participle)
  apply_prop2($prop,$templ,$msprop,$alt,$entry,$base[$prop],$target[$prop],$must_work[$prop],$prop[$prop]);
}

sub apply_prop2 {
  my $prop=shift;
  my $templ=shift; # template name
  my $msprop=shift; # morphosyntactic property (Default, Infinitive, Imperative, PastParticiple)
  my $alt=shift; # alternative number inside templates that have disjunctions
  my $entry=shift; # entry number for the same alternative and the same morphological form
  my $base=shift;
  my $target=shift;
  my $must_work=shift;
  my $propname=shift;
  if ($target =~ /^\+?=/) {
    if ($target eq "=".$msprop || $target eq "+=".$msprop) {
#      print STDERR "$templ: $msprop > $base\n";
      $scat{$templ}{$base}{$alt}=$scat{$templ}{$msprop}{$alt};
      $cat{$templ}{$base}{$alt}=$cat{$templ}{$msprop}{$alt};
      $w{$templ}{$base}{$alt}=$w{$templ}{$msprop}{$alt};
      $macrosG{$templ}{$base}{$alt}=$macrosG{$templ}{$msprop}{$alt};
      $macrosL{$templ}{$base}{$alt}=$macrosL{$templ}{$msprop}{$alt};
      $haspred{$templ}{$base}{$alt}=$haspred{$templ}{$msprop}{$alt};
      $duplications{$templ}{$alt}{$base}{$base}=1;
      if ($target eq "+=".$msprop) {
	$duplications{$templ}{$alt}{$msprop}{$base}=1;
#	print STDERR "adding \$duplications{$templ}{$alt}{$msprop}{$base}\n";
      }
    }
  } elsif ($base =~ /^\\\&(.*)$/) { # trouver mieux?
    no strict "refs";
    $scat{$templ}{$msprop}{$alt} = ($1)->($scat{$templ}{$msprop}{$alt});
  } elsif ($base eq "p") {
      print STDERR "($prop)\t".join(" - ",($haspred{$templ}{$msprop}{$alt},$scat{$templ}{$msprop}{$alt},$macrosG{$templ}{$msprop}{$alt},$macrosL{$templ}{$msprop}{$alt}))."\t$templ\t$msprop\t$alt\n";
  } elsif ($base eq "Skip") {
      if (($msprop eq $target) || $macrosG{$templ}{$msprop}{$alt}=~/$target/ || $macrosL{$templ}{$msprop}{$alt}=~/$target/) {
	  delete($scat{$templ}{$msprop}{$alt});
	  delete($cat{$templ}{$msprop}{$alt});
	  delete($w{$templ}{$msprop}{$alt});
	  delete($macrosL{$templ}{$msprop}{$alt});
	  delete($macrosG{$templ}{$msprop}{$alt});
	  delete($haspred{$templ}{$msprop}{$alt});
	  delete($duplications{$templ}{$alt}{$msprop}{$msprop});
#	  print STDERR "deleting \$duplications{$templ}{$alt}{$msprop}{$msprop}\n";
	  if (keys %{$duplications{$templ}{$alt}{$msprop}} == 0) {
	      delete($duplications{$templ}{$alt}{$msprop});
#	      print STDERR "eliminating \$duplications{$templ}{$alt}{$msprop}\n";
	  }
      }
  } elsif ($base eq "MacrosL") {
    $tmp=$target;
    if ($inheritsfrom{$templ}{$alt}{$propname} ne "_" && $inheritsfrom{$templ}{$alt}{$propname} ne "") {
      $tmp .= "=".$inheritsfrom{$templ}{$alt}{$propname};
    } elsif ($inheritsfrom{$templ}{$propname} ne "_" && $inheritsfrom{$templ}{$propname} ne "") {
      $tmp .= "=".$inheritsfrom{$templ}{$propname};
    }
    if ($macrosL{$templ}{$msprop}{$alt} !~ /(^|[, ])$tmp([, ]|$)/) {
      if ($macrosL{$templ}{$msprop}{$alt} ne "") {$macrosL{$templ}{$msprop}{$alt}.=",";}
      $macrosL{$templ}{$msprop}{$alt}.=$tmp;
    }
  } elsif ($base eq "MacrosG") {
    if ($macrosG{$templ}{$msprop}{$alt} !~ /(^|[, ])$target([, ]|$)/) {
      if ($macrosG{$templ}{$msprop}{$alt} ne "") {$macrosG{$templ}{$msprop}{$alt}.=",";}
      $macrosG{$templ}{$msprop}{$alt} .= $target;
    }
  } elsif ($base eq "Pred") {
    if ($target eq "0") {
	$haspred{$templ}{$msprop}{$alt}=0;
    } elsif ($target eq "pred") {
	if ($inheritsfrom{$templ}{$alt}{$propname} ne "_" && $inheritsfrom{$templ}{$alt}{$propname} ne "") {
	    $haspred{$templ}{$msprop}{$alt}=$inheritsfrom{$templ}{$alt}{$propname};
	} elsif ($inheritsfrom{$templ}{$propname} ne "_" && $inheritsfrom{$templ}{$propname} ne "") {
	    $haspred{$templ}{$msprop}{$alt}=$inheritsfrom{$templ}{$propname};
	}
    } else {
	if ($inheritsfrom{$templ}{$alt}{$propname} ne "") {
	    $haspred{$templ}{$msprop}{$alt}=$target;
	    if ($inheritsfrom{$templ}{$alt}{$propname} ne "_" && $inheritsfrom{$templ}{$alt}{$propname} ne "") {
		$haspred{$templ}{$msprop}{$alt}=~s/$propname/$inheritsfrom{$templ}{$alt}{$propname}/;
		$haspred{$templ}{$msprop}{$alt}=~s/\|[^_]+//;
	    }
	} elsif ($inheritsfrom{$templ}{$propname} ne "") {
	    $haspred{$templ}{$msprop}{$alt}=$target;
	    if ($inheritsfrom{$templ}{$propname} ne "_" && $inheritsfrom{$templ}{$propname} ne "") {
		$haspred{$templ}{$msprop}{$alt}=~s/$propname/$inheritsfrom{$templ}{$propname}/;
		$haspred{$templ}{$msprop}{$alt}=~s/\|[^_]+//;
	    }
	}
    }
  } elsif ($base eq "Cat") {
    $cat{$templ}{$msprop}{$alt}=$target;
  } elsif ($base eq "0") {
    if ($target eq "0") {
	$scat{$templ}{$msprop}{$alt}=~s/>.*$/>/;
    } elsif ($target =~ s/^-//) {
	$scat{$templ}{$msprop}{$alt}=~s/>(\(?[^\),>\/]+\|)?$target/">".$1/e;
	$scat{$templ}{$msprop}{$alt}=~s/,,/,/;
    } else {
        $scat{$templ}{$msprop}{$alt}=~s/>$/">".$target/e ||
	    $scat{$templ}{$msprop}{$alt}=~s/$/",".$target/e;
    }
  } elsif (defined($syntfunc{$base})) {
    $base=$base.":";
    if ($target eq "0") {
      $scat{$templ}{$msprop}{$alt}=~s/$base[^,>]+//;
      $scat{$templ}{$msprop}{$alt}=~s/([,<]),/\1/;
      $scat{$templ}{$msprop}{$alt}=~s/,>/>/;
    } elsif ($target eq "()") {
      $scat{$templ}{$msprop}{$alt}=~s/$base([^\(][^,>]*)/$base\($1\)/;
    } elsif ($target eq ")(") {
      $scat{$templ}{$msprop}{$alt}=~s/$base\(([^,>]+)\)/$base$1/;
    } elsif ($target eq "-") {
      $scat{$templ}{$msprop}{$alt}=~s/$base\(?([^,>]+)\)?//;
      $scat{$templ}{$msprop}{$alt}=~s/([,<]),/\1/;
      $scat{$templ}{$msprop}{$alt}=~s/,>/>/;
    } elsif ($target =~ s/^-//) {
      $scat{$templ}{$msprop}{$alt}=~s/$base(\(?[^\),>\/]+\|)?$target(?=[\|\),>])/$base.$1/e;
      $scat{$templ}{$msprop}{$alt}=~s/\|\|/\|/;
      $scat{$templ}{$msprop}{$alt}=~s/:(\()?\|/:\1/;
      $scat{$templ}{$msprop}{$alt}=~s/\|(\))?([,>])/\1\2/;
      $scat{$templ}{$msprop}{$alt}=~s/$base([,>])/\1/;
      $scat{$templ}{$msprop}{$alt}=~s/([,<]),/\1/;
      $scat{$templ}{$msprop}{$alt}=~s/,>/>/;
    } elsif ($target =~s/^<//) {
      $target=~s/^([^\[]+)(?:\[(.*)\])?$/$1/;
      my $ops=$2; my %ops=();
      for (split(/\s*,\s*/,$ops)) {
	/^([^\s]+)\s*>\s*([^\s]*)/ || die "error with op $_\n";
	$ops{$1}=$2;
      }
      $target=$target.":";
      if ($target ne $base) {
	$scat{$templ}{$msprop}{$alt}=~s/$base[^,>]*//;
      }
      if ($scat{$templ}{$msprop}{$alt}=~/$target([^,>]*)/) {
	my $temp=$1;
	for (keys %ops) {
	  $temp=~s/(^|\||\()$_(\||\)|$)/$1$ops{$_}$2/;
	}
	$scat{$templ}{$msprop}{$alt}=~s/$target([^,>]*)/$base$temp/;
      }
      $scat{$templ}{$msprop}{$alt}=~s/\|\|/\|/;
      $scat{$templ}{$msprop}{$alt}=~s/:(\()?\|/:\1/;
      $scat{$templ}{$msprop}{$alt}=~s/\|(\))?([,>])/\1\2/;
      $scat{$templ}{$msprop}{$alt}=~s/$base([,>])/\1/;
      $scat{$templ}{$msprop}{$alt}=~s/([,<]),/\1/;
      $scat{$templ}{$msprop}{$alt}=~s/,>/>/;
    } else {
      if ($scat{$templ}{$msprop}{$alt}=~/$base/) {
	$target=~s/^\(([^\(\)]*)\)$/$1/;
	$scat{$templ}{$msprop}{$alt}=~s/$base(\(?[^\),>\/]+)/$base.$1."|".$target/e;
      } else {
	$scat{$templ}{$msprop}{$alt}=~s/<>$/"<".$base.$target.">"/e ||
	  $scat{$templ}{$msprop}{$alt}=~s/>$/",".$base.$target.">"/e;
      }
    }
  } elsif ($base=~/(^\\\@|=)/) {
    $base=~s/\\\.\\([\*\+])/[^, \\]]\1/g;
    if ($must_work && !($macrosG{$templ}{$msprop}{$alt}=~/(^|, *)$base(,|$)/) && !($macrosL{$templ}{$msprop}{$alt}=~/(^|, *)$base(,|$)/)) {
      print STDERR "##2 WARNING: le template $templ, alternative $alt est déclaré à $propname mais cela semble impossible à prendre en compte ($msprop: /".($scat{$templ}{$msprop}{$alt})." / ".$macrosG{$templ}{$msprop}{$alt}." / ".$macrosL{$templ}{$msprop}{$alt}."/ - /$base/ - /$target/).\n";
    } else {
      if ($macrosL{$templ}{$msprop}{$alt}=~s/(^|, *)$base(,|$)/\1\2/) {
	if ($macrosL{$templ}{$msprop}{$alt} !~ /(^|[, ])$target([, ]|$)/) {
	  if ($macrosL{$templ}{$msprop}{$alt} ne "") {$macrosL{$templ}{$msprop}{$alt}.=","}
	  $macrosL{$templ}{$msprop}{$alt}.=$target;
	}
	$macrosL{$templ}{$msprop}{$alt}=~s/,,+/,/;
	$macrosL{$templ}{$msprop}{$alt}=~s/^,+//;
	$macrosL{$templ}{$msprop}{$alt}=~s/,+$//;
      }
      if ($macrosG{$templ}{$msprop}{$alt}=~s/(^|, *)$base(,|$)/\1\2/) {
	if ($macrosG{$templ}{$msprop}{$alt} !~ /(^|[, ])$target([, ]|$)/) {
	  if ($macrosG{$templ}{$msprop}{$alt} ne "") {$macrosG{$templ}{$msprop}{$alt}.=","}
	  $macrosG{$templ}{$msprop}{$alt}.=$target;
	}
	$macrosG{$templ}{$msprop}{$alt}=~s/,,+/,/;
	$macrosG{$templ}{$msprop}{$alt}=~s/^,+//;
	$macrosG{$templ}{$msprop}{$alt}=~s/,+$//;
      }
    }
  } else {
    if (!($scat{$templ}{$msprop}{$alt}=~s/$base/$target/e)) {
      if ($must_work) {
	print STDERR "##2 WARNING: le template $templ, alternative $alt est déclaré à $propname mais cela semble impossible à prendre en compte ($msprop: /".($scat{$templ}{$msprop}{$alt})." / ".$macrosG{$templ}{$msprop}{$alt}." / ".$macrosL{$templ}{$msprop}{$alt}."/ - /$base/ - /$target/).\n";
      }
    }
  }
}

# sub obj2subj {
#   my $souscat=shift;
#   if ($souscat=~/^<([^\,]*)\,obj:\(?([^\(\,\)>]*)\)?(.*)?$/) {
#     $obj=$2;
#     $suite=$3;
#     $obj=~s/obj/subj/;
#     $obj=~s/(de-|à-)?([sv])comp/\2subj/g;
#     $obj=~s/whcomp/ssubj/g; # SHOULD MAYBE BE whsubj (EVDLC)
#     $obj=~s/ssubj(\|ssubj)+/ssubj/g;
#     $obj=~s/vsubj(\|vsubj)+/vsubj/g;
#     $obj=~s/\bsubj(\|subj)+/subj/g;
#     $souscat="<suj:(par-obj),obj:$obj$suite";
#   }
#   return "$souscat";
# }


sub load_patterns_file () {
    my $patternsfile=shift;
    my $inheritsfrom_href=shift;
    my $templates_href=shift;
    my $mustbeloaded=shift;
    my $linenumber=0;
    if (!(open (PATT,"<$patternsfile"))) {
	if ($mustbeloaded) {
	    die ("########## Fichier de patrons introuvable ($patternsfile)\n");
	} else {
	    print STDERR "Attention: Fichier de patrons introuvable ($patternsfile)\n";
	    system("touch $patternsfile");
	}
    }
    # on remplit le graphe (hash) $inheritsfrom_href le graphe d'héritage, avant de faire effectivement les héritages
    # La première clef est le nom de la classe syntaxique
    # La deuxième clef est une propriété atomique OU un entier. Dans ce second cas, il s'agit d'alternatives
    # Ainsi, une entrée est définie par toutes les propriétés atomiques de $inheritsfrom_href->{$template} plus toutes les propriétés
    #    qui sont les clefs de $inheritsfrom_href{$altnum}, où $altnum est un entier tel que defined($inheritsfrom_href{$altnum})
    while (<PATT>) {
	$linenumber++;
	chomp;
	s/^\#.*$//;
	s/([^\"])\#.*$/\1/;
	s/[ \t]*$//;
	s/^[ \t]*//;
	if (/^\@([^\t]*)\t*$/ && $state==0) {
	    $template=$1;
	    $state=1;
	    $templates_href->{$template}{"scat"}{$altnum}="";
	} elsif (/^< *([^0-9=][^=]*)(=(.*))?$/ && $state==1) {
	    my $noeud=$1;
	    my $val="_";
	    if ($3 ne "") {$val=$3;}
	    if (defined($inheritsfrom{$template}{$noeud})) {
		print STDERR "##0 WARNING: le template $template a plus d'une fois un héritage du noeud $noeud\n";
	    }
	    $inheritsfrom_href->{$template}{$noeud}=$val;
	} elsif (/^\{$/ && $state==1) {
	    $inheritsfrom_href->{$template}{$altnum}=();
	    $state=2;
	} elsif (/^< *([^=]*)(=(.*))?$/ && $state==2) {
	    my $val="_";
	    if ($3 ne "") {$val=$3;}
	    if (defined($inheritsfrom_href->{$template}{$altnum}{$1})) {
		print STDERR "##1 WARNING: le template $template dans son alternative $altnum a plus d'une fois un héritage du noeud $1\n";
	    }
	    $inheritsfrom_href->{$template}{$altnum}{$1}=$val;
	} elsif (/^\|$/ && $state==2) {
	    $altnum++;
	    $templates_href->{$template}{"scat"}{$altnum}="";
	    $inheritsfrom_href->{$template}{$altnum}=();
	    $state=2;
	} elsif (/^\}$/ && $state==2) {
	    $state=9;
	    $altnum=0;
	} elsif (/^\;$/ && ($state==9 || $state==1)) {
	    $state=0;
	} elsif (/^\.?$/) {
	} else {
	    die (("#" x 30)."\n##########  Erreur: Syntax error in properties file, line $linenumber (parser state $state)\nLine:$_\n");
	}
    }
    my $done=0;
    my $step=0;
    # $done vaut 1 quand il n'y a plus nulle part d'héritage de noeud. On les remplace en effet progressivement par leur contenu.
    while (!$done) {
	$step++;
	$done=1;
	for my $currtempl (keys %$templates_href) {
	    # pour chaque pattern
	    if (defined($inheritsfrom_href->{$currtempl})) { 
		# s'il hérite encore de quelque noeud
		my $wasnotflat=defined($inheritsfrom_href->{$currtempl}{"0"});
		for my $currnode (keys %{$inheritsfrom_href->{$currtempl}}) {
		    # pour chaque ligne de niveau 0 dans le pattern
		    if ($currnode=~/^\@(.*)$/) {
			# si ladite ligne est un heritage
			$done=0;
			my $father=$1;
			delete $inheritsfrom_href->{$currtempl}{$currnode};
			if (!defined($inheritsfrom_href->{$father})) {
			    print STDERR "### Warning: le template $father semble inconnu\n";
			}
			for my $inhnode (keys %{$inheritsfrom_href->{$father}}) {
#			    print STDERR "$currnode - $inhnode\n";
			    if ($inhnode=~/^[0-9]+$/) {
				if ($wasnotflat) {
				    die("########## Erreur: le template $currtempl, qui n'est pas plat, essaye d'hériter au niveau 0 du noeud $father, qui n'est pas plat (step $step)\n.");
				} else {
				    foreach my $subnode (keys %{$inheritsfrom_href->{$father}{$inhnode}}) {
					$inheritsfrom_href->{$currtempl}{$inhnode}{$subnode}=$inheritsfrom_href->{$father}{$inhnode}{$subnode};
				    }
				}
			    } else {
				$inheritsfrom_href->{$currtempl}{$inhnode}=$inheritsfrom_href->{$father}{$inhnode};
			    }
			}
		    } elsif ($currnode=~/^[0-9]+$/) {
			# si ladite "ligne" est en fait une alternative
			my $deletealt=0;
			foreach my $subnode (keys %{$inheritsfrom_href->{$currtempl}{$currnode}}) {
			    if ($subnode=~/^\@(.*)$/) {
				my $father=$1;
				$done=0;
				delete $inheritsfrom_href->{$currtempl}{$currnode}{$subnode};
				if (!defined($inheritsfrom_href->{$father})) {
				    print STDERR "### Warning: le template $father semble inconnu\n";
				}
				for my $inhnode (keys %{$inheritsfrom_href->{$father}}) {
				    # d'abord les noeuds de niveau 0 dans le noeud hérité
				    if ($inhnode!~/^[0-9]+$/) {
					$inheritsfrom_href->{$currtempl}{$currnode}{$inhnode}=$inheritsfrom_href->{$father}{$inhnode};
				    }
										 }
				for my $inhnode (keys %{$inheritsfrom_href->{$father}}) {
				    # ensuite les noeuds des alternatives dans le noeud hérité
				    if ($inhnode=~/^[0-9]+$/) {
#				if ($deletealt==0) {
					$deletealt=1;
					my $newaltnum=100+$currnode*100+$inhnode;
					for my $tempnode (keys %{$inheritsfrom_href->{$currtempl}{$currnode}}) {
					    $inheritsfrom_href->{$currtempl}{$newaltnum}{$tempnode}=$inheritsfrom_href->{$currtempl}{$currnode}{$tempnode};
					}
					for my $tempnode (keys %{$inheritsfrom_href->{$father}{$inhnode}}) {
					    $inheritsfrom_href->{$currtempl}{$newaltnum}{$tempnode}=$inheritsfrom_href->{$father}{$inhnode}{$tempnode};
					}
#				} else {
#				    die("########## Erreur: le template $currtempl a une alternative $currnode, qui n'est pas plate, mais qui essaye d'hériter du noeud $father, qui n'est pas plat (step $step)\n.");
#				}
				    }
				}
			    }
			}
			if ($deletealt) {
			    delete $inheritsfrom_href->{$currtempl}{$currnode};
			}
		    }
		}
	    }
	}
    }
    for my $currtempl (keys %$templates_href) {
	debugprint("\@$currtempl\n");
	if (defined($inheritsfrom{$currtempl})) {
	    for my $currnode (keys %{$inheritsfrom{$currtempl}}) {
		if ($currnode=~/^\@(.*)$/) {
		    die ("########## Erreur: héritage raté ($currtempl - $currnode)\n");
		} elsif ($currnode!~/^[0-9]+$/) {
		    debugprint("< $currnode\n");
		}
	    }
	    my $an=0; # utile
	    for my $currnode (keys %{$inheritsfrom{$currtempl}}) {
		if ($currnode=~/^[0-9]+$/ && defined($inheritsfrom{$currtempl}) && defined($inheritsfrom{$currtempl}{$currnode})) {
		    if ($an==0) {debugprint("{\n");} else {debugprint("|\n")}
		    for my $currnode2 (keys %{$inheritsfrom{$currtempl}{$currnode}}) {
			if ($currnode2=~/^\@(.*)$/) {
			    die ("########## Erreur: héritage raté ($currtempl - $an - $currnode2)\n");
			} else {
			    debugprint("  < $currnode2\n");
			}
		    }
		    $an++; # utile
		}
	    }
	    if ($an>0) {
		debugprint("}\n");
	    } else {
		$inheritsfrom{$currtempl}{0}=(); # utile
	    }
	}
	debugprint(";\n");
    }
}

sub close_duplications {
    my $templ=shift;
    my $alt=shift;
    my $msprop_base=shift;
    if (defined($duplications{$templ}{$alt}{$msprop_base})) {
	for my $msprop_der1 (keys %{$duplications{$templ}{$alt}{$msprop_base}}) {
	    if ($msprop_der1 ne $msprop_base) {
		&close_duplications($templ,$alt,$msprop_der1);
		for my $msprop_der2 (keys %{$duplications{$templ}{$alt}{$msprop_der1}}) {
		    $duplications{$templ}{$alt}{$msprop_base}{$msprop_der2}=1;
		}
	    }
	}
    }
}
