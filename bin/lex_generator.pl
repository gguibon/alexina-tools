#!/usr/bin/env perl

use strict;
use open ':utf8';
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";


while (<>) {
  chomp;
  if (/^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t;]*);([^\t;]*);([^\t;]*);([^\t;]*);([^\t;]*)\t([^\t]*)\t([^\t]*)/) {
    my $ff = $1; my $lemma = $2; my $msprop = $3; my $tag = $4; my $w = $5; my $pred = $6; my $cat = $7; my $scat = $8; my $macros = $9; my $constr = $10; my $inflclass = $11;
    print "$1\t$w\t$cat\t[";
    if ($pred ne "0") {
      $pred =~ s/Lemma/$lemma/;
      $pred =~ s/"/\\"/g;
      $scat =~ s/"/\\"/g;
      print "pred=\"$pred$scat\"";
      if ($macros ne "") {
	print ",";
      }
    }
    print $macros;
    if ($tag ne "") {
      print ",";
      print "\@$tag";
    }
    print "]\t$lemma\t$msprop\t$tag\t$constr\t$inflclass\n";
  } else {
    print STDERR "Wrong format: $_ (skipping line)\n";
  }
}
