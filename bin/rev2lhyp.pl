#!/usr/bin/env perl

use utf8;
use open ':utf8';

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use strict;

my ($guess,$all,$cov,$filter,$usereq);
my $filterth=1;
my $queriesfilename="queries";
my $unknownsfilename="unknowns";
my $dersfilename;
my $morphobase;
my $lang = "fr";
while (1) {
    $_=shift;
    if (/^$/) {last;}
    elsif (/^-g$/ || /^-guess$/i) {$guess=1;}
    elsif (/^-a$/ || /^-all$/i) {$all=1;}
    elsif (/^-c$/ || /^-cov(?:erage)?$/i) {$cov=1;}
    elsif (/^-f$/ || /^-filter$/i) {$filter=1;}
    elsif (/^-r$/ || /^-req$/i) {$usereq=1;}
    elsif (/^-f=([0-9]+)$/ || /^-filter=([0-9]+)$/i) {$filter=1; $filterth=$1;}
    elsif (/^-q=(.*)$/ || /^-queries?(?:file)?=(.*)$/i) {$queriesfilename=$1;}
    elsif (/^-u=(.*)$/ || /^-unknowns?(?:file)?=(.*)$/i) {$unknownsfilename=$1;}
    elsif (/^-d=(.*)$/ || /^-ders?(?:file)?=(.*)$/i) {$dersfilename=$1;}
    elsif (/^-l=(.*)$/ || /^-lang=(.*)$/i) {$lang=$1;}
    elsif (/^[^-]/) {$morphobase=$_;}
}

print STDERR "morphobase = $morphobase\n";
require("$morphobase.check.pl");

print STDERR "  Loading data and input files...\r";
open(DATA,"<$morphobase.data") || die "\nCould not open $morphobase.data: $!";
my (%exclvars, %tabletagvars, %req);
while (<DATA>) {
    chomp;
    if (/^\#\#EXCL\#\#\t([^\t]*)\t([^\t]*)\t([^\t]*)$/) {
      $exclvars{$1}{$2}{$3}=1;
    } elsif (/^\#\#REQ\#\#\t([^\t]*)\t([^\t]*)\t([^\t]*)$/) {
      $req{$1}{$2}{$3}=1;
    } elsif (/^([^\t]*)\t([^\t]*)\t([^\t]*)$/) {
      $tabletagvars{$1}{$2}{$3}=1;
    }
}
close(DATA);

open(UW,">$unknownsfilename") || die "\nCould not open unknowns: $!";
my (%seen_forms,%rejected,%nonexistant);
my ($l, $t, $f, $tag, $v, $currv, $currv2, $currv3);
my $nl;
while (<>) {
    chomp;
    if ($nl++ % 10000 == 0) {print STDERR "  Loading data and input files...".($nl-1)."\r"}
    /^([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t:]*)([^\t]*)(\t.*)?$/ || die "\nProblem in data file";
    $f=$1; $l=$2; $tag=$3; $t=$4; $v=$5;
    if ($6 eq "\tUNKNOWN") {
      s/^#//;
      s/\tUNKNOWN$//;
      print UW "$_\n";
    }
    $seen_forms{$l}{$t}{$tag}{$v}{__ALL__}++;
    if ($cov || $filter) {
      $seen_forms{$l}{$t}{$tag}{$v}{$f}=1;
    }
}
close(UW);

my %ders;
if ($dersfilename ne "") {
#  print STDERR "opening $dersfilename\n";
  open(DER,"<$dersfilename") || die "\nCould not open $dersfilename: $!";
  while (<DER>) {
    chomp;
    next if (/<error/);
    unless (/^[^\t]*\t[^\t]*\t([^\t]*)\t([^\t:]*)([^\t]*)$/) {
      print STDERR "\nProblem in pder file: $_"; 
      next;
    }
    $l=$1; $t=$2; $v=$3;
    $l=~s/_//;
    $ders{$l}{$t}{$v}=1;
  }
  close(DER);
}

print STDERR "Loading data and input files... done        \n";


print STDERR "  Computing possible tables for each lemma...\r";
if (!$guess && !$all) {
  open(QUER,">$queriesfilename") || die "\nCould not open queries: $!";
}

my $nlemmas=scalar keys %seen_forms;

my %tables; my %forms;
my %is_consistent_cache;
my ($w,$cat);
my $delete;
my (%temp, $tempnum, $tempv, %printed_queries);
my $int=10;
$nl=0;
if ($filter>1) {$int=100}
for $l (keys %seen_forms) {
  if ($nl++ % $int == 0) {print STDERR "  Computing possible tables for each lemma...".($nl-1)."/$nlemmas\r"}
  for $t (keys %{$seen_forms{$l}}) {
#    print STDERR "\n";
    $cat=$t;
    $cat=~s/-.*$//;
    # Building the initial hash of forms  ($forms{lemma}{table}{curr_variantslist=""}{form})
    # and the hash of tables : $tables{lemma}{table}{curr_variantslist=""}
    if (!defined $tables{$l}{$t}) {
      $tables{$l}{$t}{""}=1;
      if ($cov || $filter) {
	for $tag (keys %{$seen_forms{$l}{$t}}) {
	  if (defined $seen_forms{$l}{$t}{$tag}{""}) {
	    for $f (keys %{$seen_forms{$l}{$t}{$tag}{""}}) {
	      next if ($f eq "__ALL__");
	      $forms{$l}{$t}{""}{$f}=1;
	    }
	  }
	}
      }
    }
    for $tag (keys %{$seen_forms{$l}{$t}}) {           # for each tag attested for lemma-table,
#      print STDERR "$tag:\n";
      for $v (keys %{$seen_forms{$l}{$t}{$tag}}) {     # and for each hypothetical variant for this tag,
	if ($v ne "") {                              
	  my $added=0;
	  %temp=%{$tables{$l}{$t}};
	  for $currv (keys %{$tables{$l}{$t}}) {         # for each variantslist currently supposed for lemma-table,
	    $currv2=&sortvlist($currv.$v);
	    if (&is_consistent($currv2,$t)) {          # we try to see if the variant is compatible with this variantslist
	      $tables{$l}{$t}{$currv2}=1;
	      $added=1;
	      if ($cov || $filter) {
		for $f (keys %{$forms{$l}{$t}{$currv}}) {
		  next if ($f eq "__ALL__");
		  $forms{$l}{$t}{$currv2}{$f}=1;
		}
		for $f (keys %{$seen_forms{$l}{$t}{$tag}{$v}}) {
		  $forms{$l}{$t}{$currv2}{$f}=1;
		}
	      }
	      if ($currv ne $currv2) {
		delete($tables{$l}{$t}{$currv});
	      }
	    }
	  }
	  if (!$added) {
	    for $currv (keys %temp) {
#	      print STDERR "[$currv]";
	      $currv2=$v;
	      for $tempv (split(/(?=:)/,$currv)) {
		$currv3=&sortvlist($currv2.$tempv);
		if (&is_consistent($currv3,$t)) {
		  $currv2=$currv3;
		}
	      }
	      if ($currv2 ne $v) {
		$added=1;
		$tables{$l}{$t}{$currv2}=1;
		if ($cov || $filter) {
		  for $f (keys %{$forms{$l}{$t}{$currv}}) {
		    next if ($f eq "__ALL__");
		    $forms{$l}{$t}{$currv2}{$f}=1;
		  }
		  for $f (keys %{$seen_forms{$l}{$t}{$tag}{$v}}) {
		    $forms{$l}{$t}{$currv2}{$f}=1;
		  }
		}
#		print "\tAdded $t$currv2\n";
	      }
	    }
	  }
	  if (!$added) {
	    $tables{$l}{$t}{$v}=1;
	    if ($cov || $filter) {
	      for $f (keys %{$seen_forms{$l}{$t}{$tag}{$v}}) {
		$forms{$l}{$t}{$v}{$f}=1;
	      }
	    }
#	    print "\tAdded in last resort $t$currv2\n";
	  }
	}
      }
#      for $t (keys %{$tables{$l}}) {
#	for (keys %{$tables{$l}{$t}}) {
#	  print STDERR "\n$l\t$t$_\t".(scalar keys %{$forms{$l}{$t}{$_}})."\n";
#	}
#      }
#      print STDERR "-------\n";
    }
    if ($filter) {
      for $v (keys %{$tables{$l}{$t}}) {
#	print STDERR ">$l\t$t\t$v\n";
	if ((scalar keys %{$forms{$l}{$t}{$v}})<=$filterth) {
	  $delete=1;
	  if (defined($ders{$l}{$t})) {
	    for $currv (keys %{$ders{$l}{$t}}) {
	      if (&is_consistent(&sortvlist($v.$currv))) {
		$delete=0; last;
	      }
	    }
	  }
	  if ($delete) {
	    delete($tables{$l}{$t}{$v});	  
	    delete($forms{$l}{$t}{$v});
	    $rejected{$l}{$t}{$v}=1;
#	    print STDERR "\$rejected{$l}{$t}{$v}=1; (fil)\n";
	  }
	}
      }
    }
    if ($usereq) {
      for $tag (keys %{$tabletagvars{$t}}) {
	for $v (keys %{$tabletagvars{$t}{$tag}}) {
	  if ($req{$t}{$tag}{$v} && (!defined($seen_forms{$l}{$t}{$tag}) || !defined($seen_forms{$l}{$t}{$tag}{$v}))) {
	    $rejected{$l}{$t}{":".$v}=1;
#	    print STDERR "\$rejected{$l}{$t}{:$v}=1; (req)\n";
	  }
	}
      }
    }
    if (scalar keys %{$tables{$l}{$t}} == 0) {
#      print STDERR "$l\t$t abandoned\n";
      next;
    }
    for $tag (keys %{$tabletagvars{$t}}) {
      if ($tag!~/^D\[_/ && !defined($seen_forms{$l}{$t}{$tag})) {
	if ($guess || $all) {
	  for $currv (keys %{$tables{$l}{$t}}) {
	    for $v (keys %{$tabletagvars{$t}{$tag}}) {
	      if ($v ne "") {
		$v=":".$v;
		for $w (&lemmatize($l,$cat,$t,$v,0,1)) {
		if ($w!~/<error/ && &check_lemma($w)) {
#		  print STDERR "$l\t$cat\t$t\t$v\n";
		  $currv2=&sortvlist($currv.$v);
		  if (&is_consistent($currv2,$t)) {
		    $delete=0;
		    for $currv3 (keys %{$rejected{$l}{$t}}) {
		      if (&sortvlist($currv2.$currv3) eq $currv2) {
			$delete=1;
			last;
		      }
		    }
		    if (!$delete) {
		      $tables{$l}{$t}{$currv2}=1;
		      if ($cov || $filter) {
			for $f (keys %{$forms{$l}{$t}{$currv}}) {
			  $forms{$l}{$t}{$currv2}{$f}=1;
			}
		      }
		      if ($currv ne $currv2) {
			delete($tables{$l}{$t}{$currv});
		      }
		    }
		  }
		  }
		}
	      }
	    }
	  }
	} else {
	  %temp=();
	  $tempnum=0;
	  $tempv="";
	  for $v (keys %{$tabletagvars{$t}{$tag}}) {
	    if ($v ne "") {
	      $v=":".$v;
	      for $w (&lemmatize($l,$cat,$t,$v,0,1)) {
		if ($w!~/<error/ && &check_lemma($w)) {
		  my $keepit=0;
		  for $currv (keys %{$tables{$l}{$t}}) {
		    $currv2=&sortvlist($currv.$v);
		    if (&is_consistent($currv2,$t)) {
		      $keepit=1; last;
		    }
		  }
		  if ($keepit) {
		    $tempnum++;
		    $tempv=$v;
		    $temp{"$l\t$t$v"}=1;
		  }
		}
	      }
	    }
	  }
	  if ($tempnum==1) {
	    for $currv (keys %{$tables{$l}{$t}}) {
	      $currv2=&sortvlist($currv.$tempv);
	      if (&is_consistent($currv2,$t)) {
		$delete=0;
		for $currv3 (keys %{$rejected{$l}{$t}}) {
		  if (&sortvlist($currv2.$currv3) eq $currv2) {
		    $delete=1; last;
		  }
		}
		if (!$delete) {
#		  print STDERR "$l  $t  $currv2 = 1\n";
		  $tables{$l}{$t}{$currv2}=1;
		  if ($cov || $filter) {
		    for $f (keys %{$forms{$l}{$t}{$currv}}) {
		      $forms{$l}{$t}{$currv2}{$f}=1;
		    }
		  }
		}
	      }
	      if ($currv ne $currv2) {
		delete($tables{$l}{$t}{$currv});
	      }
	    }	    
	  } elsif ($tempnum>1) {
	    for (keys %temp) {
	      if (!defined($printed_queries{"$_\t$tag"})) {
		$printed_queries{$_}=1;
		print QUER "$_\t$tag\n";
	      }
	    }
	  }
	}
      }
    }
  }
  delete($seen_forms{$l});
}
close(QUER);
print STDERR "  Computing possible tables for each lemma...done                \n";

print STDERR "  Computing and printing output...";
my $ending;
my %ending2table;
if ($guess) {
  for $l (sort keys %tables) {
    $ending=$l;
    if ($l=~/(...)$/) {$ending=$1;}
    for $t (keys %{$tables{$l}}) {
      for $v (keys %{$tables{$l}{$t}}) {
	$ending2table{$ending}{$t.$v}+=1/(scalar keys %{$tables{$l}{$t}});
      }
    }
  }
}

my ($tempbest,$tempbestt);
for $l (sort keys %tables) {
  $ending=$l;
  if ($l=~/(...)$/) {
    $ending=$1;
  }
  $tempbest=-1; $tempbestt="";
  for $t (keys %{$tables{$l}}) {
    $cat=$t;
    $cat=~s/-.*$//;
    for $v (keys %{$tables{$l}{$t}}) {
      #      if (!$filter || (scalar keys %{$forms{$l}{$t}{$v}})>$filterth) {
      if ($guess) {
	if ($ending2table{$ending}{$t.$v}>$tempbest) {
	  for $w (&lemmatize($l,$cat,$t,$v,0,1)) {
	    if ($w!~/<error/ && &check_lemma($w)) {
	      $tempbest=$ending2table{$ending}{$t.$v};
	      $tempbestt=$t.$v;
	    }
	  }
	}
      } else {
	for $w (&lemmatize($l,$cat,$t,$v,0,1)) {
	  if ($w!~/<error/ && &check_lemma($w)) {
	    print "$l\t$t$v\t@?";
	    if ($cov) {
	      print "\t".(scalar keys %{$forms{$l}{$t}{$v}})."\t{".join(",",keys %{$forms{$l}{$t}{$v}})."}";
	    }
	    print "\n";
	  }
	}
      }
      #      }
    }
  }
  if ($guess) {
    print "$l\t$tempbestt\t@?";
    if ($cov) {
      $tempbest=/^([^:]*)(:.*)/;
      $t=$1; $v=$2;
      print "\t".(scalar keys %{$forms{$l}{$t}{$v}})."\t{".join(",",keys %{$forms{$l}{$t}{$v}})."}";
    }
    print "\n";
  }
}
print STDERR "done\n";

sub is_consistent {
  my $vlist=shift;
  my $table=shift;
#  print STDERR "is_consistent($vlist,$table)?";
  if (defined($is_consistent_cache{$table}{$vlist})) {
#    print STDERR " \n";
    return $is_consistent_cache{$table}{$vlist}
  } 
  for my $v1 (split(":",$vlist)) {
      next if ($v1 eq "");
      for my $v2 (split(":",$vlist)) {
	  next if ($v2 eq "");
	  next if ($v2 eq $v1);
	  if (defined($exclvars{$table}{$v1}{$v2}) || defined($exclvars{$table}{$v2}{$v1})) {
	      $is_consistent_cache{$table}{$vlist}=0;
#	      print STDERR " no\n";
	      return 0;
	  }
      }
  }
  $is_consistent_cache{$table}{$vlist}=1;
#  print STDERR " yes\n";
  return 1;
}

