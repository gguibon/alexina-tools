#!/usr/bin/env perl

use strict;

my $wdfilename=shift;

open(WDFILE,"< $wdfilename") || die ("$wdfilename couldn't be opened: $!\n");

my %seenforms;
while (<WDFILE>) {
    chomp;
    $seenforms{$_}=1;
}

my %seenlemmas;
my %entries;
my $entry; my $form; my $data; my $lemma;
my $ok;
my $l;

$seenlemmas{__OTHERS__}=1;
while (<>) {
    if ($l++ % 1000 == 0) {print STDERR ($l-1)."\r"}
    $entry=$_;
    chomp;
    if (/^([^\t]+)\t[^\t]*\t[^\t]+\t(.*)$/) {
	$form=$1;
	$data=$2;
	$lemma=$data;
	($lemma=~s/ *\[ *pred *= *\'([^\[]*)\'((?: *,[^>]*)? *\])[^\]]*$/\1/g) || ($lemma="");
	if ($lemma eq "") {
	    $entries{__OTHER__}.=$entry;
	} else {
	    $lemma=~s/(?:Se)?(?:<[^>]+>.*)?$//;
	    $lemma=~s/___.*$//;
	    $entries{$lemma}.=$entry;
	    $ok=1;
	    for (split(/ /,$form)) {
		if (!defined($seenforms{$_})) {$ok=0}
	    }
	    if ($ok) {$seenlemmas{$lemma}=1;}
	}
    }
}

for (keys %seenlemmas) {
    print $entries{$_};
}
