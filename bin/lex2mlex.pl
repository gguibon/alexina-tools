#!/usr/bin/env perl
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;
use locale;
use DBI;
use Encode;

$encoding = shift || "l1";
$lang = shift || "fr";

$encoding = lc($encoding);
$lang = lc($lang);

$encoding =~ s/^l(\d+)$/encoding(iso-8859-$1)/
  || $encoding =~ s/^([^uU].*)$/encoding($1)/;
$encoding =~ s/^utf-8$/utf8/;
$encoding =~ s/^u8$/utf8/;
$encoding =~ s/^/:/;
print STDERR "  Encoding: $encoding\n";
binmode STDIN, $encoding;

my $l = 0;
print STDERR "  Loading data...";
my %already_inserted;
while (<>) {
  $l++;
  if ($l % 10000 == 0) {
    print STDERR "\r  Loading data...$l";
  }
  chomp;
  next if /^\s*$/;
  /^([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)\t[^\t]*\t([^\t]*)/ || die "<$_>";
  my $wordform = $1;
  my $cat = $2;
  my $lemma = $3;
  my $ms = $4;
  $lemma =~ s/__.*$//;
  if ($lang =~ /fr$/) {
    next if ($cat =~ /^cf/);
    $cat="adv" if ($cat eq "advm" or $cat eq "advp");
  }
  $cat = $cat;
  if (!defined($already_inserted{$cat}{$ms}{$lemma}{$wordform})) {
    print "$wordform\t$cat\t$lemma\t$ms\n";
    $already_inserted{$cat}{$ms}{$lemma}{$wordform} = 1;
  }
}
print STDERR "\r  Loading data...$l\n";
print STDERR "done\n";
