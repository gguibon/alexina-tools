#!/usr/bin/env perl

use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";
#use strict;
use DBI;
use Encode;

my $db = shift || "db.dat";

my $dbh = DBI->connect("dbi:SQLite:$db", "", "", {RaiseError => 1, AutoCommit => 0});
my $sth=$dbh->prepare('select value from data where key=?');
my $l = 0;
print STDERR "  Reading data...";
while (<>) {
  $l++;
  if ($l % 10000 == 0) {
    print STDERR "\r  Reading data...$l";
  }
  chomp;
  $sth->execute($_);
  %results = ();
  while (my $value = $sth->fetchrow) {
    $results{Encode::decode("utf8",$value)} = 1;
  }
  print (join "|", sort {$a cmp $b} keys %results);
  print "\n";
}
print STDERR "\r  Reading data...$l\n";
$sth->finish;
print STDERR "done\n";
$dbh->disconnect;
