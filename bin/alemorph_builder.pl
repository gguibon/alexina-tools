#!/usr/bin/env perl

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";
use utf8;

my $linenb;

while (<>) {
  chomp;
  s/<\!--.*?-->//g;
  s/([a-z]) *= *\"/\1="/g;
  push @lines, $_;
}

print "#!/usr/bin/env perl\n\n";

print <<END;
use utf8;
binmode STDIN, ":utf8";
binmode STDERR, ":utf8";
binmode STDOUT, ":utf8";

my \$verbose=0;
my \$vverbose=0;
my \$dont_show_all=0;
my \$withlabel=0;
my \$only_features="";

while (1) {
  \$_=shift;
  if (/^\$/) {last;}
  elsif (/^-no_?sa\$/) {\$dont_show_all=1;}
  elsif (/^-no_?sep\$/) {\$no_separator=1;}
  elsif (/^-vars\$/) {\$withlabel=1;}
  elsif (/^-l(?:abels?)?\$/) {\$withlabel=1;}
  elsif (/^-v(?:erbose)?\$/) {\$verbose=1;}
  elsif (/^-V(?:erbose)?\$/) {\$vverbose=1;}
  elsif (/^-f\$/) {\$only_features=shift;}
}

END

print "my \%compatible_feature_cache;\n";
print "my \%non_compatible_attributes;\n";
print "my \%non_compatible_attributes_2_mt1;\n";
print "my \%non_compatible_attributes_2_1tm;\n";

$translitteration_function = "sub translitteration {\n  my \$s = shift;\n";
$detranslitteration_function = "sub detranslitteration {\n  my \$s = shift;\n";
for (@lines) {
  if (/<translitteration /) {
    / source="([^"]+)"/ || die;
    $source = $1;
    / target="([^"]+)"/ || die;
    $target = $1;
    $translitteration_function .= "  \$s =~ s/$source/$target/g;\n";
    $detranslitteration_function .= "  \$s =~ s/$target/$source/g;\n";
    $translitteration_hash{$source} = $target;
  } elsif (/<description /) {
    if (/ lang="(.*?)"/) {
      $lang = $1;
    }
  }
}
$translitteration_function .= "  return \$s;\n}\n";
$detranslitteration_function .= "  return \$s;\n}\n";

my (%displayed_forms,%canonical_forms, %features, %featurevalues, %is_morphomic);
my $cur_cat = "";
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<category /) {
    die "Please specify a name for the category (line ".($linenb+1).")" unless / name="(.+?)"/;
    $cur_cat = $1;
    die "category '$cur_cat' defined twice" if defined($non_compatible_attributes{$cur_cat});
  } elsif (/<\/category>/) {
    $cur_cat = "";
  } elsif (/<mfsbundle /) {
    die "mfsbundle elements must be inside a category element (line ".($linenb+1).")" if $cur_cat eq "";
    / name="([^"]+)"/ || die $_;
    $name = $1;
    die "'$name' cannot be an mfsbundle name, as it is already an mfs name (attribute value) (line ".($linenb+1).")" if defined($featurevalues{$cur_cat}{$name});
    / features="([^"]+)"/ || die $_;
    $features = expand_tagset($1);
    push @{$featurenames{$cur_cat}}, $name;
    if (/ morphomic=/) {
      / morphomic="(1|true)"/ || die $_;
      $is_morphomic{$cur_cat}{$name} = 1;
    }
    print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if ($features =~ /\|\|/);
    for $f1 (split /\s*\|\s*/, $features) {
      die "'$f1' cannot be an mfs name (attribute value), as it is already an mfsbundle name (line ".($linenb+1).")" if defined($features{$cur_cat}{$f1});
      $features{$cur_cat}{$name}{$f1} = 1;
      $featurevalues{$cur_cat}{$f1} = 1;
      for $f2 (split /\s*\|\s*/, $features) {
	next if ($f1 eq $f2);
	$non_compatible_attributes{$cur_cat}{$f1}{$f2} = 1;
	print "\$non_compatible_attributes{\"$cur_cat\"}{\"$f1\"}{\"$f2\"} = 1;\n";
      }
    }
  }
}

my %ordering_weight;
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<ordering>/) {
    $in_ordering = 1;
  } elsif (/<\/ordering>/) {
    $in_ordering = 0;
  } elsif (/<order(?:ing)_?weight /) {
    die "ordering_weight elements must be inside an ordering element (line ".($linenb+1).")" if $in_ordering eq "";
    die "an ordering_weight element must contain a non-empty 'tags' attribute (line ".($linenb+1).")" unless / tags="([^"]+)"/;
    $tags = $1;
    die "an ordering_weight element must contain a non-empty integer 'weight' attribute (line ".($linenb+1).")" unless / weight="([0-9]+)"/;
    die "weight for tag '$tags' defined twice (line ".($linenb+1).")" if defined($ordering_weight{$tags});
    $ordering_weight{$tags} = $1;
  }
}


$cur_cat = "";
my $ndisp = 0;
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<category /) {
    die "Please specify a name for the category (line ".($linenb+1).")" unless / name="(.+?)"/;
    $cur_cat = $1;
    die "category '$cur_cat' defined twice" if defined($canonical_forms{$cur_cat}) || defined($displayed_forms{$cur_cat});
    $ndisp = 0;
  } elsif (/<\/category>/) {
    $cur_cat = "";
  } elsif (/<canonical_?form /) {
    die "canonicalform elements must be inside a category element (line ".($linenb+1).")" if $cur_cat eq "";
    die "Please specify a tag for the canonical form" unless / tag="(.+?)"/;
    $tag = $1;
    die "canonicalform elements must provide a single tag, no '|' is allowed (line ".($linenb+1).")" if $tag =~ /\|/;
    for (split /\./, $tag) {
      die "canonicalform involves unknown attribute value '$_' (line ".($linenb+1).")" unless (defined($featurevalues{$cur_cat}{$_}));
    }
    if (/ tag="(.+?)"/) {
      $canonical_forms{$cur_cat}{$tag} = $1;
    }
  } elsif (/<displayed_?form /) {
    die "displayedform elements must be inside a category element (line ".($linenb+1).")" if $cur_cat eq "";
    die "Please specify a tag for the displayed form" unless / tag="(.+?)"/;
    $tag = $1;
    $lctxt = "[\$tag] ";
    $rctxt = "";
    if (/ lctxt="([^"]*)"/) {$lctxt = $1;}
    if (/ rctxt="([^"]*)"/) {$rctxt = $1;}
    $lctxt =~ s/\$tag/$tag/g;
    $rctxt =~ s/\$tag/$tag/g;
    $lctxt =~ s/\\/\\\\/g;
    $rctxt =~ s/\\/\\\\/g;
    $displayed_forms{$cur_cat}[$ndisp]{tag} = $tag;
    $displayed_forms{$cur_cat}[$ndisp]{lctxt} = $lctxt;
    $displayed_forms{$cur_cat}[$ndisp]{rctxt} = $rctxt;
    $ndisp++;
  }
}

$cur_cat = "";
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<category /) {
    die "Please specify a name for the category (line ".($linenb+1).")" unless / name="(.+?)"/;
    $cur_cat = $1;
    die "category '$cur_cat' defined twice" if defined($non_compatible_feature{$cur_cat})
      || defined($non_compatible_feature_2{$cur_cat})
      || defined($non_compatible_attributes_2_mt1{$cur_cat});
  } elsif (/<\/category>/) {
    $cur_cat = "";
  } elsif (/<mfsexclusion /) {
    die "mfsexclusion elements must be inside a category element (line ".($linenb+1).")" if $cur_cat eq "";
    / features?="([^"]+)"/ || die $_;
    $features = expand_tagset($1);
    / excludes?="([^"]+)"/ || die $_;
    $excludes = expand_tagset($1);
    print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if ($excludes =~ /\|\|/);
    print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if ($features =~ /\|\|/);
    for $exclude (split /\s*\|\s*/, $excludes) {
      for $f1 (split /\s*\|\s*/, $features) {
	if ($exclude !~ /\./ && $f1 !~ /\./) {
	  if (defined($features{$cur_cat}{$exclude})) {
	    $non_compatible_feature{$cur_cat}{$f1}{$exclude} = 1;
	    for $f2 (keys %{$features{$cur_cat}{$exclude}}) {
	      $non_compatible_attributes{$cur_cat}{$f1}{$f2} = 1;
	      $non_compatible_attributes{$cur_cat}{$f2}{$f1} = 1;
	      print "\$non_compatible_attributes{\"$cur_cat\"}{\"$f1\"}{\"$f2\"} = 1;\n";
	      print "\$non_compatible_attributes{\"$cur_cat\"}{\"$f2\"}{\"$f1\"} = 1;\n";
	    }
	  } elsif (defined($featurevalues{$cur_cat}{$exclude})) {
	    $non_compatible_attributes{$cur_cat}{$f1}{$exclude} = 1;
	    $non_compatible_attributes{$cur_cat}{$exclude}{$f1} = 1;
	    print "\$non_compatible_attributes{\"$cur_cat\"}{\"$f1\"}{\"$exclude\"} = 1;\n";
	    print "\$non_compatible_attributes{\"$cur_cat\"}{\"$exclude\"}{\"$f1\"} = 1;\n";
	  } else {
	    die "mfsexclusion involves '$exclude' which is neither a known attribute name or a known attribute value for category '$cur_cat' (line ".($linenb+1).")";
	  }
	} else {
	  die "'.' on right-hand side of mfsexclusion elements is not supported" if ($exclude =~ /\./);
	  for (split /\./, $f1) {
	    die "Unknown feature value '$_' for category '$cur_cat' (line ".($linenb+1).")" unless defined($featurevalues{$cur_cat}{$_});
	  }
	  if (defined($features{$cur_cat}{$exclude})) {
	    $non_compatible_feature_2{$cur_cat}{$f1}{$exclude} = 1;
	    for $f2 (keys %{$features{$cur_cat}{$exclude}}) {
	      $non_compatible_attributes_2_mt1{$cur_cat}{$f1}{$f2} = 1;
	      $non_compatible_attributes_2_1tm{$cur_cat}{$f2}{$f1} = 1;
	      print "\$non_compatible_attributes_2_mt1{\"$cur_cat\"}{\"$f1\"}{\"$f2\"} = 1;\n";
	      print "\$non_compatible_attributes_2_1tm{\"$cur_cat\"}{\"$f2\"}{\"$f1\"} = 1;\n";
	    }
	  } elsif (defined($featurevalues{$cur_cat}{$exclude})) {
	    $non_compatible_attributes_2_mt1{$cur_cat}{$f1}{$exclude} = 1;
	    $non_compatible_attributes_2_1tm{$cur_cat}{$exclude}{$f1} = 1;
	    print "\$non_compatible_attributes_2_mt1{\"$cur_cat\"}{\"$f1\"}{\"$exclude\"} = 1;\n";
	    print "\$non_compatible_attributes_2_1tm{\"$cur_cat\"}{\"$exclude\"}{\"$f1\"} = 1;\n";
	  } else {
	    die "mfsexclusion involves '$exclude' which is neither a known attribute name or a known attribute value for category '$cur_cat' (line ".($linenb+1).")";
	  }	  
	}
      }
    }
  } elsif (/<invalidfeatureset features="(.*?)"/) {
    die "invalidfeatureset elements must be inside a category element (line ".($linenb+1).")" if $cur_cat eq "";
    $exists = $1;
    for (split /\|/, expand_tagset($exists)) {
      $invalid_feature_set{$cur_cat}{$_} = 1;
    }
  }
}

my %tags;
for $category (keys %featurenames) {
  for $name1 (@{$featurenames{$category}}) {
    next if (defined($is_morphomic{$category}{$name1}));
    for $f1 (keys %{$features{$category}{$name1}}) {
      for $name2 (@{$featurenames{$category}}) {
	next if ($name1 eq $name2);
	next if (defined($is_morphomic{$category}{$name2}));
	for $f2 (keys %{$features{$category}{$name2}}) {
	  unless (defined($non_compatible_attributes{$category}{$f1}{$f2})) {
	    $mandatory_feature_combination{$category}{$f1}{$name2}{$f2} = 1;
	    print "\$mandatory_feature_combination{\"$category\"}{\"$f1\"}{\"$name2\"}{\"$f2\"} = 1;\n";
	  }
	}
      }
    }
  }

  $tags{$category}{""} = 1;
  for $name (@{$featurenames{$category}}) {
    next if (defined($is_morphomic{$category}{$name}));
    for $tag (keys %{$tags{$category}}) {
      for $f (keys %{$features{$category}{$name}}) {
	if ($tag eq "") {$tags{$category}{$f} = 1}
	elsif (consistent_tag($category,$tag.".$f",1) && valid_tag($category,$tag.".$f")) {
	  $tags{$category}{$tag.".$f"} = 1
	}
      }
    }
  }
  delete($tags{$category}{""});
  my $n = 1;
  my $tag_cand_nb = (scalar keys %{$tags{$category}});
  for (keys %{$tags{$category}}) {
#    print STDERR "$_\t".consistent_tag($category,$_)."\t".complete_tag($category,$_)."\t".valid_tag($category,$_)."\n";
    delete($tags{$category}{$_}) unless (consistent_tag($category,$_) && complete_tag($category,$_) && valid_tag($category,$_));
  }
  @{$std_tags{$category}} = sort {reverse($a) cmp reverse($b)} keys %{$tags{$category}};
  @{$show_tags{$category}} = ();
  for (sort {$a cmp $b} keys %{$canonical_forms{$category}}) {
    push @{$show_tags{$category}}, $_;
    push @{$show_tags_display{$category}}, "#";
  }
  for (0..$#{$displayed_forms{$category}}) {
    push @{$show_tags{$category}}, $displayed_forms{$category}[$_]{tag};
    push @{$show_tags_display{$category}}, $displayed_forms{$category}[$_]{lctxt}."#".$displayed_forms{$category}[$_]{rctxt};
  }
}


for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<letterclass /) {
    / name="([^"]+)"/ || die $_;
    $name = translitteration($1);
    / letters="([^"]*)"/ || die $_;
    $letters = translitteration($1);
    $letters =~ s/^ +//;
    $letters =~ s/ *$/ /;
    $letters =~ s/  +/ /g;
    $letters =~ s/^ $//;
    die "Incorrect letterclass ".detranslitteration($name)." ($letters)" if ($letters !~ /^([^ ] )*$/);
    $letters =~ s/ //g;
    if ($letters ne "") {
      $letters = "[".$letters."]";
      $letterclassname2re{$name} = qr/$letters/;
      $letterclassname2capturing_re{$name} = "(${letters}__OP__)";
    } else {
      $letterclassname2re{$name} = "";
      $letterclassname2capturing_re{$name} = "()";
    }
  }
}


my %mappings;
my ($l1,$l2,$cur_mapping,$cur_rev_mapping);
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<(?:pairs|mapping) /) {
    / l1="([^"]+)"/ || die $_;
    $l1 = translitteration($1);
    / l2="([^"]+)"/ || die $_;
    $l2 = translitteration($1);
    $cur_mapping = normalize_function_name($l1."____".$l2);
    $cur_rev_mapping = normalize_function_name($l2."____".$l1);
    die if defined($mappings{$cur_mapping});
  } elsif (/<\/(?:paris|mapping)/) {
    $cur_mapping = "";
  } elsif (/<pair /) {
    / l1="([^"]+)"/ || die $_;
    $l1 = translitteration($1);
    / l2="([^"]+)"/ || die $_;
    $l2 = translitteration($1);
    die if defined($mappings{$cur_mapping}{$l1});
    $mappings{$cur_mapping}{$l1} = $l2;
    $mappings{$cur_rev_mapping}{$l2} = $l1;
    print "\$mappings{\"$cur_mapping\"}{\"$l1\"} = \"$l2\";\n";
    print "\$mappings{\"$cur_rev_mapping\"}{\"$l2\"} = \"$l1\";\n";
  }
}


$mgrules_s2t_function = "sub mgrules_s2t {\n  \$s = shift; return \$s if \$s =~ /^__(?:NONE|TBD)/;\n";
$mgrules_t2s_function = "sub mgrules_t2s {\n  \$s = shift; return \$s if \$s =~ /^__(?:NONE|TBD)/;\n";
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<(?:sandhi|fusion|mgrule) /) {
    / source="([^"]*)"/ || die "Invalid sandhi: no source or empty source (line ".($linenb+1).")";
    $source = translitteration($1);
    / target="([^"]*)"/ || die "Invalid sandhi: no target or empty target (line ".($linenb+1).")";
    $target = translitteration($1);
    $source =~ s/_/ +/g;
    $target =~ s/_/ +/g;
    ($s2t_input,$s2t_output) = process_operation($linenb,$source,$target);
    $s2t_output =~ s/ \+/ /g;
    $s2t_output =~ s/^\^//;
    $s2t_output =~ s/\$$//;
    unless (/ rev="-"/) {
      $t2s_input = $target;
      while ($t2s_input =~ s/\[:([^:]+):\]([\*\+\?]?)/$letterclassname2capturing_re{$1}/) {
	my $op = $2;
	$t2s_input =~ s/__OP__/$op/;
      }
      $t2s_output = $source;
      $t2s_output =~ s/ \+/ /g;
      $t2s_output =~ s/^\^//;
      $t2s_output =~ s/\$$//;
      $j = 1;
      while ($t2s_output =~ s/\[:([^:]+):\]/\$$j/) {
	$j++;
      }
    }
    $source=~s/ \+/_/g;
    $target=~s/ \+/_/g;
    $vverbose_content = "if (\$vverbose) {print STDERR \"    Sandhi\\tRule: $source -> $target (line ".($linenb+1).")\\tOutput: \".detranslitteration(\$s).\"\\n\"}";
    if (/ final=\"\+\"/) {
      push @mgrules_s2t_final, "  if (\$s =~ s/$s2t_input/$s2t_output/g) {$vverbose_content}\n";
      unshift @mgrules_t2s_final, "  if (\$s =~ s/$t2s_input/$t2s_output/g) {}\n" unless / rev="-"/;
    } else {
      push @mgrules_s2t, "    if (\$s =~ s/$s2t_input/$s2t_output/g) {\$didsomething = 1; $vverbose_content}\n";
      unshift @mgrules_t2s, "    if (\$s =~ s/$t2s_input/$t2s_output/g)  {\$didsomething = 1}\n" unless / rev="-"/;
    }
  }
}
$mgrules_s2t_function .= "  if (\$vverbose) {print STDERR \"Applying sandhis on input \".detranslitteration(\$s).\"\\n\"}\n";
$mgrules_s2t_function .= "  \$didsomething = 1;\n";
$mgrules_s2t_function .= "  while (\$didsomething) {\n";
$mgrules_s2t_function .= "    \$didsomething = 0;\n";
for (@mgrules_s2t) {
  $mgrules_s2t_function .= $_;
}
$mgrules_s2t_function .= "  }\n";
for (@mgrules_s2t_final) {
  $mgrules_s2t_function .= $_;
}
for (@mgrules_t2s_final) {
  $mgrules_t2s_function .= $_;
}
$mgrules_t2s_function .= "  \$didsomething = 1;\n";
$mgrules_t2s_function .= "  while (\$didsomething) {\n";
$mgrules_t2s_function .= "    \$didsomething = 0;\n";
for (@mgrules_t2s) {
  $mgrules_t2s_function .= $_;
}
$mgrules_t2s_function .= "  }\n";
$mgrules_s2t_function .= "  if (\$vverbose) {print STDERR \"    Sandhis final output: \".detranslitteration(\$s).\"\\n\"}\n";
$mgrules_s2t_function .= "  return \$s;\n}\n";
$mgrules_t2s_function .= "  return \$s;\n}\n";

for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<featureset /) {
    / name="(.+?)"/ || die "Please specify the name of the featureset (line ".($linenb+1).")";    
    $name = $1;
     die "Please specify the features within the featureset (using an 'exists' and/or 'except' attribute) (line ".($linenb+1).")" unless / (?:exists?|features?|except)="(.*?)"/;
    / (?:exists?|features?)="(.*?)"/;
    $exist = expand_tagset($1);
    print "\$featureset{\"$name\"} = \"$exist\";\n";
    if (/ except="(.*?)"/) {
      $except = expand_tagset($1);
      print "\$featureset_except{\"$name\"} = \"$except\";\n";
    }
  }
}

my %transfer;
for $category (keys %featurenames) {
  $transfer{$category} = "\$r = \$s";
}
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<transfer /) {
    / name="(.+?)"/ || die "Please specify the name of the transfer rule (line ".($linenb+1).")";    
    $name = "transfer_".normalize_function_name($1);
    / category="(.+?)"/ || die "Please specify the category for the transfer rule (line ".($linenb+1).")";    
    $category = $1;
    if (/ morphomic=\"?1\"?/) {
      $transfer{$category} =~ s/\$s/$name(\$s)/;
    }
    print "sub $name {\n";
    print "  my \$tag = shift;\n";
    print "  my \$newtag = \".\".\$tag;\n";
  } elsif (/<\/transfer>/) {
    print "  return \$tag;\n";
    print "}\n";
  } elsif (/<transfer_?rule/) {
    / source="(.*?)"/ || die "Please specify the source of the transfer rule (line ".($linenb+1).")";
    $source = expand_tagset($1);
    / target="(.*?)"/ || die "Please specify the target of the transfer rule (line ".($linenb+1).")";
    $target = $1;
    die "Found '|' in the target of the transfer rule (line ".($linenb+1).")" if $target =~ /\|/;
    print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if ($source =~ /\|\|/);
    for (split /\|/, $source) {
      print "  if (compatible_feature(\$tag,\"$_\",$category,1)) {\n";
      for (split /\./, $_) {
#	print "    \$newtag =~ s/(^|\\.)".quotemeta($_)."(\\.|\$)/\\1/;\n";
      }
      print "    \$newtag .= \".$target\";\n";
      print "    \$newtag =~ s/^\.//;\n";
      print "    return \$newtag;\n";
      print "  }\n";
    }
  }
}
print "my \%transfer_cache;\n";
print "sub transfer {\n";
print "  my \$s = shift;\n";
print "  my \$cat = shift;\n";
print "  return \$transfer_cache{\$cat}{\$s} if defined(\$transfer_cache{\$cat}) && defined(\$transfer_cache{\$cat}{\$s});\n";
print "  my \$r;\n";
print "  ";
for $category (keys %featurenames) {
  print "if (\$cat eq \"$category\") {\n";
  print "    $transfer{$category};\n";
  print "  } els";
}
print "e {die}\n";
print "  \$transfer_cache{\$cat}{\$s} = \$r;\n";
print "  return \$r;\n";
print "}\n";

my $operation_functions = "";
my %known_operations;
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<operation_definition /) {
    / name="(.+?)"/ || die "Please specify the name of the operation (line ".($linenb+1).")";
    $cur_operation = $1;
    $known_operations{$cur_operation} = 1;
    $operation_functions .= "sub operation_".normalize_function_name($cur_operation)." \{\n  my \$s = shift;  my \$vars = shift; my \$arg1 = shift;  my \$arg2 = shift;  my \$arg3 = shift;\n";
    $in_operation = 1;
  } elsif (/<\/operation_definition/) {
    die "Unexpected end of operation_definition (line ".($linenb+1).")" unless $in_operation;
    $operation_functions .= "  die \"Could not apply operation $cur_operation to string '\".detranslitteration(\$s).\"' (translitterated as '\$s')\";\n}\n";
    $in_operation = 0;
  } elsif (/<identity/) {
    $operation_functions .= "  return \$s;\n";
  } elsif (/<(insert|replace) /) {
    die "Unexpected insert or replace" unless $in_operation;
    if ($1 eq "insert") {
      / pattern="(.+?)"/ || die "Please specify a pattern in the insert element (line ".($linenb+1).")";
      $pattern_input = $1;
      $pattern_output = $1;
      $pattern_input =~ s/_//;
    } elsif ($1 eq "replace") {
      / source="(.+?)"/ || die "Please specify a source in the replace element (line ".($linenb+1).")";
      $pattern_input = $1;
      / target="(.*?)"/ || die "Please specify a target in the replace element (line ".($linenb+1).")";
      $pattern_output = $1;
    }
    my $readable_rule = quotemeta($pattern_input." -> ".$pattern_output);
    $pattern_input = translitteration($pattern_input);
    $pattern_output = translitteration($pattern_output);
    die "No '_' possible on the left hand side of an insert or replace operation definition element (line ".($linenb+1).")" if $pattern_input =~ /_/;
    $pattern_output =~ s/_/ⓥ/;
    ($pattern_input,$pattern_output) = process_operation($linenb,$pattern_input,$pattern_output);
    $pattern_output =~ s/^\^//;
    $pattern_output =~ s/\$$//;
    $pattern_output =~ s/ⓥ/\${arg1}/g;
    $pattern_output =~ s/ⓥ/\${arg2}/g;
    $pattern_output =~ s/ⓥ/\${arg3}/g;
    $pattern_output =~ s/ⓥ/\${arg4}/g;
    $pattern_output =~ s/ⓥ/\${arg5}/g;
    $operation_functions .= "  if (";
    if (/ var=\"(.*?)\"/) {
      $operation_functions .= "\$vars =~ /(^|:)(?:$1)(:|\$)/ && ";
    }
    if (/ varexcept=\"(.*?)\"/) {
      $operation_functions .= "\$vars !~ /(^|:)(?:$1)(:|\$)/ && ";
    }
    $vverbose_content = "if (\$vverbose) {print STDERR \"    Applying operation rule: $readable_rule (line ".($linenb+1).")\\n\"}";
    $operation_functions .= "\$s =~ s/$pattern_input/$pattern_output/";
    if (/ stop="-"/) {
      $operation_functions .= ") {$vverbose_content}\n";
    } else {
      $operation_functions .= ") {$vverbose_content; return \$s}\n";
    }
  }
}

print "my \@stems;\n";

my %patterns;
my @categories;
$cur_level = 0;
$max_block = 0;
$level2level_type[0] = "exponent";
for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<level /) {
    / level="([1-9]\d*)"/ || die "Please specify a valid level rank (line ".($linenb+1).")";
    $cur_level = $1;
    if (/ type="(exponent|theme|stem)"/) {
      die "You cannot specify twice the type of a level (here level $cur_level) (line ".($linenb+1).")" if defined($level2level_type[$cur_level]);
      $level2level_type[$cur_level] = $1;
    } elsif (/ type="(.*?)"/) {
      die "Unknown level type $1 (line ".($linenb+1).")";
    }
  } elsif (/<\/level/) {
    $cur_level = 0;
  } elsif (/<partition_?space /) {
    / name="(.+?)"/ || die "Please specify a name for the partition_space (line ".($linenb+1).")";
    $partitionspace_name = $1;
    print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if (/\|\|/);
    if (/ category="(.+?)"/) {
      @categories = split /\|/, $1;
    } else {
      @categories = keys %featurenames;
    }
    if (/ features="(.*?)"/) {
      for (@categories) {
	die "Partition_space defined at least twice for category '$_' (line ".($linenb+1).")" if defined($partitionspace2features{$_}{$partitionspace_name}{features});
	$partitionspace2features{$_}{$partitionspace_name}{features} = expand_tagset($1);
      }
    }
    if (/ (?:features?_?)?except="(.+?)"/) {
      for (@categories) {
	die "Partition_space defined at least twice for category '$_' (line ".($linenb+1).")" if defined($partitionspace2features{$_}{$partitionspace_name}{features_except});
	$partitionspace2features{$_}{$partitionspace_name}{features_except} = expand_tagset($1);
      }
    }
  } elsif (/<zoneref /) {
    die "'zoneref' elements are only allowed within tables (line ".($linenb+1).")" unless $in_table;
    /\/>/ || die "'zoneref' elements must be one-liners of the form <zoneref ... /> (line ".($linenb+1).")";
    / name="(.+?)"/ || die "Please specify a name for the referred zone (line ".($linenb+1).")";
    my $ref_zone = $1;
    die "No 'exclude' allowed in zonerefs (line ".($linenb+1).")" if / exclude="(.+?)"/;
    if ($in_default_zone) {
      $in_zone = 0;
      if ($level2level_type[$cur_level] eq "stem") {
	$zone_function .= "  return \"__TBD(stem:\".detranslitteration(\$s).\")__\";\n";
      } else {
	$zone_function .= "    if (\$block == \$prev_block && \$block <= $max_block) {\n";
	$zone_function .= "      \$didsomething = 1 unless (\$block == $max_block);\n";
	$zone_function .= "      \$block++;\n";
	$zone_function .= "      print STDERR \"Skipping block $block as no adequate rule was found\\n\" if \$verbose;\n";
	$zone_function .= "    }\n";
	$zone_function .= "    last if (\$block == $max_block + 1)\n";
	$zone_function .= "  }\n";
	#      $zone_function .= "  return \$s;\n";
	$zone_function .= "  return \$s if (\$block == $max_block + 1 && \$didsomething);\n";
	$zone_function .= "  return \"__TBD__\";\n";
      }
      $zone_function .= "}\n";
      print $zone_function;
      $zone_function = "";
    }
    $in_default_zone = 0;
    $in_zone = 0;
    $table_function .= "  \$o = zone_".normalize_function_name($ref_zone)."(\$s, \$partitionspace, \$vars, \$tag, \$category);\n";
    $table_function .= "  print STDERR \"### Could not apply '$ref_zone' on '\".detranslitteration(\$s).\"' for tag '\$tag' and partitionspace '\$partitionspace' for category '\$category'\\n\" if (\$verbose && \$o =~ /^__TBD/);\n";
    $table_function .= "  return \$o unless (\$o =~ /^__TBD/);\n";
  } elsif (/<(zone|table) /) {
    $zone_or_table = $1;
    $in_zone = 1 if ($zone_or_table eq "zone");
    $in_default_zone = ($zone_or_table eq "table");
    / name="(.+?)"/ || die "Please specify a name for the $zone_or_table (line ".($linenb+1).")";
    $cur_zone = $1;
    if ($zone_or_table eq "table") {
      $in_table = 1;
      $cur_table = $cur_zone;
      if (/ cat(?:egory)?="(.+?)"/) {
	$category = $1;
      } else {
	$category = "";
      }
      $table_function = "sub table_".normalize_function_name($cur_table)." {\n";
      $table_function .= "  my \$s = shift;\n";
      $table_function .= "  my \$partitionspace = shift;\n";
      $table_function .= "  my \$vars = shift;\n";
      $table_function .= "  my \$tag = shift;\n";
      $table_function .= "  my \$category = shift;\n";
      $table_function .= "  my \$o;\n";
    }
    $max_block = 0;
    $table_function .= "  \$o = zone_".normalize_function_name($cur_zone)."(\$s, \$partitionspace, \$vars, \$tag, \$category);\n";
    $table_function .= "  print STDERR \"### Could not apply '$cur_zone' on '\".detranslitteration(\$s).\"' for tag '\$tag' and partitionspace '\$partitionspace' for category '\$category'\\n\" if (\$verbose && \$o =~ /^__TBD/);\n";
    $table_function .= "  return \$o unless (\$o =~ /^__TBD/);\n";
    if (/ exclude="(.+?)"/) {
      die "No exclude allowed in tables with type 'stem' (line ".($linenb+1).")" if ($level2level_type[$cur_level] eq "stem");
      $exclude = expand_tagset($1);
    } else {
      $exclude = "";
    }
    $zone_function = "sub zone_".normalize_function_name($cur_zone)." {\n";
    $zone_function .= "  my \$s = shift;\n";
    $zone_function .= "  my \$partitionspace = shift;\n";
    $zone_function .= "  my \$vars = shift;\n";
    $zone_function .= "  my \$tag = shift;\n";
    $zone_function .= "  my \$category = shift;\n";
    if ($level2level_type[$cur_level] eq "stem") {
      $zone_function .= "  return \"__TBD($cur_level)__\" unless (\$tag eq \"\" || compatible_partitionspace(\$tag,\$partitionspace, \$category));\n";
    } else {
      if ($exclude ne "") {
	$zone_function .= "  return \"__NONE($cur_level)__\" if (compatible_feature(\$tag,\"$exclude\", \$category));\n";
      }
      $zone_function .= "  return \"__NONE($cur_level)__\" unless (compatible_partitionspace(\$tag,\$partitionspace, \$category));\n";
      $zone_function .= "  if (\$s =~ /^__TBD/) {\n";
#      $zone_function .= "    print STDERR \"\#\#\# could not inflect for tag \$tag (\$s)\\n\";\n";
      $zone_function .= "    return \"\$s\";\n";
      $zone_function .= "  } elsif (\$s =~ /^__NONE/) {\n";
      $zone_function .= "    return \"\$s\";\n";
      $zone_function .= "  }\n";
      $zone_function .= "  \$s =~ s/(^|\\t)__TBD.*?__//g;\n";
      $zone_function .= "  my \$didsomething = 1;\n";
      $zone_function .= "  my \$block = 1;\n";
      $zone_function .= "  my \$prev_block = 1;\n";
      $zone_function .= "  while (\$didsomething) {\n";
      $zone_function .= "    \$didsomething = 0;\n";
      $zone_function .= "    \$prev_block = \$block;\n";
    }
  } elsif (/<(?:item|stem|rule|form) /) {
    if ($level2level_type[$cur_level] eq "stem") {
      / name="S([1-9]\d*)(.*?)"/ || die "Please specify a valid stem name ('S' followed by a non-null integer) (line ".($linenb+1).")";
      $stem_id = $1;
      $stem_id_cplmt = $2;
      $zone_function .= "  if (((\$partitionspace =~ /(^|\\+)S$stem_id$stem_id_cplmt(\\+|\$)/ && \$tag eq \"\") || ((\$partitionspace =~ /(^|\\+)S$stem_id$stem_id_cplmt(\\+|\$)/ || \$partitionspace eq \"\") && \$tag ne \"\""; # ??? $stem_id_cplmt
#      if (defined($partitionspace2features{$zzzcat}{"S$stem_id$stem_id_cplmt"}{features})) {
	$zone_function .= " && compatible_feature(\$tag,\$partitionspace2features{\$category}{\"S$stem_id$stem_id_cplmt\"}{features}, \$category)";
#      }
#      if (defined($partitionspace2features{$zzzcat}{"S$stem_id$stem_id_cplmt"}{features_except})) {
	$zone_function .= " && !(\$partitionspace2features{\$category}{\"S$stem_id$stem_id_cplmt\"}{features_except} ne \"\" && compatible_feature(\$tag,\$partitionspace2features{\$category}{\"S$stem_id$stem_id_cplmt\"}{features_except}, \$category))";
#      }
      $zone_function .= "))";
    } else {
      if (/ block=\"(\d+)-(\d+)\"/) {
	$input_block = $1;
	$output_block = $2+1;
	if ($2 > $max_block) {$max_block = $2}
      } elsif (/ block=\"(\d+)\"/) {
	$input_block = $1;
	$output_block = $1+1;
	if ($1 > $max_block) {$max_block = $1}
      } else {
	$input_block = 1;
	$output_block = 2;
	if (1 > $max_block) {$max_block = 1}
      }
      $zone_function .= "    if (\$block == $input_block";
    }
    if (/ var=\"(.*?)\"/) {
      $zone_function .= " && \$vars =~ /(^|:)(?:$1)(:|\$)/";
    }
    if (/ varexcept=\"(.*?)\"/) {
      $zone_function .= " && \$vars !~ /(^|:)(?:$1)(:|\$)/";
    }
    if (/ (?:features?|tag)=\"(.*?)\"/) {
      $features = expand_tagset($1);
      $zone_function .= " && compatible_feature(\$tag,\"$features\", \$category)";
      print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if ($features =~ /\|\|/);
      for (split /\s*\|\s*/, $features) {
	$features[$cur_level]{$cur_zone}{$_} = 1;
      }
    }
    if (/ (?:features?_)?except=\"(.*?)\"/) {
      $except = expand_tagset($1);
      $zone_function .= " && !compatible_feature(\$tag,\"$except\", \$category)";
    }
    $zone_function .= ") {\n";
    if ($level2level_type[$cur_level] eq "stem") {
      if (/name="S\d[^"]*"\s*\/>/) {
	if ($stem_id == 1) {
	  $zone_function .= "    \$stems[$stem_id] = \$stems[0] if \$stems[$stem_id] eq \"\";\n";
	} else {
	  $zone_function .= "    die \"Stem S$stem_id has to be lexically specified\" if \$stems[$stem_id] eq \"\";\n";
	}
	$zone_function .= "    return \$stems\[$stem_id\];\n";
	$zone_function .= "  }\n";
      } elsif (/ source=\"S([0-9]\d*)(.*?)\"/) {
	$source_stem_id = $1;
	$source_stem_id_cmplt = $2;
	$zone_function .= "    if (defined(\$stems[$stem_id])) {\n";
	$zone_function .= "      print STDERR \"S$stem_id is \".detranslitteration(\$stems[$stem_id]).\" (previously computed)\\n\" if (\$vverbose);\n";
	$zone_function .= "      return \$stems[$stem_id]\n";
	$zone_function .= "    }\n";
	if ($source_stem_id > 0) {
	  $zone_function .= "    if (\$stems[$source_stem_id] eq \"\") {\n";
	  $zone_function .= "      \$s = zone_".normalize_function_name($cur_zone)."(\$s,\"S$source_stem_id$source_stem_id_cmplt\",\"\$vars\",\"\",\$category);\n";
	  $zone_function .= "      print STDERR \"### Could not apply '$cur_zone' on '\".detranslitteration(\$s).\"' for partitionspace 'S$source_stem_id$source_stem_id_cmplt' with vars '\$vars' and category '\$category'\\n\" if (\$verbose && \$s =~ /^__(?:TBD|NONE)/);\n";
	  $zone_function .= "    } else {\n";
	  $zone_function .= "      \$s = \$stems[$source_stem_id];\n";
	  $zone_function .= "    }\n";
	} else {
	  $zone_function .= "    \$s = \$stems[$source_stem_id];\n";
	}
      } else {
	die "Incorrect item. If it is not for a non-computable stem, did you specify the source? (line ".($linenb+1).")";
      }
    }
    if ($level2level_type[$cur_level] ne "stem" || / source=\"S[0-9]\d*.*?\"/) {
      if ($level2level_type[$cur_level] ne "stem") {$sep = " "} else {$sep = ""}
      $zone_function .= "      if (1";
      if (/ rads="(.*?)"/) {
	my $rads = translitteration($1);
	$rads =~ s/\[:([^:]+):\]/$letterclassname2re{$1}/g;
	$zone_function .= " && \$s =~ /^$rads\$/";
      }
      if (/ radsexcept="(.*?)"/) {
	my $radsexcept = translitteration($1);
	$radsexcept =~ s/\[:([^:]+):\]/$letterclassname2re{$1}/g;
	$zone_function .= " && \$s !~ /^$radsexcept\$/";
      }
      $zone_function .= ") {\n";
      print STDERR "### WARNING: found dubious pattern '||' (line ".($linenb+1).")\n" if (/\|\|/);
      if (/ operation=\"\"/) {
      } elsif (/ operation=\"((?:-[^"]+)?(?:\+[^"]+)?(?:\|(?:-[^"]+)?(?:\+[^"]+)?)*)\"/) {
	$zone_function .= "         print STDERR \"Applying level $cur_level rule $1 on \".detranslitteration(\$s).\" (rule line ".($linenb+1).")\\n\" if (\$vverbose);\n";
	$zone_function .= "         \$s .= \"".translitteration($1)."\";\n";
	$zone_function .= "         while (\$s =~ s/^(.*?)([^-+]+)-\\2(?:\\+([^-+\\|]+))?(\\|.*|\\t|\$)/$sep\$1\$3/ || \$s =~ s/^(.*?)-[^-+]+(?:\\+[^-+\\|]+)?(\\||\\t|\$)/$sep\$1/) {}\n";
	$zone_function .= "         \$s =~ s/^(.*?)\\+([^-+\\|]+).*(\\t|\$)/\$1$sep\$2\$3/;\n";
	$zone_function .= "         print STDERR \"    Success. Output: \".detranslitteration(\$s).\"\\n\" if (\$vverbose);\n";
      } elsif (/ operation=\"([^"]*)\"/) {
	my $operations = $1;
	for my $operation (split /\+/, $operations) {
	  if ($operation =~ /^([^"\(]+)(?:\(\))?$/) {
	    my $op = $1;
	    die "Undefined operation $op (line ".($linenb+1).")" unless defined($known_operations{$op});
	    $zone_function .= "         print STDERR \"Applying level $cur_level rule $op() on \".detranslitteration(\$s).\" (rule line ".($linenb+1).")\\n\" if (\$vverbose);\n";
	    $zone_function .= "         \$s = operation_".normalize_function_name($op)."(\$s,\$vars);\n";
	    $zone_function .= "         print STDERR \"    Success. Output: \".detranslitteration(\$s).\"\\n\" if (\$vverbose);\n";
	  } elsif ($operation =~ /^([^"\(]+)\(([^"\(\)]+)\)$/) {
	    my $op = $1;
	    die "Undefined operation $op (line ".($linenb+1).")" unless defined($known_operations{$op});
	    $zone_function .= "         print STDERR \"Applying level $cur_level rule $op($2) on \".detranslitteration(\$s).\" (rule line ".($linenb+1).")\\n\" if (\$vverbose);\n";
	    $args = "\"".translitteration($2)."\"";
	    $args =~ s/,/","/g;
	    $zone_function .= "         \$s = operation_".normalize_function_name($op)."(\$s,\$vars,$args);\n";
	    $zone_function .= "         print STDERR \"    Success. Output: \".detranslitteration(\$s).\"\\n\" if (\$vverbose);\n";
	  }
	}
      } elsif (/ (?:(?:pre|suf)fix|(?:pre|l(?:eft)?_?a|a)ppend)="[^"]*"/) {
	$prefix = "";
	$suffix = "";
	if (/ (?:prefix|prepend|l(?:eft)?_?append)=\"([^\"]*)\"/) {
	  $prefix = $1;
	}
	if (/ (?:suffix|append)=\"([^\"]*)\"/) {
	  $suffix = $1;
	}
	$zone_function .= "         print STDERR \"Applying level $cur_level rule ";
	if ($prefix ne "") {$zone_function .= "$prefix-"}
	$zone_function .= "X";
	if ($suffix ne "") {$zone_function .= "-$suffix"}
	$zone_function .= " on \".detranslitteration(\$s).\" (rule line ".($linenb+1).")\\n\" if (\$vverbose);\n";
	unless ($prefix eq "" && $suffix eq "") {
	  $zone_function .= "         \$s = ";
	  if ($prefix ne "") {$zone_function .= "\"".quotemeta(translitteration($prefix))."$sep\"."}
	  $zone_function .= "\$s";
	  if ($suffix ne "") {$zone_function .= ".\"$sep".quotemeta(translitteration($suffix))."\""}
	  $zone_function .= ";\n";
	}
      } elsif (/ fail="?1"?/) {
	$zone_function .= "         print STDERR \"    Failure for level $cur_level in block $input_block (failing rule line ".($linenb+1).")\\n\" if (\$vverbose);\n";
	$zone_function .= "         return(\"__NONE(b$input_block)__\");\n";	
	$zone_function .= "         print STDERR \"### Reached matching fail item on '\".detranslitteration(\$s).\"', zone '$cur_zone' (block $input_block) for tag '\$tag' with vars '\$vars' and category '\$category'\\n\" if \$verbose;\n";
      } elsif (/ like=/) {
      } elsif (/ auxlike/) {
      } else {
	die "Incorrect item/stem/rule: $_ (line ".($linenb+1).")\n";
      }
      if ($level2level_type[$cur_level] ne "stem") {
	$zone_function .= "         \$didsomething = 1;\n";
	$zone_function .= "         \$block = $output_block;\n";
      } else {
	$zone_function .= "         \$stems\[$stem_id\] = \$s;\n";
	$zone_function .= "         print STDERR \"S$stem_id is \$s\\n\" if (\$vverbose);\n";
	$zone_function .= "         return \$s;\n";
      }
      $zone_function .= "      }\n" unless /name="(S$stem_id)"\s*\/>/;
      $zone_function .= "    }\n";
    }
  } elsif (/<\/table>/) {
    die "Unexpected end of table (line ".($linenb+1).")" unless $in_table;
    if ($in_zone || $in_default_zone) {
      if ($level2level_type[$cur_level] eq "stem") {
	$zone_function .= "  print STDERR \"### Could not create stem '\$partitionspace' (or stem for tag '\$tag') for '\".detranslitteration(\$s).\"', with vars '\$vars' and category '\$category'\\n\" if \$verbose;\n";
	$zone_function .= "  return \"__TBD(stem:\".detranslitteration(\$s).\")__\";\n";
	$zone_function .= "}\n";
      } else {
	$zone_function .= "    if (\$block == \$prev_block && \$block <= $max_block) {\n";
	$zone_function .= "      \$didsomething = 1 unless (\$block == $max_block);\n";
	$zone_function .= "      \$block++;\n";
	$zone_function .= "    }\n";
	$zone_function .= "    last if (\$block == $max_block + 1)\n";
	$zone_function .= "  }\n";
	#      $zone_function .= "  return \$s;\n";
	$zone_function .= "  return \$s if (\$block == $max_block + 1 && \$didsomething);\n";
	$zone_function .= "  return \"__TBD__\";\n";
	$zone_function .= "}\n";
      }
    }
    print $zone_function;
    $in_table = 0;
    $in_zone = 0;
    $in_default_zone = 0;
    $table_function .= "  return \$o;\n";
    $table_function .= "}\n";
    print $table_function;
    if ($category ne "") {
      $patterns{$cur_table} = 1;
      print "sub pattern_".normalize_function_name($cur_table)." {\n";
      print <<END;
  my \$s = shift;
  my \$vars = shift;
  my \$tagspace = shift;
  my \@output = ();
  my \%tags_processed_via_suppletion = ();
  for \$index (0..\$#{\$tags{"$category"}}) {
    \$tag = \$tags{"$category"}[\$index];
    next unless valid_tag("$category",\$tag);
    next if (\$only_features ne "" && !compatible_feature(\$tag,\$only_features, "$category"));
    my \%suppletivetag2output;
    for my \$stag (keys \%suppletive_forms) {
      if (compatible_feature(\$stag,\$tag, "$category")) {
        for \$o (keys \%{\$suppletive_forms{\$stag}}) {
          if (\$dont_show_all) {
            \$o2 = \$show_tags_display{"$category"}[\$index];
            \$o2 =~ s/#/\$o/g;
            \$o = \$o2;
          }
          \$suppletivetag2output{\$tag} = "";
          for (split /\|/, \$o) {
            next if \$_ eq "" || \$_ !~ /^__(?:NONE|TBD)/;
            \$suppletivetag2output{\$tag} .= "\\n" if \$suppletivetag2output{\$tag} ne "";
            \$suppletivetag2output{\$tag} .= \$s."\\t".\$_."\\t".\$tag;
          }
          \$tags_processed_via_suppletion{\$tag} = 1;
        }
      }
    }
  }
  for \$index (0..\$#{\$tags{"$category"}}) {
    \$tag = \$tags{"$category"}[\$index];
    next unless valid_tag("$category",\$tag);
    next if (\$only_features ne "" && !compatible_feature(\$tag,\$only_features, "$category"));
    print STDERR "===== tag \$tag =====\\n" if (\$vverbose);
    next unless ((!defined(\$featureset{"\$tagspace"}) || compatible_feature(\$tag,\$featureset{"\$tagspace"}, "$category")) && (!defined(\$featureset_except{"\$tagspace"}) || !compatible_feature(\$tag,\$featureset_except{"\$tagspace"}, "$category")));
    if (defined(\$tags_processed_via_suppletion{\$tag}) && \$suppletivetag2output{\$tag} ne "") {
      for (split /\\n/, \$suppletivetag2output{\$tag}) {
        push \@output, \$_ unless $_ eq "";
      }
    } else {
      \$o = \$s;
END
      print "      \$o = table_".normalize_function_name($cur_table)."(\$o, \"\", \$vars, \$tag, \"$category\");\n";
      print <<END;
      \$o = mgrules_s2t(\$o);
      push \@output, \$s."\\t".\$o."\\t".\$tag unless \$o =~ /^__(?:NONE|TBD)/;
    }
  }
  \@stems = \@orig_stems;
  return join "\\n", \@output;
}
END
  }
    $cur_table = "";
    $cur_zone = "";
  } elsif (/<\/zone>/) {
    die "Unexpected end of zone (line ".($linenb+1).")" unless $in_zone;
    $in_zone = 0;
    if ($level2level_type[$cur_level] eq "stem") {
      $zone_function .= "  return \"__TBD(stem:\".detranslitteration(\$s).\")__\";\n";
    } else {
      $zone_function .= "    if (\$block == \$prev_block && \$block <= $max_block) {\n";
      $zone_function .= "      \$didsomething = 1 unless (\$block == $max_block);\n";
      $zone_function .= "      \$block++;\n";
      $zone_function .= "    }\n";
      $zone_function .= "    last if (\$block == $max_block + 1)\n";
      $zone_function .= "  }\n";
#      $zone_function .= "  return \$s;\n";
      $zone_function .= "  return \$s if (\$block == $max_block + 1 && \$didsomething);\n";
      $zone_function .= "  return \"__TBD__\";\n";
    }
    $zone_function .= "}\n";
    print $zone_function;
    $zone_function = "";
    $cur_zone = $cur_table;
  }
}

for $category (keys %featurenames) {
  print "\@{\$all_tags{\"$category\"}} = (".(join (",", map {"\"$_\""} sort {tags_ordering($a,$category) <=> tags_ordering($b,$category) || $a cmp $b} @{$std_tags{$category}})).");\n";
  print "\@{\$show_tags{\"$category\"}} = (".(join (",", map {"\"$_\""} @{$show_tags{$category}})).");\n";
  print "\@{\$show_tags_display{\"$category\"}} = (".(join (",", map {"\"$_\""} @{$show_tags_display{$category}})).");\n";
  print <<END;
my \@tags;
if (\$dont_show_all) {
  \@{\$tags{\"$category\"}} = \@{\$show_tags{\"$category\"}};
} else {
  \@{\$tags{\"$category\"}} = \@{\$all_tags{\"$category\"}};
}
END
}

for $linenb (0..$#lines) {
  $_ = $lines[$linenb];
  if (/<pattern /) {
    $in_pattern = 1;
    / cat(?:egory)?="(.+?)"/ || die "Please specify a category for the pattern (line ".($linenb+1).")";
    $category = $1;
    / name="(.+?)"/ || die "Please specify a name for the pattern (line ".($linenb+1).")";
    $pattern_function = "sub pattern_".normalize_function_name($1)." {\n";
    die "Pattern or table with the same name already defined (line ".($linenb+1).")" if defined($patterns{$1});
    $patterns{$1} = 1;
    $pattern_function .= "  my \$s = shift;\n";
    $pattern_function .= "  my \$vars = shift;\n";
    $pattern_function .= "  my \$tagspace = shift;\n";
    $pattern_function .= "  my \@output = ();\n";
    $pattern_function .= "  my \%tags_processed_via_suppletion = ();\n";
    $pattern_function .= "  my \%suppletivetag2output = ();\n";
    $pattern_function .= "  for \$index (0..\$#{\$tags{\"$category\"}}) {\n";
    $pattern_function .= "    \$tag = \$tags{\"$category\"}[\$index];\n";
    $pattern_function .= "    next if (\$only_features ne \"\" && !compatible_feature(\$tag,\$only_features, \"$category\"));\n";
    $pattern_function .= "    for my \$stag (keys \%suppletive_forms) {\n";
    $pattern_function .= "      if (compatible_feature(\$stag,\$tag, \"$category\")) {\n";
    $pattern_function .= "        for \$o (keys \%{\$suppletive_forms{\$stag}}) {\n";
    $pattern_function .= "          if (\$dont_show_all) {\n";
    $pattern_function .= "            \$o2 = \$show_tags_display{\"$category\"}[\$index];\n";
    $pattern_function .= "            \$o2 =~ s/#/\$o/g;\n";
    $pattern_function .= "            \$o = \$o2;\n";
    $pattern_function .= "          }\n";
    $pattern_function .= "          \$suppletivetag2output{\$tag} = \"\";\n";
    $pattern_function .= "          for (split /\\|/, \$o) {\n";
    $pattern_function .= "            next if \$_ eq \"\" || \$_ =~ /^__(?:NONE|TBD)/;\n";
    $pattern_function .= "            \$suppletivetag2output{\$tag} .= \"\\n\" if \$suppletivetag2output{\$tag} ne \"\";\n";
    $pattern_function .= "            \$suppletivetag2output{\$tag} .= \$s.\"\\t\".\$_.\"\\t\".\$tag;\n";
    $pattern_function .= "          }\n";
    $pattern_function .= "          \$tags_processed_via_suppletion{\$tag} = 1;\n";
    $pattern_function .= "        }\n";
    $pattern_function .= "      }\n";
    $pattern_function .= "    }\n";
    $pattern_function .= "  }\n";
  } elsif (/<\/pattern/) {
    die "Unexpected end of pattern (line ".($linenb+1).")" unless $in_pattern;
    $in_pattern = 0;
    $pattern_function .= "  return join \"\\n\", \@output;\n";
    $pattern_function .= "}\n";
    print $pattern_function;
  } elsif (/<subpattern/) {
    die "Unexpected subpattern (line ".($linenb+1).")" unless $in_pattern;
    $in_subpattern = 1;
    %subpattern = ();
    if (/ var=\"(.*?)\"/) {
      $subpattern_var = $1; # pas de quotemeta pour gérer les | ; était "quotemeta($1)"
    } else {
      $subpattern_var = "";
    }
    if (/ varexcept=\"(.*?)\"/) {
      $subpattern_varexcept = $1; # pas de quotemeta pour gérer les | ; était "quotemeta($1)"
    } else {
      $subpattern_varexcept = "";
    }
  } elsif (/<\/subpattern>/) {
    die "Unexpected subpattern end (line ".($linenb+1).")" unless $in_subpattern;
    $in_subpattern = 0;
    $pattern_function .= "  if (1";
    if ($subpattern_var ne "") {
      $pattern_function .= " && \$vars =~ /(^|:)(?:$subpattern_var)(:|\$)/";
    }
    if ($subpattern_varexcept ne "") {
      $pattern_function .= " && \$vars !~ /(^|:)(?:$subpattern_varexcept)(:|\$)/";
    }
    $pattern_function .= ") {\n";
    $pattern_function .= "    for \$index (0..\$#{\$tags{\"$category\"}}) {\n";
    $pattern_function .= "      \$tag = \$tags{\"$category\"}[\$index];\n";
    $pattern_function .= "      next if (\$only_features ne \"\" && !compatible_feature(\$tag,\$only_features, \"$category\"));\n";
    $pattern_function .= "      print STDERR \"===== tag \$tag =====\\n\" if (\$vverbose);\n";
    $pattern_function .= "      next unless (compatible_feature(\$tag,\$featureset{\"\$tagspace\"}, \"$category\") && (!defined(\$featureset_except{\"\$tagspace\"}) || !compatible_feature(\$tag,\$featureset_except{\"\$tagspace\"}, \"$category\")));\n";
    $pattern_function .= "      if (defined(\$tags_processed_via_suppletion{\$tag}) && \$suppletivetag2output{\$tag} ne \"\") {\n";
    $pattern_function .= "        for (split /\\n/, \$suppletivetag2output{\$tag}) {\n";
    $pattern_function .= "          push \@output, \$_ unless \$_ eq \"\";\n";
    $pattern_function .= "        }\n";
    $pattern_function .= "      } else {\n";
    $pattern_function .= "        \$o = \$s;\n";
#tsatl    $pattern_function .= "        my \$tried_something_at_this_level;\n";
    for $level (sort {$a <=> $b} keys %subpattern) {
      $pattern_function .= "        print STDERR \"-- LEVEL $level --\\n\" if (\$vverbose);\n";
      $pattern_function .= "        \$didsomething_at_this_level = 0;\n";
#tsatl      $pattern_function .= "        \$tried_something_at_this_level = 0;\n";
      for $i (0..$#{$subpattern{$level}{var}}) {
	$pattern_function .= "        if (\$didsomething_at_this_level == 0 && \$o !~ /__(?:NONE|TBD)/";
	if (defined($subpattern{$level}{partitionspace}[$i])) {
	  $pattern_function .= "   && compatible_partitionspace(\$tag,\"".$subpattern{$level}{partitionspace}[$i]."\", \"$category\")";
	}
	if ($subpattern{$level}{features}[$i] ne "") {
	  $pattern_function .= "   && compatible_feature(\$tag,\"$subpattern{$level}{features}[$i]\", \"$category\")";
	}
	if ($subpattern{$level}{features_except}[$i] ne "") {
	  $pattern_function .= "   && !compatible_feature(\$tag,\"$subpattern{$level}{features_except}[$i]\", \"$category\")";
	}
	if ($subpattern{$level}{var}[$i] ne "") {
	  $pattern_function .= "   && \$vars =~ /(^|:)(?:$subpattern{$level}{var}[$i])(:|\$)/";
	}
	if ($subpattern{$level}{varexcept}[$i] ne "") {
	  $pattern_function .= "   && \$vars !~ /(^|:)(?:$subpattern{$level}{varexcept}[$i])(:|\$)/";
	}
	if ($subpattern{$level}{rads}[$i] ne "") {
	  $pattern_function .= "   && \$o =~ /^".$subpattern{$level}{rads}[$i]."\$/";
	}
	if ($subpattern{$level}{radsexcept}[$i] ne "") {
	  $pattern_function .= "   && \$o !~ /^".$subpattern{$level}{radsexcept}[$i]."\$/";
	}
	$pattern_function .= "  ) {\n";
	if ($subpattern{$level}{transfer}[$i] eq "") {
	  $tagwithtransfer = "\$tag";
	} else {
	  $tagwithtransfer = "transfer_".normalize_function_name($subpattern{$level}{transfer}[$i])."(\$tag)";
	}
	if (defined($subpattern{$level}{partitionspace}[$i])) {
	  $pattern_function .= "          \$o2 = table_".normalize_function_name($subpattern{$level}{table}[$i])."(\$o, \"".$subpattern{$level}{partitionspace}[$i]."\", \$vars.\":".$subpattern{$level}{lvar}[$i]."\", \$tag, \"$category\"); #1\n";
	} elsif (defined($subpattern{$level}{zone}[$i])) {
	  $pattern_function .= "          \$o2 = zone_".normalize_function_name($subpattern{$level}{zone}[$i])."(\$o, \"\", \$vars.\":".$subpattern{$level}{lvar}[$i]."\", \$tag, \"$category\"); #2\n";
	} else {
	  die "Internal error (line ".($linenb+1).")";
	}
	if ($subpattern{$level}{on_failure}[$i] eq "fail") {
	  $pattern_function .= "          \$o = \$o2;\n";
	  $pattern_function .= "          \$didsomething_at_this_level = 1;\n";
	} else {
	  $pattern_function .= "          \$o = \$o2 unless \$o2 =~ /^__(?:NONE|TBD)/;\n";
	  $pattern_function .= "          \$didsomething_at_this_level = 1 unless \$o2 =~ /^__(?:NONE|TBD)/;\n";
	}
#tsatl	$pattern_function .= "          \$tried_something_at_this_level = 1;\n";
	if ($subpattern{$level}{on_failure}[$i] eq "fail") {
	  $pattern_function .= "        } else {\n";
	  $pattern_function .= "          \$o = \"__NONE($level)__\" if (\$didsomething_at_this_level == 0);\n";
	}
	$pattern_function .= "        }\n";
      }
#tsatl      $pattern_function .= "        if (\$tried_something_at_this_level == 0) {\n";
#tsatl      $pattern_function .= "          \$o = \"__NONE($level)__\";\n";
#tsatl      $pattern_function .= "        }\n";
    }
    $pattern_function .= "        \$o = mgrules_s2t(\$o);\n";
    $pattern_function .= "        if (\$dont_show_all) {\n";
    $pattern_function .= "          \$o2 = \$show_tags_display{\"$category\"}[\$index];\n";
    $pattern_function .= "          \$o2 =~ s/#/\$o/g;\n";
    $pattern_function .= "          \$o = \$o2;\n";
    $pattern_function .= "        }\n";
    $pattern_function .= "        push \@output, \$s.\"\\t\".\$o.\"\\t\".\$tag unless \$o =~ /^__(?:NONE|TBD)/;\n";
    $pattern_function .= "      }\n";
    $pattern_function .= "    }\n";
    $pattern_function .= "    \@stems = \@orig_stems;\n";
    $pattern_function .= "  }\n";
    %subpattern = ();
  } elsif (/<realzone /) {
    die "Unexpected realzone (line ".($linenb+1).")" unless $in_subpattern;
    / level="(.+?)"/ || die "Please specify a level for the zone (line ".($linenb+1).")";
    $level = $1;
    $on_failure = "fail";
    if (/ on_?failure="(.*?)"/) {
      if ($1 eq "skip") {
	$on_failure = "skip";
      } else {
	die "Invalid value for on_failure (line ".($linenb+1).")";
      }
    }
    if (/ table="(.+?)"/) {
      $table = $1;
      if (/ zone="(.+?)"/) {
	die "You cannot specify both a zone and a table in a realzone element (line ".($linenb+1).")";
      }
      if (/ partition_?space="(.+?)"/) {
	$partitionspace = $1;
      } else {
	$partitionspace = "";
      }
      if (/ var="(.+?)"/) {
	$var = $1;
      } else {
	$var = "";
      }
      if (/ varexcept="(.+?)"/) {
	$varexcept = $1;
      } else {
	$varexcept = "";
      }
      if (/ l(?:ocal)?_?var="(.+?)"/) {
	$lvar = $1;
      } else {
	$lvar = "";
      }
      if (/ transfer="(.+?)"/) {
	$transfer = $1;
      } else {
	$transfer = "";
      }
      if (/ (?:exists?|features?)="(.*?)"/) {
	$exist = expand_tagset($1);
      } else {
	$exist = "";
      }
      if (/ (?:(?:features?_?)?excepts?)="(.*?)"/) {
	$except = expand_tagset($1);
      } else {
	$except = "";
      }
      if (/ rads="(.*?)"/) {
	$rads = translitteration($1);
	$rads =~ s/\[:([^:]+):\]/$letterclassname2re{$1}/g;
      } else {
	$rads = "";
      }
      if (/ radsexcept="(.*?)"/) {
	$radsexcept = translitteration($1);
	$radsexcept =~ s/\[:([^:]+):\]/$letterclassname2re{$1}/g;
      } else {
	$radsexcept = "";
      }
      push @{$subpattern{$level}{table}}, $table;
      push @{$subpattern{$level}{partitionspace}}, $partitionspace;
      push @{$subpattern{$level}{features}}, $exist;
      push @{$subpattern{$level}{features_except}}, $except;
      push @{$subpattern{$level}{var}}, $var;
      push @{$subpattern{$level}{varexcept}}, $varexcept;
      push @{$subpattern{$level}{rads}}, $rads;
      push @{$subpattern{$level}{radsexcept}}, $radsexcept;
      push @{$subpattern{$level}{lvar}}, $lvar;
      push @{$subpattern{$level}{transfer}}, $transfer;
      push @{$subpattern{$level}{on_failure}}, $on_failure;
    } elsif (/ zone="(.+?)"/) {
      push @{$subpattern{$level}{zone}}, $1;
      if (/ var="(.+?)"/) {
	$var = $1;
      } else {
	$var = "";
      }
      if (/ varexcept="(.+?)"/) {
	$varexcept = $1;
      } else {
	$varexcept = "";
      }
      if (/ l(?:ocal)?_?var="(.+?)"/) {
	$lvar = $1;
      } else {
	$lvar = "";
      }
      if (/ transfer="(.+?)"/) {
	$transfer = $1;
      } else {
	$transfer = "";
      }
      if (/ (?:exists?|features?)="(.*?)"/) {
	$exist = expand_tagset($1);
      } else {
	$exist = "";
      }
      if (/ (?:(?:features?_?)?excepts?)="(.*?)"/) {
	$except = expand_tagset($1);
      } else {
	$except = "";
      }
      if (/ rads="(.*?)"/) {
	$rads = translitteration($1);
	$rads =~ s/\[:([^:]+):\]/$letterclassname2re{$1}/g;
      } else {
	$rads = "";
      }
      if (/ radsexcept="(.*?)"/) {
	$radsexcept = translitteration($1);
	$radsexcept =~ s/\[:([^:]+):\]/$letterclassname2re{$1}/g;
      } else {
	$radsexcept = "";
      }
      push @{$subpattern{$level}{features}}, $exist;
      push @{$subpattern{$level}{features_except}}, $except;
      push @{$subpattern{$level}{var}}, $var;
      push @{$subpattern{$level}{varexcept}}, $varexcept;
      push @{$subpattern{$level}{rads}}, $rads;
      push @{$subpattern{$level}{radsexcept}}, $radsexcept;
      push @{$subpattern{$level}{lvar}}, $lvar;
      push @{$subpattern{$level}{transfer}}, $transfer;
      push @{$subpattern{$level}{on_failure}}, $on_failure;
    } else {
      die "Each realzone must specify either a table (possibly with a partition_space) or a zone (line ".($linenb+1).")";
    }
  }
}

print $translitteration_function;
print $detranslitteration_function;
print $operation_functions;
print $mgrules_s2t_function;
#print $mgrules_t2s_function;

for my $category (keys %partitionspace2features) {
  for $partitionspace (keys %{$partitionspace2features{$category}}) {
    print "\$partitionspace2features{\"$category\"}{\"$partitionspace\"}{features} = \"".$partitionspace2features{$category}{$partitionspace}{features}."\";\n";
    print "\$partitionspace2features{\"$category\"}{\"$partitionspace\"}{features_except} = \"".$partitionspace2features{$category}{$partitionspace}{features_except}."\";\n";
  }
}

print <<END;
sub compatible_feature {
  my \$tag = shift;
  my \$tagset = shift;
  my \$cat = shift;
  my \$do_not_do_transfer = shift || 0;
  \$tag = transfer(\$tag,\$cat) unless \$do_not_do_transfer;
  #??? \$tagset = transfer(\$tagset,\$cat);
  if (defined(\$compatible_feature_cache{\$cat}{\$tagset}{\$tag})) {
    return \$compatible_feature_cache{\$cat}{\$tagset}{\$tag};
  }
  if (\$tagset eq \"\") {
    \$compatible_feature_cache{\$cat}{\$tagset}{\$tag} = 1;
    return 1;
  }
  loop: for my \$stag (split /\\s*\\|\\s*/, \$tagset) {
    for my \$feat (split /\\s*\\.\\s*/, \$tag) {
      for my \$sfeat (split /\\s*\\.\\s*/, \$stag) {
        next loop if defined(\$non_compatible_attributes{\$cat}{\$feat}{\$sfeat});
      }      
    }
    \$compatible_feature_cache{\$cat}{\$tagset}{\$tag} = 1;
    return 1;
  }
  \$compatible_feature_cache{\$cat}{\$tagset}{\$tag} = 0;
  return 0;
}

sub compatible_partitionspace {
  my \$tag = shift;
  my \$partitionspace = shift;
  my \$cat = shift;
  return 1 if (\$partitionspace eq \"\");
  my \$local_answer;
  for \$ps (split /\\+/, \$partitionspace) {
    \$local_answer = -1;
    if (\$partitionspace2features{\$cat}{\$ps}{features} ne "") {
      if (compatible_feature(\$tag, \$partitionspace2features{\$cat}{\$ps}{features}, \$cat)) {
        if (\$partitionspace2features{\$cat}{\$ps}{features_except} ne "") {
          if (compatible_feature(\$tag, \$partitionspace2features{\$cat}{\$ps}{features_except}, \$cat) == 1) {
            \$local_answer = 0 if \$local_answer == -1;
          }
          \$local_answer = 1 if \$local_answer == -1;
        }
        \$local_answer = 1 if \$local_answer == -1;
      }
      \$local_answer = 0 if \$local_answer == -1;
    } else {
      if (\$partitionspace2features{\$cat}{\$ps}{features_except} ne "") {
        if (compatible_feature(\$tag, \$partitionspace2features{\$cat}{\$ps}{features_except}, \$cat) == 1) {
          \$local_answer = 0 if \$local_answer == -1;
        }
        \$local_answer = 1 if \$local_answer == -1;
      }
      \$local_answer = 1 if \$local_answer == -1;
    }
    return 1 if (\$local_answer == 1);
  }
  return 0;
}


sub valid_tag {
  my \$cat = shift;
  my \$s = shift;
END
  for (keys %{$invalid_feature_set{$cat}}) {
    print "  return 0 if equals_or_is_included_in(\$s,\"$_\",\$cat);\n";
  }
print <<END;
  return 1;
}

END

sub translitteration {
  my $s = shift;
  for (sort {length($b) <=> length($a)} keys %translitteration_hash) {
    $s =~ s/$_/$translitteration_hash{$_}/g;
  }
  return $s;
}
sub detranslitteration {
  my $s = shift;
  for (keys %translitteration_hash) {
    $s =~ s/$translitteration_hash{$_}/$_/g;
  }
  return $s;
}
sub normalize_function_name {
  my $s = shift;
  $s =~ s/-/__HYPHEN__/g;
  $s =~ s/\+/__PLUS__/g;
  return $s;
}

sub consistent_tag {
  my $cat = shift;
  my $s = shift;
  my $bool = shift || 0;
  for $f1 (split /\./, $s) {
    for $f2 (split /\./, $s) {
      next if ($f1 ge $f2);
      if (defined($non_compatible_attributes{$cat}{$f1}{$f2})) {
	return 0;
      }
    }
    if ($bool == 0 && defined($non_compatible_attributes_2_1tm{$cat}{$f1})) {
      for my $complextag (keys %{$non_compatible_attributes_2_1tm{$cat}{$f1}}) {
	return 0 if (compatible_feature($complextag,$s,$cat));
      }
    }
  }
  return 1;
}

sub expand_tagset {
  $ts = shift;
  my $didsomething = 0;
  if ($ts =~ /\([^()|]*\|/) {
    while ($ts =~ s/(\([^()|]*)\|/\1,/g) {}
    while ($ts =~ s/(^|\|)([^,\|]*?)\(([^,\|\(\)]+),([^\(\)\|]+)\)([^\|]*)(\||$)/\1\2\3\5|\2(\4)\5\6/) {
      $ts =~ s/\(([^,\(\)\|]+)\)/\1/g;
    }
  }
  return $ts;
}

sub compatible_feature {
  my $tag = shift;
  my $tagset = shift;
  my $cat = shift;
  if ($tagset eq "") {
    return 1;
  }
  loop: for my $stag (split /\s*\|\s*/, $tagset) {
    for my $feat (split /\s*\.\s*/, $tag) {
      for my $sfeat (split /\s*\.\s*/, $stag) {
        next loop if defined($non_compatible_attributes{$cat}{$feat}{$sfeat});
      }      
    }
    return 1;
  }
  return 0;
}

sub complete_tag {
  my $cat = shift;
  my $s = shift;
  my %ignore_features = ();
  for $f1 (split /\./, $s) {
    for (keys %{$non_compatible_feature{$cat}{$f1}}) {
      $ignore_features{$_} = 1;
    }
  }
  for my $complextag (keys %{$non_compatible_feature_2{$cat}}) {
    if (compatible_feature($complextag,$s,$cat)) {
      for (keys %{$non_compatible_feature_2{$cat}{$complextag}}) {
	$ignore_features{$_} = 1;
      }
    }
  }
  for $f1 (split /\./, $s) {
    for $name2 (keys %{$mandatory_feature_combination{$cat}{$f1}}) {
      next if (defined($ignore_features{$name2}));
      $localok = 0;
    test:
      for $f2 (keys %{$mandatory_feature_combination{$cat}{$f1}{$name2}}) {
	$qmf2 = quotemeta($f2);
	if ($s =~ /(^|\.)$qmf2(\.|$)/) {
	  $localok = 1;
#	  last test;
	}
      }
      if ($localok == 0) {return 0}
    }
  }
  return 1;
}

sub valid_tag {
  my $cat = shift;
  my $s = shift;
  for (keys %{$invalid_feature_set{$cat}}) {
    return 0 if equals_or_is_included_in($s,$_,$cat);
  }
  return 1;
}

sub equals_or_is_included_in {
  my $tag = shift;
  my $tagset = shift;
  my $cat = shift;
  my $answer = 0;
  loop:
  for my $stag (split /\s*\|\s*/, $tagset) {
    next loop if (length($stag) > length($tag)); 
    for my $feat (split /\s*\.\s*/, $stag) {
      next loop unless ($tag =~ /(^|\.\s*)$feat(\s*\.|$)/);
    }
  }
  return $answer;
}

print <<END;

my \$nb = 0;
my \%lastlemmaid;

while (<>) {
  chomp;
  if (++\$nb % 100 == 0) {print STDERR "  Inflecting lemmas...\$nb\\r"}
  next if /^\\#/ || /^\\s*\$/;
  /^(.*?)\\t(([^\\/:\\t]*)(?::([^\\/\\t]+))?(?:\\/:?([^\\t\\/]*)(?:\\/(.*?)(?:\\/(.*?))?)?)?)(\\t.*|\$)/;
  next if (\$3 eq "0");
  my \$citform = translitteration(\$1);
  my \$pattern_function = "pattern_".normalize_function_name(\$3);
  my \$vars = \$4;
  my \$explicit_stems = \$5;
  my \$suppletion = \$6;
  my \$tagspace = \$7;
  my \$info = \$1." - ".\$2;
  my \$remainder = \"\\t\".\$2.\$8;
  my \$label = "";
  if (\$citform =~ s/___(.*)\$//) {
    \$label = \$1;
  }
  \$lastlemmaid{\$citform."___".\$label}++;
  die "### ERROR: Citation form '\$citform' has initial or final whitespaces" if (\$citform =~ /^\\s+/ || \$citform =~ /\\s+\$/);
  \@stems = ();
  \@orig_stems = ();
  \%suppletive_forms = ();
  \$stems[0] = \$citform;
  if (\$suppletion ne \"\") {
    for (split /\\s*,\\s*/, \$suppletion) {
      /^(.*?)=(.*)\$/ || die "Invalid suppletion form: \$_";
      my \$tagset = expand_tagset(\$1);
      my \$sform = \$2;
      for my \$tag (split /\\|/, \$tagset) {
        \$suppletive_forms{\$tag}{\$sform} = 1;
      }
    }
  }
  if (\$explicit_stems ne \"\") {
    my \$stemid = 1;
    for (split /\\s*,\\s*/, \$explicit_stems) {
      \$stems[\$stemid] = translitteration(\$_) unless /^\$/;
      \$stemid++;
    }
  }
  \@orig_stems = \@stems;
  if (\$vverbose) {
    print STDERR "========================================================\\n";
    print STDERR "============ \$info  =============\\n";
    print STDERR "========================================================\\n";
  }
  \@forms = split /\\n/, \&{\$pattern_function}(\$stems[0],\$vars,\$tagspace)."\\n";
  my \$r = "";
  my \$output_label = "";
  if (\$withlabel) {
    \$output_label = "___\${label}__".(\$lastlemmaid{\$citform."___".\$label});
  }
  for (\@forms) {
    /^(.*?)\\t(.*?)\\t(.*)/ || next;
    if (\$dont_show_all) {
      my \$tmp = \$2;
      \$tmp =~ s/\\\\n/\\n/g;
      \$r .= detranslitteration(\$tmp).", ";
    } else {
      \$r .= detranslitteration("\$citform\$output_label\\tDefault\\t\$2\\t\$3")."\\n";
    }
  }
  if (\$dont_show_all) {
    \$r =~ s/, /\\t/g;
    \$r =~ s/\\t\$//;
    \$remainder =~ /^(\\t.*?)\\t/ || die;
    \$r .= "\$1\\n";
  } else {
    \$r =~ s/\\n/\$remainder\\n/g;
  }
  if (\$no_separator) {
    \$r =~ s/ //g;
  }
  print \$r;
}
if (\$nb > 100) {
  print STDERR "  Inflecting lemmas...done (\$nb lemmas inflected)\\n";
}

sub normalize_function_name {
  my \$s = shift;
  \$s =~ s/-/__HYPHEN__/g;
  return \$s;
}

sub expand_tagset {
  \$ts = shift;
  my \$didsomething = 0;
  if (\$ts =~ /\\([^()|]*\\|/) {
    while (\$ts =~ s/(\\([^()|]*)\\|/\\1,/g) {}
    while (\$ts =~ s/(^|\\|)([^,\\|]*?)\\(([^,\\|\\(\\)]+),([^\\(\\)\\|]+)\\)([^\\|]*)(\\||\$)/\\1\\2\\3\\5|\\2(\\4)\\5\\6/) {
      \$ts =~ s/\\(([^,\\(\\)\\|]+)\\)/\\1/g;
    }
  }
  return \$ts;
}

END






sub process_operation {
  my $linenb = shift;
  my $pattern_input = shift;
  my $pattern_output = shift;
  $pattern_input =~ s/#$/\$/;
  $pattern_input =~ s/^#/^/;
  $pattern_output =~ s/[#\$]$//;
  $pattern_output =~ s/^[\^#]//;
  my %s2t_input_variables = ();
  my %s2t_input_varid2perlvarid = ();
  my %s2t_input_perlvarid2varid = ();
  my %s2t_output_variables = ();
  my $s2t_input_maxperlvarid = 0;
  my $s2t_input_maxanonvarid = 0;
  my $s2t_output_maxperlvarid = 0;
  my $s2t_output_maxanonvarid = 0;
  while ($pattern_input =~ /\[([0-9]*):([^:]+):\]/) {
    my $varid = $1;
    my $lc = $2;
    if ($varid eq "") {
      $varid = --$s2t_input_maxanonvarid;
    } elsif ($varid eq "0") {
      $perlvarid = "";
    }
    if ($varid ne "0" && !defined($s2t_input_varid2perlvarid{$varid})) {
      $perlvarid = ++$s2t_input_maxperlvarid;
      $s2t_input_varid2perlvarid{$varid} = $perlvarid;
      $s2t_input_perlvarid2varid{$perlvarid} = $varid;
    }
    die "Unknown letterclass '$lc' (line ".($linenb+1).")" unless defined ($letterclassname2re{$lc});
    if ($varid eq "0") {
      $pattern_input =~ s/\[0:([^:]+):\]/$letterclassname2re{$1}/;
    } elsif ($varid eq "") {
      $pattern_input =~ s/\[:([^:]+):\]([\*\+\?]?)/$letterclassname2capturing_re{$1}/;
      my $op = $2;
      $pattern_input =~ s/__OP__/$op/;
      $s2t_input_variables{$perlvarid} = $lc;
    } else {
      if (defined($s2t_input_variables{$perlvarid})) {
	die "Type mismatch for variable n°$varid: '$s2t_input_variables{$perlvarid}' ne '$lc' (line ".($linenb+1).")" if $s2t_input_variables{$perlvarid} ne $lc;
	$pattern_input =~ s/\[([0-9]*):[^:]+:\]/\\$perlvarid/;
      } else {
	$pattern_input =~ s/\[[0-9]*:([^:]+):\]([\*\+\?]?)/$letterclassname2capturing_re{$1}/;
	my $op = $2;
	$pattern_input =~ s/__OP__/$op/;
	$s2t_input_variables{$perlvarid} = $lc;
      }
    }
  }
  while ($pattern_output =~ /\[([0-9]*):([^:]+):\]/) {
    my $varid = $1;
    my $lc = $2;
    if ($varid eq "") {
      $varid = --$s2t_output_maxanonvarid;
    } elsif ($varid eq "0") {
      die;
    }
    if ($varid ne "0" && $varid ne "") {
      $perlvarid = $s2t_input_varid2perlvarid{$varid};
      if ($s2t_output_maxperlvarid < $perlvarid) {
	$s2t_output_maxperlvarid = $perlvarid;
      }
    }
    die "Unknown letterclass '$lc'" unless defined ($letterclassname2re{$lc});
    if ($varid < 0) {
      $s2t_output_variables{$perlvarid} = $lc;
      my $f = normalize_function_name("$s2t_input_variables{$perlvarid}____$lc");
      if ($lc eq $s2t_input_variables{$perlvarid}) {
	$pattern_output =~ s/\[:[^:]+:\][\+\*\?]?/\$$perlvarid/;
      } elsif (defined($mappings{$f})) {
	$pattern_output =~ s/\[:[^:]+:\][\+\*\?]?/\$mappings{"$f"}{\$$perlvarid}/;
      } else {
	die "Invalid operation definition component: a mapping between class '".detranslitteration($s2t_input_variables{$perlvarid})."' and class '".detranslitteration($lc)."' seems to be required for variable \$$varid (\$$perlvarid) (line ".($linenb+1).")";
      }
    } elsif ($varid > 0) {
      my $f = normalize_function_name("$s2t_input_variables{$perlvarid}____$lc");
      if (defined($s2t_output_variables{$perlvarid})) {
	die if $s2t_output_variables{$perlvarid} ne $lc;
      }
      if ($lc eq $s2t_input_variables{$perlvarid}) {
	$pattern_output =~ s/\[[0-9]+:[^:]+:\][\+\*\?]?/\$$perlvarid/;
      } elsif (defined($mappings{$f})) {
	$pattern_output =~ s/\[[0-9]+:[^:]+:\][\+\*\?]?/\$mappings{"$f"}{\$$perlvarid}/;
      } else {
	die "Invalid operation definition component: a mapping between class '".detranslitteration($s2t_input_variables{$perlvarid})."' and class '".detranslitteration($lc)."' seems to be required for variable \$$varid (\$$perlvarid) (line ".($linenb+1).")";
      }
    }
  }
  die "Non-balanced operation definition component (".($maxi-1)." vs. ".($maxj-1)." anonymous variables, ".($maxi-1)." vs. ".($maxj-1)." explicitely named variables) (line ".($linenb+1).")" unless ($s2t_input_maxperlvarid == $s2t_output_maxperlvarid && $s2t_input_maxanonvarid == $s2t_output_maxanonvarid);
  return ($pattern_input,$pattern_output);
}

sub tags_ordering {
  my $tag = shift;
  my $category = shift;
  my $weight = 0;
  for (keys %ordering_weight) {
    if (compatible_feature($tag,$_,$category)) {
      $weight += $ordering_weight{$_};
    }
  }
  return $weight;
}
